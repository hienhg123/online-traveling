CREATE DATABASE [Online-Traveling]
GO
Use [Online-Traveling]
GO
CREATE TABLE [User](
    [userID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[username] [varchar](25) NOT NULL,
	[password] [varchar](25) NOT NULL,
	[lastName] [nvarchar](25) NOT NULL,
	[firstName] [nvarchar](25) NOT NULL,
	[gender] [bit] NULL,
	[dob] DATE NOT NULL CHECK (YEAR(GETDATE()) - YEAR(dob) >=18),
	[address] [nvarchar](255) NULL,
	[role] [int] NOT NULL,
	[status] [bit] NOT NULL,
	[phone] [varchar](10) NULL,
	[email] [varchar](50) NOT NULL,
)
GO
CREATE TABLE [Business](
     [busiID] int IDENTITY (1,1) NOT NULL PRIMARY KEY,
	 [username] nvarchar(25) NOT NULL,
	 [busiName] nvarchar(25) NOT NULL,
	 [email] nvarchar(50) NOT NULL,
	 [phonenumber] [varchar](10) NOT NULL,
	 [status] BIT NOT NULL,
	 [password] varchar(25) NOT NULL,
	 [address] nvarchar(255) NOT NULL,
	 [firstName] nvarchar(25) NOT NULL,
	 [lastName] nvarchar(25) NOT NULL
)

GO
CREATE TABLE [Destination](
	[DestinationId] int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[DestinationName] nvarchar(25) NOT NULL,
	[DestinationImage] varchar(225) NOT NULL,
)

GO
CREATE TABLE [Hotel](
     [HotelId] int IDENTITY (1,1) NOT NULL PRIMARY KEY,
	 [HotelName] nvarchar(25) NOT NULL,
	 [HotelAddress] nvarchar(255) NOT NULL,
	 [Description] nvarchar(500) NOT NULL,
	 [HotelImage] varchar(75) NOT NULL,
	 [busiID] int FOREIGN KEY REFERENCES [Business](busiID),
	 [DestinationId] int FOREIGN KEY REFERENCES [Destination](DestinationId) NOT NULL,
)
GO 
CREATE TABLE [RoomType](
   [roomTypeID] INT IDENTITY (1,1) NOT NULL PRIMARY KEY,
   [roomTypeName] varchar(25) NOT NULL,
   [description] nvarchar(300) NOT NULL
)
GO
CREATE TABLE [Room](
	[RoomId] int IDENTITY (1,1) NOT NULL PRIMARY KEY,
	[RoomDescription] nvarchar(500) NOT NULL,
	[RoomImage] varchar(75) NOT NULL,
	[Price] int NOT NULL,
	[Size] FLOAT NOT NULL,
	[RoomTypeId] int FOREIGN KEY REFERENCES [RoomType](roomTypeID),
	[Quantity] int NOT NULL CHECK(Quantity>=0),
	[HotelId] int FOREIGN KEY REFERENCES [Hotel](HotelId),
	[RoomName] nvarchar(75) NOT NULL
)


GO
CREATE TABLE [Utility](
	[UtilityId] int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[UtilityName] nvarchar(25) NOT NULL,
)
GO
CREATE TABLE [Room_Utility](
	[RoomId] int  NOT NULL FOREIGN KEY REFERENCES [Room](RoomId),
	[UtilityId] int NOT NULL FOREIGN KEY REFERENCES [Utility](UtilityId),
)
GO
CREATE TABLE [Amenities](
    [AmenitiesID] int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[AmenitiesName] nvarchar(50) NOT NULL
)
GO
CREATE TABLE [Hotel_Amenities](
    [AmenitiesID] int NOT NULL FOREIGN KEY REFERENCES [Amenities](AmenitiesID),
	[HotelId] int NOT NULL FOREIGN KEY REFERENCES [Hotel](HotelId)
)
GO
CREATE TABLE [DiscountByDate](
	[DiscountId] int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[DiscountFrom] DATE,
	[DiscountTo] DATE,
	[Percentage] INT NOT NULL CHECK([Percentage]>=0 AND [Percentage]<=100),
	[Status] bit NOT NULL,
)

GO
CREATE TABLE [RatePlan](
     [PlanId] int IDENTITY (1,1) NOT NULL PRIMARY KEY,
	 [PlanName] nvarchar(75) NOT NULL,
	 [PlanType] int NOT NULL,
	 [Percentage] float NOT NULL,
	 [Day] int,
	 [HotelId] int FOREIGN KEY REFERENCES [Hotel](HotelId),
	 [AvailablePayment] int, --1: Cash, 2: Credit Card, 3: Both--
	 [CancellationPolicy] varchar(25) CHECK([CancellationPolicy] = 'Non-refundable' OR [CancellationPolicy] = 'Flexible'),
	 [Meal] bit,
)

GO
CREATE TABLE Payment(
[PaymentID] INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
[PaymentStatus] BIT NOT NULL,
[PaymentAt] DATETIME NOT NULL,
[PaymentMethod] nvarchar(20) NOT NULL check ([PaymentMethod] = 'Cash' OR [PaymentMethod] = 'Paypal')
)
GO
CREATE TABLE [Booking](
	[BookingId] int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[RoomId] int FOREIGN KEY REFERENCES [Room](RoomId) NOT NULL,
	[userID] int FOREIGN KEY REFERENCES [User](userID) NOT NULL,
	[DiscountId] int FOREIGN KEY REFERENCES [DiscountByDate](DiscountId) NULL,
	[ToDate] DATE NOT NULL,
	[FromDate] DATE NOT NULL,
	[Status] varchar(25) NOT NULL CHECK([Status] = 'Booked' OR [Status] = 'Check-in' OR [Status] = 'Renting' OR [Status] = 'Check-out' OR [Status] = 'Finished' OR [Status] = 'Rated' OR [Status] = 'Cancelled'),
	[Rating] FLOAT CHECK([Rating]>=0 AND [Rating]<=5),
	[Price] FLOAT NOT NULL,
	[Comment] nvarchar(250),
	[PlanId] int NOT NULL FOREIGN KEY REFERENCES [RatePlan](PlanId),
	[PaymentID] int FOREIGN KEY REFERENCES [Payment](PaymentID),
	[BookerName] nvarchar(75),
)

CREATE TABLE [CheckHotel](
     [HotelId] int IDENTITY (1,1) NOT NULL PRIMARY KEY,
	 [HotelName] nvarchar(25) NOT NULL,
	 [HotelAddress] nvarchar(255) NOT NULL,
	 [Description] nvarchar(500) NOT NULL,
	 [HotelImage] varchar(75) NOT NULL,
	 [busiID] int,
	 [DestinationId] int  NOT NULL,
)



CREATE TABLE Room_Plan(
	[RoomId] int FOREIGN KEY REFERENCES [Room](RoomId),
	[PlanId] int FOREIGN KEY REFERENCES [RatePlan](PlanId)
)

GO 
CREATE TABLE [dbo].[Question](
	[QuestionId] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[QuestionContent] [nvarchar](255) NULL,
	[AnswerContent] [nvarchar](255) NULL,
	[busiID] [int]  NULL FOREIGN KEY REFERENCES [Business](busiID),
	[RoomId] [int] NOT NULL FOREIGN KEY REFERENCES [Room](RoomId),
	[userID] [int] NOT NULL FOREIGN KEY REFERENCES [User](userID),
	[dateQuestion] datetime NOT NULL,
	[dateAnswer] datetime  NULL,
	)
GO 
CREATE TABLE [dbo].[ReportQuestion](
	[ReportId] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[reportContent] [nvarchar](255) NULL,
	[dateReport] [datetime] NULL,
	[userID] [int]  NULL FOREIGN KEY REFERENCES [User](userID),
	[QuestionId] [int]  NULL FOREIGN KEY REFERENCES [Question](QuestionId),
	[status] bit null
	)
GO
CREATE TABLE Attraction(
[AttractionId] int IDENTITY(1,1) PRIMARY KEY NOT NULL,
[AttractionName] nvarchar(75) NOT NULL,
[DestinationId] int FOREIGN KEY REFERENCES [Destination](DestinationId) NOT NULL,
[AttractionImage] varchar(150) ,
[AttractionDescription] nvarchar(500) ,
[AttractionAddress] nvarchar(150) ,
[AttractionHotline] varchar(25)
)
GO
CREATE TABLE Cancellation(
   CancelID int NOT NULL IDENTITY (1,1) PRIMARY KEY,
   CancelDayType nvarchar(50) CHECK(CancelDayType ='Regular' or CancelDayType = 'Holiday') NOT NULL,
   CancelDateFrom DATE,
   CancelDateTo Date,
   CancelPolicyDetail nvarchar(250) NOT NULL,
   CancelAfter int NOT NULL,
   CancelBefore int NOT NULL,
   CancelPenalty int NOT NULL,
   CancelStatus bit not null,
   HotelId int FOREIGN KEY REFERENCES Hotel(HotelId)
)

GO
INSERT INTO [RoomType] (roomTypeName,[description])
VALUES ('Standard',N'Phòng cỡ trung bình, loại phòng cơ bản nhất tại hầu hết các khách sạn, được bố trí ở các tầng thấp, không có view đẹp và chỉ gồm những vật dụng cơ bản nhất.'),
('Superior',N'Là loại phòng chất lượng hơn Standard, diện tích phòng được tăng thêm, có view nhìn và cách bày trí đẹp mắt hơn hẳn và thường nằm ở những tầng gần giữa của tòa nhà.'),
('Deluxe',N'Loại phòng khách sạn này thường nằm ở các tầng giữa trở lên nên sở hữu view nhìn ra quang cảnh bên ngoài khá đẹp. Ở vị trí này, chất lượng phòng được nâng lên mức cao cấp với các tiện nghi hiện đại và tốt nhất'),
('Suite',N'loại phòng cao cấp nhất trong tất cả các loại phòng khách sạn, thông thường được bố trí ở các tầng cao nhất, sở hữu diện tích phòng cực rộng với đầy đủ tiện nghi như một căn nhà ở và có view nhìn toàn cảnh từ cửa sổ hoặc ban công ra thành phố cực đẹp.'),
('Single Bedroom',N'phòng 1 giường đơn cho 1 người.'), ('Twin Bedroom',N'phòng 2 giường đơn cho 2 người.'), ('Double Bedroom', N'phòng 1 giường đôi cho 2 người.'), ('Triple Bedroom',N'phòng 3 giường đơn HOẶC 1 giường đôi và 1 giường đơn cho 3 người.')
GO
INSERT INTO [Business] ([username],[busiName],[email],[phonenumber],[status],[password],[address],[firstName],[lastName])
VALUES ('hehe123','cong ty cay xanh','test1@gmail.com','0916356789',1,'123456789','Cau long bien, Ha Noi,Viet nam','long','nhan'),
('mitreem','mi tre em','test@gmail.com','0354876236',1,'123456789','Cau Bien Long, Viet Name, Ha Noi','long','an')
GO
INSERT INTO [User] ([username],[password],[lastName],[firstName],[gender],[dob],[address],[role],[status],[phone],[email])
VALUES ('fehg','12345678','nguyen','hieu',1,'2002-12-12','truong dai hoc fpt',2,1,'0852480599','test3@gmail.com'),
('admin' , '12345678','admin','admin',1,'2002-12-12','truong dai hoc fpt',1,1,'0365786256','admin@gmail.com')
GO 
INSERT INTO Destination([DestinationImage],[DestinationName])
VALUES ('https://www.dalattrip.com/dulich/media/2017/12/thanh-pho-da-lat.jpg', N'Đà Lạt'),
('https://cdn3.ivivu.com/2022/09/T%E1%BB%95ng-quan-du-l%E1%BB%8Bch-V%C5%A9ng-T%C3%A0u-ivivu.jpg',N'Vũng Tàu'),
('https://statics.vinpearl.com/dac-san-sai-gon-lam-qua-0_1624720587.jpg',N'TP. Hồ Chí Minh'),
('https://media.baodautu.vn/Images/chicuong/2022/06/24/16-Bana_Hilss.jpg',N'Đà Nẵng'),
('https://cdn3.ivivu.com/2022/07/h%E1%BB%93-g%C6%B0%C6%A1m-du-l%E1%BB%8Bch-H%C3%A0-N%E1%BB%99i-ivivu.jpg',N'Hà Nội'),
('https://media.baodautu.vn/Images/phuoctuan/2022/06/24/289026285_5571687692851380_7349109356899246250_n.jpeg',N'Huế'),
('https://vcdn1-dulich.vnecdn.net/2022/05/09/shutterstock-280926449-6744-15-3483-9174-1652070682.jpg?w=0&h=0&q=100&dpr=1&fit=crop&s=bGCo6Rv6DseMDE_07TT1Aw',N'Nha Trang'),
('https://upload.wikimedia.org/wikipedia/commons/6/6e/Bai-sao-phu-quoc-tuonglamphotos.jpg',N'Phú Quốc'),
('https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/08/hoi-an-quang-nam-vntrip.jpg',N'Hội An'),
('https://image-us.24h.com.vn/upload/4-2021/images/2021-10-28//1635390091-sapa-trong-may-mu-16349480971052073044867-width900height600.jpg',N'Sa Pa'),
('https://vcdn1-dulich.vnecdn.net/2022/04/02/dulichQuyNhon-1648878861-3106-1648880222.jpg?w=0&h=0&q=100&dpr=2&fit=crop&s=wFYxIbRCAt_Yy6OCMqXkOg',N'Quy Nhơn'),
('https://booking.pystravel.vn/uploads/posts/avatar/1591693949.jpg',N'Buôn Ma Thuột'),
('https://vcdn1-dulich.vnecdn.net/2022/04/08/vne-phanthietmuine4-huy-thoai-4752-8992-1649399388.jpg?w=0&h=0&q=100&dpr=2&fit=crop&s=4eKf77GWzU9ejWSI5i0Kog',N'Mũi Né')

GO
INSERT INTO Utility([UtilityName]) VALUES (N'Bồn tắm'), (N'Điều hòa không khí'), (N'Phòng tắm riêng'), (N'Ban công'), (N'Máy giặt'),
(N'Hệ thống cách âm'),(N'TV màn hình phẳng')

INSERT INTO Hotel(HotelName, HotelAddress, [Description], HotelImage, busiID, DestinationId) 
VALUES(N'Vườn Xuân', N'438 Minh Khai. Hà Nội, Việt Nam', N'Khách sạn này được thành lập từ 2023 bởi Nguyễn Đức Cường với rất nhiều thứ thú vị', 'images/hotel1.jpg', 1, 1),
(N'Không thật', N'118 Lý Thường Kiệt Hà Nội, Việt Nam', N'Từ khi khai trương khách sạn, đây đã là địa điểm quay phim nổi tiếng với nhiều phim truyền hình Viêt Nam có nhiều cảnh quay ở khách sạn này.', 'images/hotel2.jpg', 1, 1),
(N'Long Biên', N'118 Long Biên Hà Nội, Việt Nam',N'Năm ở vị trí đẹp, view nhìn toàn thành phố', 'images3.jpg',1,1)

INSERT INTO Room([RoomDescription],RoomImage, Price, Size, RoomTypeId, Quantity, HotelId, [RoomName] ) 
VALUES (N'Loại phòng ưa thích của Sơn Tùng MTP', 'images/room1.jpg', 2000000, 84, 1, 3, 1, N'Phòng tiêu chuẩn'),
(N'Phim 500 days of Summer từng có cảnh quay ở loại phòng này', 'images/room2.jpg', 2500000, 100, 2, 5, 2, N'Phòng quay phim tiêu chuẩn'),
(N'Loại phòng ưa thích của ca sĩ Chi Pu', 'images/room3.jpg', 2500000, 100, 3, 5, 1, N'Tiêu chuẩn nâng cao'),
(N'Phòng ưa thích của các khách hàng đến từ Nhật Bản', 'images/room4.jpg', 5000000, 120, 3, 5, 2, N'Phòng cảnh phim'),
(N'Phòng ưa thích của các khách hàng đến từ Nhật Bản', 'images/room4.jpg', 500000, 50, 1, 5, 2, N'Phòng cảnh phim'),
(N'Phòng ưa thích của các khách hàng đến từ Nhật Bản', 'images/room4.jpg', 1500000, 60, 1, 2, 1, N'Phòng cảnh phim'),
(N'Phòng ưa thích của các khách hàng đến từ Nhật Bản', 'images/room4.jpg', 1500000, 65, 1, 3, 3, N'Phòng cảnh phim')


INSERT INTO Room_Utility(RoomId,UtilityId)
VALUES(1,2),(1,1),(2,1),(2,2),(2,3),(3,1),(3,2),(3,3),(3,6),(4,1),(4,2),(4,3),(4,6),(4,7),
(5,2),(5,3),(5,7),(6,6),(6,7),(7,1),(7,2),(7,6),(7,7)

INSERT INTO Amenities(AmenitiesName)
VALUES (N'Wi-fi Miễn Phí'), (N'Chỗ đỗ xe'),(N'Phòng gia đình'),(N'Lễ Tân 24/7'),(N'Nhà Hàng'),(N'Hồ bơi'),(N'Cho phép mang vật nuôi')

INSERT INTO Hotel_Amenities(HotelId,AmenitiesID)
VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7)
,(2,1),(2,2),(2,4),(2,5),(2,7),(3,1),(3,3),(3,6)

INSERT INTO DiscountByDate(DiscountFrom,DiscountTo,[Percentage], [Status]) 
VALUES ('2022-09-04', '2022-09-05', 10, 0), ('2022-09-02', '2022-09-03', 10, 1), ('2022-12-24','2022-12-25', 10, 1)

INSERT INTO [RatePlan]([PlanName],[PlanType],[Percentage],[Day], [HotelId], [AvailablePayment], [CancellationPolicy], [Meal]) VALUES
('First Time Booking', 1, 10, NULL, 1, 1, 'Non-refundable', 1),
('First Time Booking', 1, 15, NULL, 2, 2, 'Flexible', 0),
('Long Stay 1', 2, 10, 7, 1, 3, 'Non-refundable', 0),
('Long Stay 2', 2, 15, 10, 1, 3, 'Flexible', 1)

INSERT INTO [Booking](userID,RoomId,FromDate,ToDate,[Status],DiscountId,Rating,[Price], [PlanId], [PaymentID], [BookerName])
VALUES(1,1, '2022-09-13', '2022-09-16', 'Check-in', 1, NULL, 2000000, 1, NULL, 'Nguyen Duc Cuong'), 
(1,3,'2022-09-02','2022-09-04','Finished', 2, 4.2, 2500000, 1, NULL, 'Nguyen Duc Dung')



INSERT INTO [Attraction](AttractionName, DestinationId, AttractionImage, AttractionAddress, AttractionDescription, AttractionHotline) VALUES
(N'Quảng trường Lâm Viên', 1, 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/09/33895294331_9c49395640_c-e1505464666175.jpg', N'Trần Quốc Toản, Phường 1, Thành phố Đà Lạt.', N'Sức hút kỳ lạ của công trình độc đáo này đã tạo nên một diện mạo mới cho thành phố mộng mơ Đà Lạt.', '09123901232'),
('Dalat Golf Café', 1, 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/10/dalat-golf-1.jpg',  N'14 Đống Đa, Phường 3, Thành phố Đà Lạt', N'Không phải đi quá xa để tìm cho mình một không gian có thể thu trọn thành phố Đà Lạt vào tầm mắt. Dù bạn đến Dalat Golf bất kỳ thời điểm nào đều có thể thoải mái tận hưởng, buổi sáng sương mù bao phủ', '02633 503535'),
(N'Amazing Coffee Đà Lạt', 1, 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/10/amazing-coffee-5.jpg', N'02 Đống Đa – Phường 3 – Đà Lạt', N'Trong tiết trời se se lạnh của Đà Lạt ngồi bên khung cửa sổ thơ mộng, phóng tầm mắt ngắm view thành phố từ trên cao đẹp bất kể ngày đêm thì chắc hẳn ai cũng sẽ bị si mê. Nằm ngay đầu cửa ngõ vào thành phố cùng view đắt giá nhìn thẳng bến xe liên tỉnh Đà Lạt.', '02633 50 35 35')


