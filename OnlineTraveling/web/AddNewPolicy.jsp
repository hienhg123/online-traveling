<%-- 
    Document   : AddNewPolicy
    Created on : Oct 26, 2022, 11:34:57 PM
    Author     : Admin
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="CSS/rentailHistory.css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
        <title>Cancellation Policy Add New</title>
    </head>
    <body>
        <%@include file="View/Header.jsp" %>
        <c:if test="${ sessionScope.business == null}">
            <br><br><br><br><br><br><br><br><br><br><br><br><br>
            <h1>Bạn không có quyền truy cập vào trang này</h1>
        </c:if>
        <c:if test="${ sessionScope.business != null}">   
            <br><br><br><br><br><br><br><br>
            <div class="main">
                <div class="container light-style flex-grow-1 container-p-y">
                    <div id="wrapper">
                        <h1>Tạo chính sách hủy đặt phòng mới</h1>
                        <c:if test="${requestScope.error != null}">
                            <h3>${requestScope.error}</h3>
                        </c:if>
                        <form action="addCancelPolicy" method="post" id="add">
                            Chính sách hủy phòng ngày thường
                            <input type="radio" name="dayType" value="Regular" onclick="showType(1)"><br/>
                            Chính sách hủy phòng cho ngày lễ hoặc mùa du lịch
                            <input type="radio" name="dayType" value="Holiday" onclick="showType(2)"><br/>
                            <div id="holiday">
                                <input type="hidden" name="dateFrom" placeholder="Ngày bắt đầu" id="dateFrom" required><br/>
                                <input type="hidden" name="dateTo" placeholder="Ngày kết thúc" id="dateTo" required><br/>
                            </div>
                            Chỉ có 1 giá trị ngày
                            <input type="radio" name="range" value="1" onclick="show(1)"><br/>
                            Có ngày trong khoảng
                            <input type="radio" name="range" value="2" onclick="show(2)"><br/>
                            <div id="2options">
                                <input type="hidden" name="after[]" id="after" required placeholder="Ngày 1">  
                                <input type="hidden" name="before[]"  id="before" required placeholder="Ngày 2">                      
                                <input type="number" name="penalty[]" required placeholder="Phí hủy phòng (%) tiền phòng" >  
                                <input type="text" name="detail[]" placeholder="Chi tiết chính sách" required=""><br/>
                            </div>
                            <input type="hidden" name="hotelID" value="${requestScope.hotelID}">

                            <a href="#" id="add_more_fields"><i class="fa fa-plus"></i>Add More</a>
                            <a href="#" id="remove_fields"><i class="fa fa-plus"></i>Remove Field</a>
                            <input type="submit" value="Thêm">
                        </form>

                    </div>
                </div>
            </div>

        </c:if>

        <%@include file="View/Footer.jsp" %>
    </body>
    <script>
        function show(val) {
            if (val === 1) {
                document.getElementById('after').type = 'number';
                document.getElementById('before').type = 'hidden';
                document.getElementsByTagName("span").style.display = 'none';
            } else {
                document.getElementById('after').type = 'number';
                document.getElementById('before').type = 'number';
                document.getElementsByTagName("span").style.display = 'inline';
            }

        }
        function showType(val) {
            if (val === 1) {
                document.getElementById('dateFrom').type = 'hidden';
                document.getElementById('dateTo').type = 'hidden';
            } else {
                document.getElementById('dateFrom').type = 'date';
                document.getElementById('dateTo').type = 'date';
            }
        }
        var new_options = document.getElementById('2options');
        var add_more_fields = document.getElementById('add_more_fields');
        var remove_fields = document.getElementById('remove_fields');

        add_more_fields.onclick = function () {
            var breakLine = document.createElement("br");
            var newAfter = document.createElement('input');
            newAfter.setAttribute('type', 'number');
            newAfter.setAttribute('name', 'after[]');
            newAfter.setAttribute('id', 'after');
            newAfter.setAttribute('placeholder', 'Ngày 1');
            newAfter.setAttribute('required', '');
            var newBefore = document.createElement('input');
            newBefore.setAttribute('type', 'number');
            newBefore.setAttribute('name', 'before[]');
            newBefore.setAttribute('id', 'before');
            newBefore.setAttribute('placeholder', 'Ngày 2');
            newBefore.setAttribute('required', '');
            var newPenalty = document.createElement('input');
            newPenalty.setAttribute('type', 'number');
            newPenalty.setAttribute('name', 'penalty[]');
            newPenalty.setAttribute('placeholder', 'Phí hủy phòng');
            newPenalty.setAttribute('required', '');
            var newDetail = document.createElement('input');
            newDetail.setAttribute('type', 'text');
            newDetail.setAttribute('name', 'detail[]');
            newDetail.setAttribute('placeholder', 'Chi tiết chính sách');
            newDetail.setAttribute('required', '');
            new_options.appendChild(breakLine);
            new_options.appendChild(newAfter);
            new_options.appendChild(newBefore);
            new_options.appendChild(newPenalty);
            new_options.appendChild(newDetail);
            new_options.appendChild(breakLine);
        };

        remove_fields.onclick = function () {
            var input_tags = new_options.getElementsByTagName('input');

            //loop de xoa 4 input
            for (var i = 0; i < 4; i++) {
                if (input_tags.length > 4) {
                    new_options.removeChild(input_tags[(input_tags.length) - 1]);
                }
            }
        };
    </script>
</html>
