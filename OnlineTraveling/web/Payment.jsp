<%-- 
    Document   : Payment
    Created on : Oct 26, 2022, 12:48:30 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="CSS/bookConfirm.css"/>
        <script src="https://www.paypal.com/sdk/js?client-id=Aci3ewgQyXzt4PbZpKAJcRj-AJ-ezjt96T0AglJ82kSilA2MRIJbOHttHEUHTeZXbaqLsG9Mt9XirXZ0"></script>
        <script src= "https://ajax.googleapis.com/ajax/1libs/jquery/3.5.1/jquery.min.js"></script>

        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="View/Header.jsp" %>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <h3>${requestScope.numberOfRoom}</h3>
        <h3>${requestScope.priceUS}</h3>
        <h3>${requestScope.priceVN}</h3>
        <h3>${requestScope.roomAmt}</h3>     
        <h3>${requestScope.hotelID}</h3>
        <div>
            <h3>Phương thức thanh toán</h3>  
            <div class="chooseMethod">
                <form action="action"></form>
                <div id="payment">
                    <input type="radio" name="payment" id="payment-cash" onclick="show(1)">
                    <label for="payment-cash">Thanh toán khi đến nơi</label>    
                </div>
                <div id="payment">
                    <input type="radio" name="payment" id="payment-paypal" onclick="show(2)">
                    <label for="payment-paypal">Paypal</label>
                </div>
                </form>
            </div>
            <div id="paypal" class="content paypal">
                <h2>Bạn đã chọn phương thức thanh toán qua PayPal. Bạn sẽ được chuyển đến website
                    của Paypal để hoàn thành giao dịch này</h2>
                <h2>Số tiền bạn sẽ phải thanh toán là: ${requestScope.priceUS}</h2>
                <form action="payment" method="post">
                    <input type="hidden" value="${requestScope.priceUS}" name="amt">
                    <input type="hidden" value="${requestScope.numberOfRoom}" name="rid">
                    <input type="hidden" value="${requestScope.roomAmt}" name="roomAmt">
                    <input type="hidden" value="${requestScope.hotelID}" name="hotelID">
                    <input type="hidden" name="toDate" value="${requestScope.toDate}">
                    <input type="hidden" name="fromDate" value="${requestScope.fromDate}">
                    <input type="hidden" value="Tiếp" id="submit">
                </form>
                <p id="mess"></p>
            </div>
            <div id="cash" class="content cash">
                <h2>Bạn đã chọn phương thức thanh toán qua khi đến nơi. Giao dịch của bạn sẽ được
                    thực hiện bởi ${requestScope.hotel.hotelName}</h2>
                <h2>Số tiền bạn sẽ phải thanh toán là: ${requestScope.priceVN}</h2>
                <form action="payment" method="post">
                    <input type="hidden" value="${requestScope.priceVN}" name="amount">
                    <input type="hidden" value="${requestScope.numberOfRoom}" name="room">
                    <input type="hidden" value="${requestScope.roomAmt}" name="roomAmt">
                    <input type="hidden" value="${requestScope.hotelID}" name="hotelID">
                    <input type="hidden" name="toDate" value="${requestScope.toDate}">
                    <input type="hidden" name="fromDate" value="${requestScope.fromDate}">
                    <input type="submit" value="Tiếp">
                </form>
            </div>

        </div>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>       
        <%@include file="View/Footer.jsp" %>
    </body>
    <script>
        function show(val) {
            if (val == 2) {
                document.getElementById('paypal').style.display = 'block';
                document.getElementById('cash').style.display = 'none';
            } else {
                document.getElementById('cash').style.display = 'block';
                document.getElementById('paypal').style.display = 'none';
            }

        }
        let value = document.getElementById("amount");
        paypal.Buttons({
            // Sets up the transaction when a payment button is clicked
            createOrder: (data, actions) => {
                return actions.order.create({
                    purchase_units: [{
                            amount: {
                                value: 1 // Can also reference a variable or function
                            }
                        }]
                });
            },
            // Finalize the transaction after payer approval
            onApprove: (data, actions) => {
                return actions.order.capture().then(function (orderData) {
                    // Successful capture! For dev/demo purposes:
                    console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));
                    const transaction = orderData.purchase_units[0].payments.captures[0];
//                    alert(`Transaction ${transaction.status}: ${transaction.id}\n\nSee console for all available details`);
                    // When ready to go live, remove the alert and show a success message within this page. For example:
                    let mess = document.getElementById('mess');
                    let next = document.getElementById('submit').type = "submit";
                    mess.textContent = "Thanh toán thành công"

                });
            }
        }).render('#paypal');
    </script>
</html>
