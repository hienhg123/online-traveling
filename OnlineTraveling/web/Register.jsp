<%-- 
    Document   : Register.jsp
    Created on : Sep 15, 2022, 9:08:21 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Đăng kí</title>

        <!-- Font Icon -->
        <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">

        <!-- Main css -->
        <link rel="stylesheet" href="CSS/register.css">
    </head>
    <body>
        <%@include  file="View/Header.jsp" %>
        <div class="main">
            <!-- Sign up form -->
            <section class="signup">
                <div class="container">
                    <div class="signup-content">
                        <div class="signup-form">
                            <h2 class="form-title">Đăng kí cho cá nhân</h2>
                            <h2><span>${requestScope.message}</span></h2>
                            <form action="register" method="post" class="register-form" id="register-form">
                                <div class="form-group">
                                    <label for="email"><i class="zmdi zmdi-email"></i></label> <input type="email" name="email" id="email" placeholder="Email của bạn" />
                                </div>
                                <div class="form-group">
                                    <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label> <input type="text" name="username" id="username" placeholder="Chúng tôi có thể gọi bạn là" />
                                </div>
                                <div class="form-group">
                                    <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label> <input type="text" name="firstName" id="firstName" placeholder="Tên" />
                                </div>
                                <div class="form-group">
                                    <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label> <input type="text" name="lastName" id="lastName" placeholder="Họ" />
                                </div>
                                <div class="form-group">
                                    <label for="pass"><i class="zmdi zmdi-lock"></i></label> <input type="date" name="dob" id="dob" placeholder="Ngày sinh" /> 
                                </div>
                                <div class="form-group">
                                    <label for="pass"><i class="zmdi zmdi-lock"></i></label> <input type="password" name="password" id="newPass" placeholder="Mật khẩu" />
                                </div>

                                <div class="form-group">
                                    <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
                                    <input type="password" name="confirm" id="confirm" placeholder="Xác nhận lại mật khẩu" />
                                </div>
                                <div class="form-group form-button">
                                    <input type="button" value="Đăng kí" id="send" onclick="checkFullForUser()" class="form-submit">
                                    <p id="message"></p>                                                       
                                </div>
                            </form>
                        </div>
                        <div class="signup-image">
                            <figure>
                                <img src="images/personal-image.jpg" alt="sing up image">
                            </figure>
                            <a href="home" class="signup-image-link">Đã là thành viên</a>
                            <span><br><br></span>
                            <a href="businessRegister" class="signup-image-link">
                                <h3>Đăng kí cho doanh nghiệp</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <%@include  file="View/Footer.jsp" %>    
        <script src="JS/CheckEvents.js"></script>
    </body>
</html>
