<%-- 
    Document   : AdmincheckHotel
    Created on : Oct 1, 2022, 8:10:31 PM
    Author     : nguye
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Check Active Hotel</h1>
        <table border="1">
            <tr>
                <td>HotelId</td>
                <td>HotelName</td>
                <td>HotelAddress</td>
                <td>Description</td>
                <td>HotelImage</td>
                <td>busiID</td>
                <td>DestinationId</td>
                <td>Action</td>
            </tr>
            <c:forEach items="${cheho}" var="h">
                <tr>
                    <td>${h.getHotelId()}</td>
                    <td>${h.getHotelName()}</td>
                    <td>${h.getHotelAddress()}</td>
                    <td>${h.getDescription()}</td>
                    <td><img style='width:150px;height:100px' src="${h.getHotelImage()}" alt="${h.getHotelImage()}"></td>
                    <td>${h.getBusiID()}</td>
                    <td>${h.getDestinationID()}</td>
                    <td><a href="CheckHotel?hid=${h.getHotelId()}">CheckHotel</a></td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
