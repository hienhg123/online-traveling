<%-- Document : Login.jsp Created on : Sep 12, 2022, 9:06:20 PM Author : admin --%>
    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <!DOCTYPE html>
        <html>
        <head>
            <title>Login Form</title>
            <link rel="stylesheet" href="./CSS/login.css">
            <link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
            <script src="https://kit.fontawesome.com/a81368914c.js"></script>
            <meta name="viewport" content="width=device-width, initial-scale=1">
        </head>
        <body>
            <%@include file="View/Header.jsp"%>
                <img class="wave" src="images/wave.png">
                <div class="container">
                    <div class="img">
                        <img src="images/bg.svg">
                    </div>
                    <div class="login-content">
                        <form action="login" method="post">
                            <input type="hidden" value="${url}" name="url"/>
                            <img src="images/avatar.svg">
                            <h3 class="title">Chào Mừng</h3>
                            <h4 class="mb-0 mr-4 mt-2" style="color:red">${requestScope.error}</h4>
                            <div class="input-div one">
                                <div class="i">
                                    <i class="fas fa-user"></i>
                                </div>
                                <div class="div">
                                    <h5>Email</h5>
                                    <input type="email" class="input" name="email">
                                </div>
                            </div>
                            <div class="input-div pass">
                                <div class="i">
                                    <i class="fas fa-lock"></i>
                                </div>
                                <div class="div">
                                    <h5>Mật khẩu</h5>
                                    <input type="password" class="input" name="password">
                                </div>
                            </div>
                            <br>
                            <a class="login-a" href="Register.jsp">Đăng ký</a>
                            <br>
                            <a class="login-a" href="businessLogin">Bạn là doanh nghiệp?</a>
                            <br>
                            <a class="login-a" href="forgot">Quên mật khẩu?</a>
                            <input type="submit" class="btn-login" value="Login">
                        </form>
                    </div>
                </div>
                <%@include file="View/Footer.jsp" %>
                    <script src="./JS/CheckEvents.js"></script>
        </body>
        </html>