<%-- 
    Document   : changePassword
    Created on : Sep 16, 2022, 12:33:33 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Change Password</title>

        <!-- Font Icon -->
        <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css" />

        <!-- Main css -->
        <link rel="stylesheet" href="./CSS/register.css" />
    </head>
    <body>
        <%@include file="View/Header.jsp" %>

        <div class="main">         
            <!-- Sign up form -->
            <section class="signup">
                <div class="container">
                    <div class="signup-content">
                        <div class="signup-form">
                            <h1 class="form-title">Change Password</h1>
                            <span><h3>${requestScope.mess}</h3></span>
                            <form action="changePassword" method="post" class="register-form" id="register-form">
                                <div class="form-group">
                                    <input type="password" name="oldPass" id="oldPass" placeholder="Your Old Password" />
                                </div>
                                <div class="form-group">
                                    <input type="password" name="newPass" id="newPass" placeholder="New Password" />
                                </div>
                                <div class="form-group">
                                    <input type="password" name="confirm" id="confirm" placeholder="Confirm Password" />
                                </div>
                                <div class="form-group form-button">
                                    <input type="button" onclick="checkPassToChange()" id="send" class="form-submit" value="SUBMIT" />
                                    <p id="message"></p>
                                    <c:set var="email" value="${sessionScope.account.email}"/>
                                    <c:if test="${requestScope.wrongPass == null}">
                                        <h2>${requestScope.sucsessChangeing}</h2>
                                    </c:if>
                                    <c:if test="${requestScope.wrongPass != null}">
                                        <h2>${requestScope.wrongPass}</h2>
                                    </c:if>  
                                </div>
                            </form>
                        </div>
                        <div class="signup-image">
                            <figure>
                                <img src="images/change-password.jpg" alt="Change Password Image" />
                            </figure>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <%@include file="View/Footer.jsp" %>
        <script src="vendor/jquery/jquery.min.js"></script>       
        <script src="JS/CheckEvents.js"></script>
    </body>
</html>
