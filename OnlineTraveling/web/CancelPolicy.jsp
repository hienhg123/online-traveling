<%-- 
    Document   : CancelPolicy
    Created on : Oct 26, 2022, 11:14:36 PM
    Author     : Admin
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="CSS/rentailHistory.css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
        <title>Cancellation Policy</title>
    </head>
    <body>
        <%@include file="View/Header.jsp" %>

        <c:if test="${ sessionScope.business == null}">
            <br><br><br><br><br><br><br><br><br><br><br><br><br>
            <h1>Bạn không có quyền truy cập vào trang này</h1>
        </c:if>
        <c:if test="${ sessionScope.business != null}">        
            <br><br><br><br><br><br><br><br>
            <div class="main">
                <div class="container light-style flex-grow-1 container-p-y">
                    <div id="wrapper">
                        <h1>Chính sách hủy phòng của bạn </h1>
                        <c:if test="${requestScope.mess != null}">
                            <h3>${requestScope.mess}</h3>
                        </c:if>
                        <button class="button-28" role="button">
                            <a class="link" href="addCancelPolicy?hotelID=${requestScope.hotelID}">Thêm chính sách</a></button>
                        <table id="keywords" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th>Chính sách</th>
                                    <th>Chi tiết</th>
                                    <th>Phí hủy phòng</th>
                                    <th>Loại ngày</th>
                                    <th>Từ ngày</th>
                                    <th>Đến ngày</th>
                                    <th>Chính sửa</th>                           
                                    <th>Trạng Thái</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.policy}" var="p">   
                                    <tr>
                                        <td>${p.cancelDayType}</td>
                                        <td>${p.cancelPolicyDetail}</td>
                                        <td>${p.cancelPenalty}%</td>
                                        <td>${p.cancelDayType}</td>
                                        <td>${p.cancelDateFrom}</td>
                                        <td>${p.cancelDateTo}</td>
                                        <td><a href="updatePolicy?cancelID=${p.cancelID}&hotelID=${requestScope.hotelID}">Chỉnh sửa</a></td>
                                        <c:if test="${p.cancelStatus == true}">
                                            <td><a href="deactiveStatus?cancelID=${p.cancelID}&hotelID=${requestScope.hotelID}">Hoạt động</a></td>
                                        </c:if>
                                        <c:if test="${p.cancelStatus != true}">
                                            <td><a href="activeStatus?cancelID=${p.cancelID}&hotelID=${requestScope.hotelID}">Không hoạt động</a></td>
                                        </c:if>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </c:if>


        <%@include file="View/Footer.jsp" %>
    </body>
</html>
