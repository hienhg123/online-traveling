<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="CSS/addNewFirstBookPlan.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    </head>
    <body>
        <%@include  file="View/Header.jsp" %>




        <c:if test="${requestScope.hotel.busiID != sessionScope.busiID}">
            <br><br><br><br><br><br><br><br><br><br><br><br><br>
            <h1>Bạn không có quyền truy cập vào trang này</h1>
        </c:if>
        <c:if test="${requestScope.hotel.busiID == sessionScope.busiID}">
            <p>busiID của hotel = ${requestScope.hotel.busiID}</p>
            <p>busiID của session = ${sessionScope.busiID}</p>
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br>

            <div class="container-md">
                <h1>Tạo khuyến mãi lần đầu đặt phòng cho khách sạn của bạn</h1>
                <div class="row">
                    <div class="col-md-8">
                        <form action="addFirstBookPlan" method="post">
                            <div class="">
                                <div><input type="text" placeholder="Tên gói giá trị" name="planName"}" required></div>
                                <div><input type="number" min="0" max="100" class="form-control" placeholder="Phần trăm giảm giá" name="percentage" required></div>
                                <div>
                                    <span>Phương thức thanh toán: </span>
                                    <input type="radio" name="availablePayment" value="1" checked>  Tiền mặt
                                    <input type="radio" name="availablePayment" value="2">  Paypal
                                    <input type="radio" name="availablePayment" value="3">  Cả tiền mặt và paypal
                                </div>
                                <div>
                                    <span>Chính sách hủy phòng:</span>
                                    <input type="radio" name="cancellationPolicy" value="Non-refundable" checked>  Không hoàn tiền
                                    <input type="radio" name="cancellationPolicy" value="Flexible">  Theo chính sách

                                </div>
                                <div>
                                    <span>Bao gồm bữa sáng:</span>
                                    <input type="radio" name="meal" value="false" checked>  Không
                                    <input type="radio" name="meal" value="true">  Có
                                </div>
                            </div>
                            <input type="hidden" value="${requestScope.hotel.hotelId}" name="hotelId">
                            <button type="submit" class="btn btn-warning">GỬI</button>

                        </form>
                    </div>
                </div>
            </div>
        </c:if>

        <%@include  file="View/Footer.jsp" %>

    </body>
</html>
