<%-- 
    Document   : AddRatePlan
    Created on : Oct 16, 2022, 8:16:00 PM
    Author     : ACER
--%>


<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add new rate plan</title>

        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    </head>

    <body>
        <%@include file="View/Header.jsp" %>
        <c:if test="${requestScope.hotel.busiID != sessionScope.busiID}">
            <br><br><br><br><br><br><br><br><br><br><br><br><br>
            <h1>Bạn không có quyền truy cập vào trang này</h1>
        </c:if>
        <c:if test="${requestScope.hotel.busiID == sessionScope.busiID}">
            <div class="main">
                <br><br><br><br><br><br><br><br><br><br><br>
                <div class="container light-style flex-grow-1 container-p-y">
                    <div id="wrapper">
                        <div class="row">

                            <div class="col-md-4">
                                <h1>Khuyến mãi lần đầu</h1>
                                <h3>Hãy thu hút những khách hàng mới của bạn bằng cách thêm khuyến mãi cho những người chưa đặt phòng ở khách sạn của bạn bao giờ</h3>
                                <h3><a href="addFirstBookPlan?hotelId=${requestScope.hotelId}">Click vào đây để thêm</a></h3>
                            </div>
                            <div class="col-md-4">
                                <h1>Khuyến mãi theo số ngày ở</h1>
                                <h3>Hãy trở nên nổi bật trong mắt những người có kế hoạch đi du lịch dài hạn nào</h3>
                                <h3><a href="addLongStayPlan?hotelId=${requestScope.hotelId}">Click vào đây để tạo</a></h3>
                            </div>
                            <div class="col-md-4">
                                <h1>Khuyễn mãi cho người đặt phòng sớm</h1>
                                <h3>Thu hút những vị khách đã sắp xếp được kế hoạch đi du lịch trong tương lai xa</h3>
                                <h3><a href="addEarlyBookPlan?hotelId=${requestScope.hotelId}">Click vào đây để tạo</a></h3>
                            </div>
                            <div class="col-md-4">
                                <h1>Gói giá trị phòng tiêu chuẩn</h1>
                                <h3>Thêm và quản lý những gói giá trị không cần điều kiện để thỏa mãn</h3>
                                <h3><a href="addCustomPlan?hotelId=${requestScope.hotelId}">Click vào đây để tạo</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </c:if>
        <%@include file="View/Footer.jsp" %>
    </body>

</html>
