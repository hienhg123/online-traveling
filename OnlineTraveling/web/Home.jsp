<%-- Document : home.jsp Created on : Sep 13, 2022, 9:26:17 PM Author : admin --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <!DOCTYPE html>
            <html>

            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
                <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
                <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
                <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
                <link rel="stylesheet" href="CSS/home.css">
                <title>Book'nStay</title>
            </head>

            <body>
                <jsp:include page="View/Header.jsp" />

                <!--Banner-->
                <section class="home" id="home">
                    <div class="content">
                        <h3>Hãy chọn điểm đến của bạn</h3>
                        <p>Khám phá những miền đất mới cùng chúng tôi</p>
                        <a href="#gallery" class="btn">Khám phá</a>
                    </div>
                    <div class="video-container">
                        <video src="images/vid-1.mp4" id="video-slider" loop autoplay muted></video>
                    </div>
                    <div class="search">
                        <form action="searchByDestination" class="searching" method="post">
                            <p id="message"></p><br />
                            <input type="text" name="destination" placeholder="Điểm đến" required>
                            <label for="dateFrom">Nhận phòng</label>
                            <input type="date" name="from" id="dateFrom">
                            <label for="dateTo">Trả phòng</label>
                            <input type="date" name="to" id="dateTo">
                            <button type="button" onclick="checkDate()" id="send"><i class="fas fa-search"
                                    id="search-btn"></i></button>
                        </form>
                    </div>
                </section>

                <!--Gallery-->
                <section class="gallery" id="gallery">
                    <h1 class="heading">
                        <span>g</span>
                        <span>a</span>
                        <span>l</span>
                        <span>l</span>
                        <span>e</span>
                        <span>r</span>
                        <span>y</span>
                    </h1>
                    <div class="box-container">
                        <c:forEach var="x" items="${requestScope.destinationData}">
                            <div class="box">
                                <img src="${x.destinationImage}" alt="">
                                <div class="content">
                                    <h3>${x.destinationName}</h3>
                                    <a href="/OnlineTraveling/hotel?desId=${x.destinationID}" class="btn">see
                                        more</a>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </section>

                <!-- Services -->
                <section class="services" id="services">
                    <h1 class="heading">
                        <span>s</span>
                        <span>e</span>
                        <span>r</span>
                        <span>v</span>
                        <span>i</span>
                        <span>c</span>
                        <span>e</span>
                        <span>s</span>
                    </h1>
                    <div class="box-container">
                        <div class="box">
                            <i class="fas fa-hotel"></i>
                            <h3>Khách sạn</h3>
                            <p>Tuyển chọn những khách sạn chất lượng cao kèm giá cả phù hợp với tất cả nhu cầu của khách hàng!
                            </p>
                        </div>
                        <div class="box">
                            <i class="fas fa-utensils"></i>
                            <h3>Ăn uống</h3>
                            <p>Cung cấp đầy đủ những địa điểm ăn uống hot trend trên mạng xã hội và cập nhật thường xuyên!
                            </p>
                        </div>
                        <div class="box">
                            <i class="fas fa-bullhorn"></i>
                            <h3>An toàn</h3>
                            <p><span>Book'nStay </span>hứa hẹn an toàn của quý khách luôn đặt lên hàng đầu kèm chính sách bảo đảm quyền lợi của đôi bên!</p>
                        </div>
                        <div class="box">
                            <i class="fas fa-plane"></i>
                            <h3>Dịch vụ</h3>
                            <p>Tư vấn 24/7 nhanh gọn lẹ với tiêu chí trải nhiệm của bạn là niềm hạnh phúc của
                                <span>Book'nStay</span>!
                            </p>
                        </div>
                        <div class="box">
                            <i class="fas fa-hiking"></i>
                            <h3>Phiêu lưu</h3>
                            <p>Luôn tạo ra các sân chơi mang đầy sự thú vị và phấn khích để kích thích sự mạo hiểm của quý khách hàng!</p>
                        </div>
                    </div>
                </section>

                <!-- Contact -->
                <section class="contact" id="contact">
                    <h1 class="heading">
                        <span>c</span>
                        <span>o</span>
                        <span>n</span>
                        <span>t</span>
                        <span>a</span>
                        <span>c</span>
                        <span>t</span>
                    </h1>
                    <div class="row">
                        <div class="image">
                            <img src="images/contact-img.svg" alt="">
                        </div>
                        <form action="">
                            <div class="inputBox">
                                <input type="text" placeholder="Name">
                                <input type="email" placeholder="Email">
                            </div>
                            <div class="inputBox">
                                <input type="number" placeholder="Phone">
                                <input type="text" placeholder="Subject">
                            </div>
                            <textarea placeholder="message" name="" id="" cols="30" rows="10"></textarea>
                            <input type="submit" class="btn" value="send message">
                        </form>
                    </div>
                </section>

                <!--Dashboard-->
                <div class="row">
                    <div class="col-md-8"></div>
                    <div class="col-md-1"></div>
                    <c:if test="${sessionScope.account.role == 1}">
                        <div class="dashboard col-md-3">
                            <p>Dashboard</p>
                            <p>System admin</p>
                            <a href="listRoom">Room type list</a>
                            <br />
                            <a href="listUser">User List</a>
                            <br>
                            <a href="AcdeHotel">Active New Hotel</a>
                        </div>
                    </c:if>
                    <c:if test="${sessionScope.account.role != 1}">
                        <div class="dashboard col-md-3">
                            <p>Dashboard</p>
                            <p>System Business</p>
                            <br>
                            <a href="AddHotel">Add New Hotel</a>
                            <br/>
                                <a href="adminProcessReportBusiness">Process Report Business</a>
                        </div>
                    </c:if>
                </div>


                <!--Footer-->
                <%@include file="View/Footer.jsp" %>

                    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
                    <script src="JS/Home_Events.js"></script>
                    <script>
                        function compareDate() {
                            let from_raw = document.getElementById("dateFrom").value;
                            let to_raw = document.getElementById("dateTo").value;
                            let from = new Date(from_raw);
                            let to = new Date(to_raw);
                            //check if to is behind from
                            if (to.getTime() > from.getTime()) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                        //check date
                        function checkDate() {
                            let check = compareDate();
                            if (check === true) {
                                document.getElementById("send").type = "submit";
                            } else {
                                message.textContent = "Ngày nhận phòng phải sau ngày trả phòng";
                            }
                        }
                    </script>
            </body>

            </html>