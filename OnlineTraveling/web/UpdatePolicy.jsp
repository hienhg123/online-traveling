<%-- 
    Document   : AddNewPolicy
    Created on : Oct 26, 2022, 11:34:57 PM
    Author     : Admin
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="CSS/rentailHistory.css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
        <title>Cancellation Policy Add New</title>
    </head>
    <body>
        <%@include file="View/Header.jsp" %>
        <c:if test="${ sessionScope.business == null}">
            <br><br><br><br><br><br><br><br><br><br><br><br><br>
            <h1>Bạn không có quyền truy cập vào trang này</h1>
        </c:if>
        <c:if test="${ sessionScope.business != null}">   
            <c:set var="cp" value="${requestScope.cancel}"/>
            <br><br><br><br><br><br><br><br>
            <div class="main">
                <div class="container light-style flex-grow-1 container-p-y">
                    <div id="wrapper">
                        <h1>Chỉnh sửa chính sách hủy đặt phòng </h1>
                        <form action="updatePolicy" method="post" id="add">
                            <c:if test="${cp.cancelDayType == 'Holiday'}">
                                <h3>Chỉnh sửa cho các ngày lễ và mùa du lịch</h3>
                                <input type="hidden" name="dayType" value="Holiday">
                                Ngày bắt đầu: <input type="date" name="fromDate" value="${cp.cancelDateFrom}" required><br/>
                                Ngày kết thúc: <input type="date" name="toDate" value="${cp.cancelDateTo}" required><br/>
                                Hủy đặt phòng trước: <input type="number" name="after" value="${cp.cancelAfter}" required><br/>
                                Hoặc hủy đặt phòng trước: <input type="number" name="before" value="${cp.cancelBefore}" required><br/>

                            </c:if>
                            <c:if test="${cp.cancelDayType == 'Regular'}">   
                                <h3>Chỉnh sửa cho các ngày bình thường</h3>
                                <input type="hidden" name="dayType" value="Regular">
                                Hủy đặt phòng trước: <input type="number" name="after" value="${cp.cancelAfter}" required><br/>
                                Hoặc hủy đặt phòng trước: <input type="number" name="before" value="${cp.cancelBefore}" required><br/>                              
                            </c:if> 
                            Phí hủy phòng: <input type="number" name="penalty" value="${cp.cancelPenalty}" required ><br/>    

                            <input type="hidden" name="hotelID" value="${requestScope.hotelID}">
                            <input type="hidden" name="cancelID" value="${cp.cancelID}">
                            <textarea  name="detail" rows="10" cols="30" placeholder="Chi tiết về chính sách hủy phòng" required>${cp.cancelPolicyDetail}</textarea>
                            <input type="submit" value="Cập nhật">
                        </form>
                    </div>
                </div>
            </div>
        </c:if>
        <%@include file="View/Footer.jsp" %>
    </body>
    <script>
        function show(val) {
            if (val == 1) {
                document.getElementById('after').type = 'number';
                document.getElementById('before').type = 'hidden';
            } else {
                document.getElementById('after').type = 'number';
                document.getElementById('before').type = 'number';
            }

        }
        function showType(val) {
            if (val == 1) {
                document.getElementById('dateFrom').type = 'hidden';
                document.getElementById('dateTo').type = 'hidden';
            } else {
                document.getElementById('dateFrom').type = 'date';
                document.getElementById('dateTo').type = 'date';
            }
        }
    </script>
</html>
