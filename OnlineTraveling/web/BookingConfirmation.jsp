<%-- 
    Document   : BookingConfirmation
    Created on : Oct 2, 2022, 4:34:25 PM
    Author     : ACER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="CSS/bookConfirm.css"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script src="https://www.paypal.com/sdk/js?client-id=Aci3ewgQyXzt4PbZpKAJcRj-AJ-ezjt96T0AglJ82kSilA2MRIJbOHttHEUHTeZXbaqLsG9Mt9XirXZ0"></script>
    </head>
    <body>
        <br><br><br>

        <c:if test="${sessionScope.user == null}">
            <h1>Bạn cần đăng nhập</h1>
        </c:if>
        <c:if test="${sessionScope.user != null}">
            <c:set var = "firstBookPlan" value = "${requestScope.firstTimePlan}"/>
            <c:set var = "longStayPlan" value = "${requestScope.longStayPlan}"/>
            <c:set var = "earlyBookPlan" value = "${requestScope.earlyBookPlan}"/>
            <c:set var = "finalPlan" value = "${requestScope.finalPlan}"/>

            <div class="container">
                <h1>Đơn đặt phòng</h1>
                <form action="bookingConfirmation" method="post">
                    <div class ="row">
                        <input type="hidden" name="userId" value="${sessionScope.user.userID}">
                        <input type="hidden" name="fromDate" value="${requestScope.fromDate}">
                        <input type="hidden" name="toDate" value="${requestScope.toDate}">
                        <input type="hidden" name="hotelID" value="${requestScope.hotel.hotelId}">
                        <div class="col-md-6">
                            <p>Khách sạn: ${requestScope.hotel.hotelName}</p>
                            <p>Địa chỉ: ${requestScope.hotel.hotelAddress}</p>
                            <br>
                            <p>Ở từ: ${requestScope.fromDate} </p>
                            <p>Đến ngày: ${requestScope.toDate} </p>
                            <p>Số đêm: ${days}</p>
                            <br>
                            <h1>Chi tiết giá</h1>

                            <table class="table table-striped table-dark">
                                <c:forEach items="${requestScope.roomList}" var="r">
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">1 đêm</th>
                                        <th scope="col">${days} đêm</th>
                                        <th scope="col">${r.amount} phòng</th>
                                    </tr>


                                    <tr>
                                        <th scope="row">${r.roomName}</td>
                                        <td>${r.oneNightPrice_str} VND</td>
                                        <td>${r.manyNightPrice_str} VND</td>
                                        <td>${r.oneRoomPrice_str} VND</td>
                                    </tr>
                                    <input type="hidden" name="rid" value="${r.roomId}">
                                    <input type="hidden" name="amt" value="${r.amount}">
                                    <input type="hidden" name="rpid" value="${r.planId}">
                                    <input type="hidden" name="price" value="${r.manyNightPrice}">
                                </c:forEach>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                    <th scope="col">Số tiền phải thanh toán:</th>
                                    <th scope="col">${priceStr} VND</th>
                                
                                </tr>
                            </table>
                            <div class="col-md-6">
                                <p>Tên người đặt: ${sessionScope.user.firstName}</p>
                                <p>Email: ${sessionScope.user.email}</p>
                                <p>Số điện thoại: ${sessionScope.user.phone}</p>   
                                
                                <input type="hidden"  name="finalPrice" value="${requestScope.price}">
                                <input type="hidden"  name="fromDate" value="${requestScope.fromDate}">
                                <input type="hidden"  name="toDate" value="${requestScope.toDate}">
                                <input type="submit" value="Bước tiếp theo">
                            </div>
                        </div>
                    </div>
                </form>
            </c:if>

            <br><br><br><br><br><br><br><br><br><br><br><br><br>
            </body>  
            </html>
