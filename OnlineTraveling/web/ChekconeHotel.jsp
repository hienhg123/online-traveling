<%-- 
    Document   : ChekconeHotel
    Created on : Oct 1, 2022, 8:45:32 PM
    Author     : nguye
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList" %>
<%@page import="dal.*" %>
<%@page import="models.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="CheckHotel" method="post">
            HotelId:<input type="number" name="hoid" value="${id}" readonly="true" >
            <br>
            HotelName:<input type="text" name="honame" value="${name}" readonly="true" >
            <br>
            HotelAddress:<input type="text" name="hoadd" value="${add}" readonly="true" >
            <br>
            HotelAddress:<input type="text" name="des" value="${des}" readonly="true" >
            <br>
            HotelImage:<img style='width:150px;height:100px' src="${img}" alt="${img}">
            <br>
            BusId:<input type="number" name="buid" value="${buid}" readonly="true" >
            <br>
            DestinationId:<input type="number" name="desid" value="${desid}" readonly="true" >
            <br>
            <input type="submit" name="ac" value="Active" onclick="return acc()">    
            <input type="submit" name="de"  value="Delete" onclick="return dele()" style="margin-left: 30px">
        </form>
    </body>
    <script>
        function acc() {
            let text;
            if (confirm("Active Hotel") === true) {
                return true;
            } else {
                return false;
            }
        }
        
    </script>
     <script>
        function dele() {
            let texts;
            if (confirm("Delete Hotel") === true) {
                return true;
            } else {
                return false;
            }
        }
        
    </script>
</html>
