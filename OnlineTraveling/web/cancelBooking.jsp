<%-- 
    Document   : cancelBooking
    Created on : Oct 2, 2022, 8:53:26 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <style type="text/css">
        table, th, td{
            border:1px solid #868585;
        }
        table{
            border-collapse:collapse;
            width:100%;
        }
        th, td{
            text-align:left;
            padding:10px;
        }
        table tr:nth-child(odd){
            background-color:#eee;
        }
        table tr:nth-child(even){
            background-color:white;
        }
        table tr:nth-child(1){
            background-color:black;
            color:white;
        }
    </style>
    <body>
        <%@include  file="View/Header.jsp" %>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <br/><br/><br/>
        <a href="roomrental">Tất Cả</a>
        <a href="bookedRoom">Chờ xác nhận</a>      
        <table border="1">
            <tr>
                <th>FromDate</th>
                <th>ToDate</th>
                <th>Status</th>
                <th>RoomName</th>
                <th>RoomPrice</th>        

            </tr>

            <c:forEach items="${bookListT}" var="r">
                    <c:set var="id" value="${r.getBookingId()}"/>
                    <tr>
                        <td>${r.getFromDate()}</td>
                        <td>${r.getToDate()}</td>
                        <td>${r.getStatus()}</td>
                        <td>${r.getRoomName()}</td>
                        <td>${r.getPrice()}</td>
                    </tr>
            </c:forEach>
        </table>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <br/><br/><br/>
        <%@include  file="View/Footer.jsp" %>       
    </body>
</body>
</html>
