<%-- 
    Document   : UserRentalRoom
    Created on : Sep 30, 2022, 8:56:46 PM
    Author     : Dell
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.ArrayList" %>
<%@page import="dal.*" %>
<%@page import="models.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="CSS/rentailHistory.css" />
        <script src="https://www.paypal.com/sdk/js?client-id=Aci3ewgQyXzt4PbZpKAJcRj-AJ-ezjt96T0AglJ82kSilA2MRIJbOHttHEUHTeZXbaqLsG9Mt9XirXZ0"></script>
        <title>Detail</title>
    </head>

    <body>
        <%@include file="/View/Header.jsp" %>
        <div class="main">
            <div class="container light-style flex-grow-1 container-p-y"> 
                <div id="wrapper">
                    <c:set var="b" value="${requestScope.bookListT}"/>
                    <h1>Khách sạn ${b.getHotelName()}</h1>     
                    <h2>${requestScope.days}</h2>
                    <c:if test="${requestScope.penalty == 0}">
                        <h3>Hủy phòng miễn phí</h3>
                        <h3>${requestScope.days}</h3>
                        <form action="cancelBooking" method="post">
                            <input type="hidden" name="bookingID" value="${requestScope.bookingID}">
                            <input type="submit" value="Hoàn thành">
                        </form>
                    </c:if>
                    <c:if test="${requestScope.penalty != 0}">
                        <h3 id="mess">Bạn sẽ phải thanh trả :  nếu muốn hủy phòng</h3>
                        <h3>${requestScope.days}</h3>
                        <div id="paypal"></div> 
                    </c:if>
                </div>
            </div>
        </div>

        <%@include file="/View/Footer.jsp" %>
        <script>
            paypal.Buttons({
                // Sets up the transaction when a payment button is clicked
                createOrder: (data, actions) => {
                    return actions.order.create({
                        purchase_units: [{
                                amount: {
                                    value: 1 // Can also reference a variable or function
                                }
                            }]
                    });
                },
                // Finalize the transaction after payer approval
                onApprove: (data, actions) => {
                    return actions.order.capture().then(function (orderData) {
                        // Successful capture! For dev/demo purposes:
                        console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));
                        const transaction = orderData.purchase_units[0].payments.captures[0];
//                        alert(`Transaction ${transaction.status}: ${transaction.id}\n\nSee console for all available details`);
                        // When ready to go live, remove the alert and show a success message within this page. For example:
                        var suscess = document.getElementById('paypal');
                        suscess.innerHTML = ' <form action="cancelBooking" method="post">\
                            <input type="hidden" name="bookingID" value="${requestScope.bookingID}">\
                            <input type="hidden" name="transID" value="${transaction.id}">\
                            <input type="submit" value="Hoàn thành">\
                            </form>'
                        var mess = document.getElementById("mess");
                        mess.textContent = "Giao dịch đã hoàn tất, bạn đã hủy phòng thành công"
                    });
                }
            }).render('#paypal');
        </script>
    </body>
</html>
