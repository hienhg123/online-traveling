<%-- Document : propertyManagement Created on : Sep 17, 2022, 11:29:00 PM Author : ACER --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
            <!DOCTYPE html>
            <html>

            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>JSP Page</title>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
                <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
                <link rel="stylesheet" href="CSS/businessHotel.css">
                <style>
                    body {
                        font-family: 'Inter';
                        font-size: 22px;
                    }
                </style>
            </head>

            <body>
                <%@include file="View/Header.jsp" %>
                    <section class="packages" id="packages">
                        <h1 class="heading">
                            <span>h</span>
                            <span>o</span>
                            <span>t</span>
                            <span>e</span>
                            <span>l</span>
                        </h1>
                         <div><a href="businessAnswer"><button>Quản lý câu hỏi khách hàng về phòng?</button></a></div>
                        <c:if test="${sessionScope.business.busiID != null || requestScope.currentBusinessID == sessionScope.business.busiID}">
                            <div class="box-container">
                                <c:forEach var="x" items="${requestScope.listHotel}">
                                    <div class="box">
                                        <img src="${x.getHotelImage()}" alt="">
                                        <div class="content">
                                            <h3> <i class="fas fa-map-marker-alt"></i>${x.getHotelName()}</h3>
                                            <p>Address: ${x.getHotelAddress()}</p>
                                            <div class="stars">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="far fa-star"></i>
                                            </div>
                                            <a class="btn" href="/OnlineTraveling/hotelBusinessRoom?hotelId=${x.getHotelId()}">${x.getHotelName()}</a>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </c:if>
                    </section>

                    <%@include file="View/Footer.jsp" %>
                        </div>
            </body>

            </html>