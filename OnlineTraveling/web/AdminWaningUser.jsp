<%-- 
    Document   : AdminWaningUser
    Created on : Oct 16, 2022, 12:18:50 AM
    Author     : HONG QUAN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="main">
            <!-- Sign up form -->
            <section class="signup">
                <div class="container">
                    <div class="signup-content">
                        <div class="signup-form">
                            <h1 class="form-title">Enter your Email</h1>
                           
                            <form action="processRequestBusiness" method="post" class="register-form" id="register-form">
                                <div class="form-group">
                                    <input type="email" name="email" id="email" placeholder="Your Email Address" />
                                </div>
                                <div class="form-group form-button">
                                    <input type="button" id="send" class="form-submit" value="Send" onclick="checkEmailLengthToSend()" />
                                    <p id="message"></p>
                                </div>
                            </form>
                        </div>
                        <div class="signup-image">
                            <figure>
                                <img src="images/email.png" alt="Email Image" />
                            </figure>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </body>
    <script src="vendor/jquery/jquery.min.js"></script> 
<script src="JS/CheckEvents.js"></script>
</html>
