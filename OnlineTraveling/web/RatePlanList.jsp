<%-- 
    Document   : RatePlanList
    Created on : Oct 16, 2022, 3:40:06 PM
    Author     : ACER
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hotel List</title>
        <link rel="stylesheet" href="CSS/rentailHistory.css" />
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    </head>

    <body>
        <%@include file="View/Header.jsp" %>


        <c:if test="${requestScope.hotel.busiID != sessionScope.busiID}">
            <br><br><br><br><br><br><br><br><br><br><br><br><br>
            <h1>Bạn không có quyền truy cập vào trang này</h1>
        </c:if>
        <c:if test="${requestScope.hotel.busiID == sessionScope.busiID}">

            <div class="main">
                <div class="container light-style flex-grow-1 container-p-y">
                    <div id="wrapper">
                        <h1>Danh sách gói giá </h1>
                        <button class="button-28" role="button">
                            <a class="link" href="addRatePlan?hotelId=${requestScope.hotelId}">Tạo mới</a></button>
                        <table id="keywords" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th>Tên gói giá</th>
                                    <th>Giảm giá</th>
                                    <th>Loại giảm giá</th>
                                    <th>Số ngày</th>
                                    <th>Chỉnh sửa</th>
                                    <th>Xóa</th>
                                    <th>Ghi chú</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.planList}" var="p">
                                    <tr>
                                        <td>${p.planName}</td>
                                        <td>${p.percentage}%</td>
                                        <td>
                                            <c:if test="${p.planType == 1}">
                                                Lần đầu book
                                            </c:if>
                                            <c:if test="${p.planType == 2}">
                                                Theo ngày ở
                                            </c:if>
                                            <c:if test="${p.planType == 3}">
                                                Theo ngày đặt trước
                                            </c:if>
                                            <c:if test="${p.planType == 4}">
                                                Tiêu chuẩn
                                            </c:if>

                                        </td>
                                        <td>${p.day}</td>
                                        <td>
                                            <c:if test="${p.planType==1}">
                                                <a href="updateFirstBookPlan?hotelId=${requestScope.hotelId}&planId=${p.planId}">Chỉnh sửa</a>
                                            </c:if>
                                            <c:if test="${p.planType==2}">
                                                <a href="updateLongStayPlan?PlanId=${p.planId}">Chỉnh sửa</a>
                                            </c:if>
                                            <c:if test="${p.planType==3}">
                                                <a href="updateEarlyBookPlan?PlanId=${p.planId}">Chỉnh sửa</a>
                                            </c:if>
                                            <c:if test="${p.planType==4}">
                                                <a href="updateCustomPlan?PlanId=${p.planId}">Chỉnh sửa</a>
                                            </c:if>
                                        </td>
                                        <td><a href="deleteRatePlan?PlanId=${p.planId}&hotelId=${requestScope.hotelId}">Xóa</a></td>
                                        <td>
                                            <!--
                                            <c:if test="${p.reasonable == false}">
                                                <p style="color: red">Có gói điều kiện thấp hơn nhưng lại cho nhiều khuyến mãi hơn</p>
                                            </c:if>-->
                                            <c:if test="${p.reasonable == true}">
                                                Không có
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </c:if> 
        <%@include file="View/Footer.jsp" %>
    </body>

</html>