<%-- 
    Document   : checkOTP
    Created on : Sep 16, 2022, 7:19:06 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Quên Mật Khẩu</title>

        <!-- Font Icon -->
        <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css" />

        <!-- Main css -->
        <link rel="stylesheet" href="./CSS/register.css" />
    </head>
    <body>
        <%@include file="View/Header.jsp" %>
        <div class="main">
            <!-- Sign up form -->
            <section class="signup">
                <div class="container">
                    <div class="signup-content">
                        <div class="signup-form">
                            <h1 class="form-title">Enter your OTP: </h1>
                            <span><h3>${requestScope.wrongOTP}</h3></span>
                            <form action="checkOTP" method="post" class="register-form" id="register-form">
                                <div class="form-group">
                                    <label for="otp"><i class="zmdi zmdi-account"></i></label>
                                    <input type="text" name="otp" id="otp" placeholder="Enter OTP" />
                                </div>
                                <div class="form-group form-button">
                                    <input type="submit" class="form-submit" value="Send" />
                                </div>
                            </form>
                        </div>
                        <div class="signup-image">
                            <figure>
                                <img src="images/otp.jpg" alt="OTP Image" />
                            </figure>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <%@include file="View/Footer.jsp" %>
        <script src="vendor/jquery/jquery.min.js"></script>
    </body>
</html>
