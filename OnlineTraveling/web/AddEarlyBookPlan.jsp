<%-- 
    Document   : AddEarlyBookPlan
    Created on : Oct 17, 2022, 6:49:27 AM
    Author     : ACER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="CSS/addNewFirstBookPlan.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    </head>
    <body>
        <%@include  file="View/Header.jsp" %>




        <c:if test="${requestScope.hotel.busiID != sessionScope.busiID}">
            <br><br><br><br><br><br><br><br><br><br><br><br><br>
            <h1>Bạn không có quyền truy cập vào trang này</h1>
        </c:if>
        <c:if test="${requestScope.hotel.busiID == sessionScope.busiID}">
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br>

            <div class="container-md">
                <h1 style="font-weight: bold">Tạo gói giá theo số ngày khách hàng đặt phòng trước cho khách sạn của bạn</h1>
                <div class="row">
                    <div class="col-md-8">
                        <form action="addEarlyBookPlan" method="post">
                            <div>
                                <h3>Tên gói giá trị:</h3>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Tên gói giá trị của bạn ..." name="planName" >
                            </div>
                            <br>
                            
                            <div>
                                <h3>Số ngày mà khách hàng nếu book trước, sẽ được hưởng giá ưu đãi.</h3>
                            </div>
                            <div class="form-group">
                                <input type="number" min="1" max="730" class="form-control" placeholder="Số ngày ở" name="day" value="7">
                            </div>
                            <br>
                            
                            <div>
                                <h3>Phần trăm ưu đãi mà khách hàng sẽ hưởng khi số ngày đặt trước đáp ứng</h3>
                            </div>
                            <br>
                            
                            <div class="form-group">
                                <input type="number" min="0" max="100" class="form-control" placeholder="Phần trăm giảm giá" name="percentage" value="10">
                            </div>
                            <input type="hidden" value="${requestScope.hotelId}" name="hotelId">
                            <c:if test="${requestScope.error != null}">
                                <h3>${requestScope.error}</h3>
                            </c:if>
                            <button type="submit" class="btn btn-warning">GỬI</button>

                        </form>
                    </div>
                </div>
            </div>
        </c:if>



        <%@include  file="View/Footer.jsp" %>

    </body>
</html>
