<%-- 
    Document   : UpdateAttraction
    Created on : Oct 17, 2022, 8:25:07 AM
    Author     : ACER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="CSS/addNewFirstBookPlan.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    </head>
    <body>
        <%@include  file="View/Header.jsp" %>




        <c:if test="${sessionScope.account.role != 1}">
            <br><br><br><br><br><br><br><br><br><br><br><br><br>
            <h1>Bạn không có quyền truy cập vào trang này</h1>
        </c:if>
        <c:if test="${sessionScope.account.role == 1}">
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br>

            <div class="container-md">
                <h1 style="font-weight: bold">Chỉnh sửa thông tin về địa điểm thu hút</h1>
                <div class="row">
                    <div class="col-md-8">
                        <form action="updateAttraction" method="post">
                            <div>
                                <h3>Tên địa điểm:</h3>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Tên địa điểm ..." name="attName" value="${requestScope.att.attractionName}">
                            </div>
                            <br>

                            <div>
                                <h3>Link ảnh:</h3>
                            </div>
                            <div class="form-group">
                                <input type="text"  class="form-control" placeholder="Đường dẫn tới ảnh" name="attImage" value="${requestScope.att.getAttractionImage()}">
                            </div>
                            <br>

                            <div>
                                <h3>Mô tả: </h3>
                            </div>
                            <br>
                            <div class="form-group">
                                <textarea placeholder="Mô tả" name="attDesc" style="width: 75%">${requestScope.att.attractionDescription}</textarea>  
                            </div>

                            <div>
                                <h3>Địa chỉ: </h3>
                            </div>
                            <br>
                            <div class="form-group">
                                <input type="text"  class="form-control" placeholder="Địa chỉ ..." name="attAddress" value="${requestScope.att.attractionAddress}">
                            </div>

                            <div>
                                <h3>Hotline: </h3>
                            </div>
                            <br>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Hotline. . ." name="attHotline" value="${requestScope.att.attractionHotline}">
                            </div>

                            <input type="hidden" value="${requestScope.att.attractionId}" name="attId">
                     
                            <c:if test="${requestScope.error != null}">
                                <h3>${requestScope.error}</h3>
                            </c:if>
                            <button type="submit" class="btn btn-warning">GỬI</button>

                        </form>
                    </div>
                </div>
            </div>
        </c:if>



        <%@include  file="View/Footer.jsp" %>
