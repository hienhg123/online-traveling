<%-- 
    Document   : forgotPassword
    Created on : Sep 16, 2022, 11:03:39 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Forgot Password</title>

        <!-- Font Icon -->
        <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css" />

        <!-- Main css -->
        <link rel="stylesheet" href="./CSS/register.css" />
    </head>
     <body>
        <%@include file="View/Header.jsp" %>
        <div class="main">
            <!-- Sign up form -->
            <section class="signup">
                <div class="container">
                    <div class="signup-content">
                        <div class="signup-form">
                            <h1 class="form-title">Enter your Email</h1>
                            <span><h3>${requestScope.notFound}</h3></span>
                            <form action="forgot" method="post" class="register-form" id="register-form">
                                <div class="form-group">
                                    <input type="email" name="email" id="email" placeholder="Your Email Address" />
                                </div>
                                <div class="form-group form-button">
                                    <input type="button" id="send" class="form-submit" value="Send" onclick="checkEmailLengthToSend()" />
                                    <p id="message"></p>
                                </div>
                            </form>
                        </div>
                        <div class="signup-image">
                            <figure>
                                <img src="images/email.png" alt="Email Image" />
                            </figure>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <%@include file="View/Footer.jsp" %>
        <script src="vendor/jquery/jquery.min.js"></script> 
        <script src="JS/CheckEvents.js"></script>
    </body>
</html>
