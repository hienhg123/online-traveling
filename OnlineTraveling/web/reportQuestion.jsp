<%-- 
    Document   : reportQuestion
    Created on : Oct 15, 2022, 11:24:13 AM
    Author     : HONG QUAN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <title>Document</title>
        <style>
            .gradient-custom { /* fallback for old browsers */
                background: #4facfe; /* Chrome 10-25,
                Safari 5.1-6 */
                background: -webkit-linear-gradient(to bottom right, rgba(79, 172, 254,
                    1), rgba(0, 242, 254, 1)); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+,
                    Safari 7+ */
                background: linear-gradient(to bottom right, rgba(79, 172, 254, 1), rgba(0,
                    242, 254, 1))
            }
        </style>
    </head>
    <body>
        <section class="gradient-custom">
            <div class="container my-5 py-5">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-12 col-lg-10 col-xl-8">
                        <div class="card">
                            <div class="card-body p-4">
                                <h4 class="text-center mb-4 pb-2">Báo cáo</h4>

                                <div class="row">
                                    <div class="col">
                                        <div class="d-flex flex-start">
                                            <div class="flex-grow-1 flex-shrink-1">
                                                <div>
                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <p class="mb-1">
                                                            Tìm hỗ trợ hoặc báo cáo bình luận
                                                        </p>
                                                    </div>
                                                    <p class="small mb-0">
                                                        Bạn có thể báo cáo bình luận sau khi chọn vấn đề
                                                    </p>
                                                </div>    
                                                <form action="reportQuestion" method="post">

                                                    <div class="d-flex flex-start mt-4">
                                                        <div class="flex-grow-1 flex-shrink-1">
                                                            <div>
                                                                <div class="d-flex justify-content-between align-items-center">  
                                                                    <input type="hidden"value="${requestScope.questionID}" name="questionId"/>
                                                                    <input type="hidden" value="${requestScope.roomID}" name="roomID"/>
                                                                    <input type="radio" name="reportContent" value="Bạo lực"/>Bạo lực

                                                                    <input type="radio" name="reportContent" value="Quấy rối"/>Quấy rối

                                                                    <input type="radio" name="reportContent" value="Thông tin sai sự thật"/>Thông tin sai sự thật

                                                                    <input type="radio" name="reportContent" value="Spam"/>Spam

                                                                    <input type="radio" name="reportContent" value="Bán hàng trái phép"/>Bán hàng trái phép
                                                                    <br/>
                                                                    <input type="radio" name="reportContent" value="Ngôn ngữ gây thù ghét"/>Ngôn ngữ gây thù ghét
                                                                </div>      
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex flex-start mt-4">
                                                        <div class="flex-grow-1 flex-shrink-1">
                                                            <div>
                                                                <div class="d-flex justify-content-between align-items-center">  
                                                                    Vấn đề khác:<input type="text" name="reportname"/>
                                                                </div>      
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex flex-start mt-4">
                                                        <div class="flex-grow-1 flex-shrink-1">
                                                            <div>
                                                                <div class="d-flex justify-content-between align-items-center">  
                                                                    <input type="submit" value="Gửi"/>
                                                                </div>      
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>