<%-- 
    Document   : AddnewHotel
    Created on : Sep 30, 2022, 2:41:23 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="AddHotel" method="post" enctype="multipart/form-data">
            HotelName<input type="text" name="name" id="name"><br>
            HotelAddress<input type="text" name="addr" id="add"><br>
            Description<input type="text" name="des" id="des"><br>
            HotelImage<input type="file" name="img" id="img"><br>
            BusiID<input type="number" name="bus" id="bus"><br>
            DestinationId<input type="number" name="desid" id="desid"><br>
            <input type="submit" value="Submit" onclick="return myFunctionss()">
        </form>
    </body>
    <script>
        function myFunctionss() {
            let text;
            var name = document.getElementById("name").value;
            var add = document.getElementById("add").value;
            var des = document.getElementById("des").value;
            var img = document.getElementById("img").value;
            var busid = document.getElementById("bus").value;
            var desid = document.getElementById("desid").value;
            var msg = "";
            var flag = true;
            if (name === "") {
                msg += "HotelName not empty.\n";
                flag = false;
            }
            if (add === "") {
                msg += "Address not empty.\n";
                flag = false;
            }
            if (des === "") {
                msg += "Description not empty.\n";
                flag = false;
            }
            if (img === "") {
                msg += "HotelImage not empty.\n";
                flag = false;
            }
            if (busid === "") {
                msg += "busiID not empty.\n";
                flag = false;
            }
            if (desid === "") {
                msg += "DestinationId not empty.\n";
                flag = false;
            }
            if (flag === false) {
                alert(msg);
                return false;
            }
            if (flag === true) {
                if (confirm("Send to admin for approval") === true) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    </script>
</html>
