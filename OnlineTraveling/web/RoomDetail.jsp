<%-- 
    Document   : RoomDetail
    Created on : Sep 30, 2022, 11:52:08 PM
    Author     : HONG QUAN
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>

        <style>
            .userstyle{
                font-size: 20px;
                font-weight: bold;

            }
            .liststyle{
                width:45%;
                margin-left: 30px;
                border-radius: 15px;
                border-radius: 15px 15px;
                border-radius: 15px 15px 15px;
                border-radius: 15px 15px 15px;
            }
            .manager{
                margin-left: 50px;
            }
            .btnDelete{
                display:flex;
            }
            .hoverDelete{
                text-decoration: none;
                color:black;
            }
        </style>
    </head>
    <body>
        <c:set var="r" value="${requestScope.roomR}"/>

        <table border="1">
            <tbody>
                <tr>
                    <td>${r.getRoomName()}</td>
                </tr>
                <tr>
                    <td>${r.getRoomDescription()}</td>
                </tr>
                <tr>
                    <td>Tien nghi: ${r.getRoomType().getDescription()}</td>
                </tr>
                <tr>
                    <td>Dien tich :   ${r.getSize()}</td>
                </tr>
                <tr>
                    <td>Giá: ${r.getPrice()}/ngày</td>
                </tr>            
            </tbody>
        </table>

        
        <c:if test="${sessionScope.account == null}">
            <div>Ðăng nhập</div>
        </c:if>

        <c:if test="${sessionScope.account !=null}">
            <div>
                ${sessionScope.account.getUserName()}
            </div>
        </c:if>

        <div class="form-group">

            <c:if test="${sessionScope.account !=null}">
                <form action="RoomDetail" method="post">
                    <textarea class="form-control" name="content" placeholder="Nhap danh gia cua ban..."></textarea>
                    <br> 
                    <input type="hidden" value="${r.getRoomId()}" name="roomId"/>
                    <input type="submit" value="Gui binh luan"class="btn btn-danger"/>

                </form>
            </c:if>
            <c:if test="${sessionScope.account == null}">

                <form action="login" method="get">
                    <textarea class="form-control" name="content" placeholder="Nhap danh gia cua ban..."></textarea>
                    <br> 
                    <input type="hidden" value="/OnlineTraveling/RoomDetail?roomId=${r.getRoomId()}" name="url"/>
                    <input type="submit" value="Gui binh luan"class="btn btn-danger"/>

                </form>
            </c:if>


            <c:forEach items="${requestScope.listQ}" var="question">
                <div class="row">
                    <div class="btnDelete">           
                        <c:set var="id" value="${question.getQuestionId()}"/>

                        <div class="liststyle" class="col-md-2" style="padding:10px;background-color: grey;">

                            <div class="userstyle">${question.getUser().getUserName()}</div>
                            <div class="col-md-10 my-date">
                                <p>${question.getQuestionContent()}</p>
                                <i>${question.getDateQuestion()}</i>
                            </div>
                        </div>
                        <div class="delete">
                            <buttion>...</buttion>
                            <div class="hoverDelete">
                                <c:set var="useID" value="${question.getUser().getUserID()}"/>
                                <c:choose>
                                    <c:when test="${sessionScope.account.getUserID() == useID}">
                                        <a style="text-decoration: none;
                                           color:black;" href="editQuestion?questionID=${id}&roomId=${r.getRoomId()}" onclick="doDelete('${id}')">Xóa</a>
                                    </c:when>

                                    <c:when test="${sessionScope.account == null}">
                                        <form action="login" method="get">
                                            <input type="hidden" value="/OnlineTraveling/RoomDetail?roomId=${r.getRoomId()}" name="url"/>

                                            <input type="submit" value="Báo cáo"class="btn btn-danger"/>
                                        </form> 
                                    </c:when>
                                    <c:when test="${sessionScope.account.getUserID() != useID}">

                                        <a href="reportQuestion?roomId=${r.getRoomId()}&questionID=${id}" class="btn btn-danger">Báo cáo</a>
                                    </c:when>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>

                <div class="row">
                    <div class="manager" clas="col-md-2" style="padding:10px">
                        <div  class="col-md-10 my-date">
                            <c:if test="${question.getAnswerContent() != null}">
                                <div>Quản lý</div>
                                <p>${question.getAnswerContent()}</p>
                                <i>${question.getDateAnswer()}</i>
                            </c:if>

                        </div>

                    </div>
                </div>
            </c:forEach>
            <script>
                window.onload = function () {
                    let dates = document.querySelectorAll(".my-date > i")
                    for (let i = 0; i < dates.length; i++) {
                        let d = dates[i]
                        d.innerText = moment(d.innerText).fromNow();
                    }
                }
                function  doDelete(id) {
                    if (confirm("Ban co muon xoa cau hoi nay khong ?")) {
                        window.location = "editQuestion?questionID=" +${id};
                    }
                }
            </script>
</html>
