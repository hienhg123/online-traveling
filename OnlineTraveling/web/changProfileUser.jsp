<%-- Document : userProfile Created on : Sep 17, 2022, 3:56:30 PM Author : ACER --%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8" />
        <title>User Profile</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="CSS/userProfile.css" />
    </head>

    <body>
        <%@include file="View/Header.jsp" %>
        <div class="main">
            <div class="container light-style flex-grow-1 container-p-y">
                <h1>Account Details</h1>
                <c:if test="${sessionScope.account.role == 1 || requestScope.currentID == sessionScope.account.userID}">
                    <div class="card overflow-hidden">
                        <div class="row no-gutters row-bordered row-border-light">
                            <div class="col-md-3 pt-0">
                                <div class="list-group list-group-flush account-settings-links">
                                    <a class="list-group-item list-group-item-action active" href="userProfile?userID=${requestScope.currentID}">Thông tin cá
                                        nhân</a>
                                    <span>${requestScope.mess}</span>
                                    <c:if test="${requestScope.currentID == sessionScope.account.userID}">
                                        <a class="list-group-item list-group-item-action" href="changeInfoUser?userID=${requestScope.currentID}">Cập
                                            nhật thông
                                            tin</a>
                                        <a class="list-group-item list-group-item-action" href="changePassword.jsp">Đổi mật khẩu</a>
                                        <a class="list-group-item list-group-item-action" href="roomrental?userID=${requestScope.currentID}">Lịch sử
                                            thuê phòng</a>
                                        </c:if>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <form action="changeInfoUser" method="post" class="update-form" id="update-form">
                                    <div class="tab-content">
                                        <div class="tab-pane fade active show" id="account-general">
                                            <div class="card-body media align-items-center">
                                                <img src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="" class="d-block ui-w-80" />
                                                <div class="media-body ml-4">
                                                    <label class="btn btn-outline-primary">
                                                        Upload new photo
                                                        <input type="file"
                                                               class="account-settings-fileinput" />
                                                    </label> &nbsp;
                                                </div>
                                            </div>
                                            <hr class="border-light m-0" />

                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label class="form-label">First Name</label>
                                                    <input type="text" name="firstName" class="form-control mb-1" value="${requestScope.currentUser.firstName}" />
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">Last Name</label>
                                                    <input type="text" name="lastName" class="form-control" value="${requestScope.currentUser.lastName}" />
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">Gender</label>
                                                    <c:choose>
                                                        <c:when test="${(requestScope.currentUser.gender)== false}">
                                                            <input type="radio" name="gender" value="1" /> Male
                                                            <input type="radio" name="gender" value="0" checked="checked"/> Female   
                                                        </c:when>
                                                        <c:otherwise>
                                                            <input type="radio" name="gender" value="1" checked="checked"/>Male
                                                            <input type="radio" name="gender" value="0"/>Female       
                                                        </c:otherwise>
                                                    </c:choose>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">Date of Birth</label>
                                                    <c:if test="${requestScope.currentUser.dob == null}">
                                                        <input type="text" name="dob" class="form-control" value="..." />
                                                    </c:if>
                                                    <c:if test="${requestScope.currentUser.dob != null}">
                                                        <input type="text" name="dob" class="form-control" value="${requestScope.currentUser.dob}" />
                                                    </c:if>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">Address</label>
                                                    <c:if test="${requestScope.currentUser.address == null}">
                                                        <input type="text" name="address" class="form-control" value="..." />
                                                    </c:if>
                                                    <c:if test="${requestScope.currentUser.address != null}">
                                                        <input type="text" name="address" class="form-control" value="${requestScope.currentUser.address}" />
                                                    </c:if>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">Phone</label>
                                                    <c:if test="${requestScope.currentUser.phone == null}">
                                                        <input type="text" name="phone" class="form-control mb-1" value="..." />
                                                    </c:if>
                                                    <c:if test="${requestScope.currentUser.phone != null}">
                                                        <input type="text" name="phone" class="form-control mb-1" value="${requestScope.currentUser.phone}" />
                                                    </c:if>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label">E-mail</label>
                                                    <c:if test="${requestScope.currentUser.email == null}">
                                                        <input type="text" name="email" class="form-control" value="..." readonly />
                                                    </c:if>
                                                    <c:if test="${requestScope.currentUser.email != null}">
                                                        <input type="text" name="email" class="form-control" value="${requestScope.currentUser.email}" readonly />
                                                    </c:if>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-right mt-3">
                                        <button type="submit" class="btn btn-primary">Save
                                            changes</button>&nbsp;
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${sessionScope.account.role != 1 && requestScope.currentID != sessionScope.account.userID}">
                    <h1>You are not allowed to see the information of this page</h1>
                </c:if>
            </div>
        </div>
        <%@include file="/View/Footer.jsp" %>
    </body>

</html>