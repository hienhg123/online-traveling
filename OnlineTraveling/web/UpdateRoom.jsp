<%-- 
    Document   : UpdateRoom
    Created on : Sep 17, 2022, 10:05:34 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Update RoomType</h1>
        <form action="updateroom" method="post" onsubmit="return CheckInput();">
            <h3>RoomTypeID</h3>
            <input type="number" name="romid" value="${roomid}" readonly="true">
            <br>
            <h3>RoomTypeName</h3>
            <input type="text" name="romname" value="${roomname}" id="romname">
            <br>
            <h3>Description</h3>
            <textarea name="romdes" cols="70" rows="5" id="romdes">${desc}</textarea>
            <br><br>
            <button>Update</button>
        </form>
    </body>
    <script type="text/javascript">
        function CheckInput() {
            var roomname = document.getElementById("romname").value;
            var description = document.getElementById("romdes").value;
            var msg = "";
            var flag = true;

            if (roomname === "") {
                msg += "roomname not empty.\n";
                flag = false;
            }
            if (description === "") {
                msg += "description not empty.\n";
                flag = false;
            }
            if (flag === false) {
                alert(msg);
                return false;
            }

            return true;
        }
    </script>
</html>
