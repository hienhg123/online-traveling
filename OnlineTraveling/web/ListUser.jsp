<%-- 
    Document   : ListUser
    Created on : Sep 16, 2022, 6:47:25 PM
    Author     : Dell
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList" %>
<%@page import="dal.*" %>
<%@page import="models.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <style type="text/css">
        table, th, td{
            border:1px solid #868585;
        }
        table{
            border-collapse:collapse;
            width:100%;
        }
        th, td{
            text-align:left;
            padding:10px;
        }
        table tr:nth-child(odd){
            background-color:#eee;
        }
        table tr:nth-child(even){
            background-color:white;
        }
        table tr:nth-child(1){
            background-color:black;
            color:white;
        }
    </style>
    <body>
        <br><br>
        <h1>Quản Lý Người Dùng</h1>
        <a href="home"> Return home page</a>
        <form action="searchuser" method="post" style="float: right" onsubmit="return CheckInput();">
            Form&emsp;<input type="date" name="from" id="from" value="fro">
            &emsp;=>To&emsp;<input type="date" name="to" id="to" value="tto">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
            <select name="adeactive" >
                <option value="full">FullUser</option>
                <option value="active">Active</option>
                <option value="deactive">Deactive</option>
            </select>
            <input type="text" name="searchusername" placeholder="UserName" value="${uname}">
            <button>Search</button>
        </form>
        <br><br>
        <table border="1" >
            <tr>
                <th>ID</th>
                <th>UserName</th>
                <th>Email</th>
                <th>Gender</th>
                <th>Address</th>
                <th>DOB</th>
                <th>Role</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
                <%
                   ArrayList<User> user = (ArrayList<User>) request.getAttribute("searchname");
                   for(User u : user){
                        String gen;
                        String ac;
                        if(u.isGender()==true){
                            gen = "Male";
                        }else{
                            gen = "Female";
                        }
                        if(u.isStatus()==true){
                            ac = "Active";
                        }else{
                            ac = "Deactive";
                        }
                    if(u.getRole()!=1){
                
                %>
                <tr>
                    <td><%=u.getUserID()%></td>
                    <td><%=u.getUserName()%></td>
                    <td><%=u.getEmail()%></td>
                    <td><%=gen%></td>
                    <td><%=u.getAddress()%></td>
                    <td><%=u.getDob()%></td>
                    <td><%=u.getRole()%></td>
                    <td><%=ac%></td>
                    <% if(u.isStatus() != true){%>
                    <td><a href="active?pid=<%=u.getUserID()%>">Active</a></td>
                    <%
                        }else{
                    %>
                    <td><a href="deactive?pid=<%=u.getUserID()%>">Deactive</a></td>
                    <%
                        }
                    %>
                </tr>

                <%       
                    }
                }
                %>

        </table>
    </body>
    <script type="text/javascript">
        function CheckInput() {
            var from = document.getElementById("from").value;
            var to = document.getElementById("to").value;
            var msg = "";
            var flag = true;

            if (from === "" &&  to !== "") {
                msg += "from not empty.\n";
                flag = false;
            }
            if (to === "" && from !== "") {
                msg += "to not empty.\n";
                flag = false;
            }
            if (flag === false) {
                alert(msg);
                return false;
            }

            return true;
        }
    </script>
</html>

