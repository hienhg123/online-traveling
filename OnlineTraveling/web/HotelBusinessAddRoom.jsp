<%-- Document : Register.jsp Created on : Sep 15, 2022, 9:08:21 PM Author : admin --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <!DOCTYPE html>
            <html>

            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                <c:set var="x" value="${requestScope.hotel}" />
                <title>Đăng kí phòng</title>

                <!-- Font Icon -->
                <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">

                <!-- Main css -->
                <link rel="stylesheet" href="CSS/register.css">
            </head>

            <body>
                <jsp:include page="View/Header.jsp" />
                    <div class="main">
                        <!-- Sign up form -->
                        <section class="signup">
                            <div class="container">
                                <div class="signup-content">
                                    <div class="signup-form">
                                        <h2 class="form-title">Tạo phòng mới</h2>
                                        <h2><span>${requestScope.message}</span></h2>
                                        <form action="hotelRoomCreate" method="post" class="register-form" id="register-form" name="addNew" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="hotelId"><i
                                                        class="zmdi zmdi-account material-icons-name"></i>Hotel
                                                    ID</label>
                                                <input type="number" name="hotelId" id="hotelId" value="${x.getHotelId()}" min="1" max="${x.getHotelId()}"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="roomname"><i
                                                        class="zmdi zmdi-account material-icons-name"></i>Tên
                                                    phòng</label>
                                                <input type="text" name="roomname" maxlength="50" id="roomname" placeholder="Nhập..." />
                                            </div>
                                            <div class="form-group">
                                                <label for="description"><i
                                                        class="zmdi zmdi-account material-icons-name"></i>Mô tả</label>
                                                <input type="textarea" name="description" maxlength="250" id="description" placeholder="Nhập..." />
                                            </div>
                                            <div class="form-group">
                                                <label for="image"><i
                                                        class="zmdi zmdi-account material-icons-name">Image</i></label>
                                                <input type="file" name="image" id="image">
                                            </div>
                                            <div class="form-group">
                                                <label for="price"><i
                                                        class="zmdi zmdi-account material-icons-name"></i>Giá</label>
                                                <input type="text" onkeypress='validate(event)' name="price" id="price" placeholder="Nhập..." />
                                            </div>
                                            <div class="form-group">
                                                <label for="size"><i
                                                        class="zmdi zmdi-account material-icons-name"></i>Kích
                                                    thước</label>
                                                <input type="text" onkeypress='validate(event)' name="size" id="size" placeholder="Nhập..." />
                                            </div>
                                            <div class="form-group">
                                                <div class="select-box">
                                                    <label for="roomtype" class="zmdi zmdi-account material-icons-name"><span
                                                            class="label-desc">Room type</span> </label>
                                                    <select id="roomtype" name="roomtype" class="select">
                                                        <option value="1">Standard</option>
                                                        <option value="2">Superior</option>
                                                        <option value="3">Deluxe</option>
                                                        <option value="4">Suite</option>
                                                        <option value="5">Single Bedroom</option>
                                                        <option value="6">Twin Bedroom</option>
                                                        <option value="7">Double Bedroom</option>
                                                        <option value="8">Triple Bedroom</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="quantity"><i
                                                        class="zmdi zmdi-account material-icons-name"></i>Số
                                                    lượng</label>
                                                <input type="number" min="0" name="quantity" id="quantity" placeholder="Nhập..." />
                                            </div>
                                            <div class="form-group form-button">
                                                <input type="submit" name="submit" value="Thêm" onclick="return checkFullForRoom()" class="form-submit">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="signup-image">
                                        <figure>
                                            <img src="images/personal-image.jpg" alt="sing up image">
                                        </figure>
                                        <a href="hotelBusiness?busiID=${sessionScope.business.busiID}" class="signup-image-link">
                                            <h3>Danh sách khách sạn</h3>
                                        </a>
                                            <a href="hotelBusinessRoom?hotelId=${x.getHotelId()}" class="signup-image-link">
                                            <h3>Danh sách phòng</h3>
                                        </a>
                                        <span><br><br></span>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <jsp:include page="View/Footer.jsp" />
                        <script src="JS/AddNewRoom_Events.js"></script>
            </body>

            </html>