<%-- Document : home.jsp Created on : Sep 13, 2022, 9:26:17 PM Author : admin --%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
        <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
        <link rel="stylesheet" href="CSS/home.css" />
        <title>Book'nStay</title>
    </head>

    <body>
        <!--Footer-->
        <section class="footer">

            <div class="box-container">

                <div class="box">
                    <h3 class="footer-title">about us</h3>
                    <p class="credit"><span> Book'nStay </span> was founded by 5 amazing students from FPT University. Since then the project made a huge impact to all student by its convenient and friendly design, fast and reliable. We are keeping forward to increase
                        our branch soon for us to reach to Globe. Thanks for all your support!</p>
                    <p class="credit"><span> Book'nStay </span> Your Best Booking Agency</p>
                </div>
                <div class="box">
                    <h3 class="footer-title">Map</h3>
                    <p class="credit"></p>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951.5862271318597!2d105.52510219043992!3d21.013211142486092!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31345b465a4e65fb%3A0xaae6040cfabe8fe!2zVHLGsOG7nW5nIMSQ4bqhaSBI4buNYyBGUFQ!5e1!3m2!1svi!2s!4v1663348823014!5m2!1svi!2s"
                            width="550" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
                <div class="box">
                    <h3 class="footer-title">quick links</h3>
                    <p class="credit"></p>
                    <a href="Home.jsp">Home</a>
                    <a href="Reservation.jsp">Reservations</a>
                    <a href="CarRental.jsp">Car Rental</a>
                    <a href="Location.jsp">Location</a>
                </div>
                <div class="box">
                    <h3 class="footer-title">follow us</h3>
                    <p class="credit"></p>
                    <a href="#">facebook</a>
                    <a href="#">instagram</a>
                    <a href="#">twitter</a>
                    <a href="#">linkedin</a>
                </div>

            </div>

            <h1 class="credit"> created by <span> Book'nStay </span> | No-copy right! </h1>

        </section>
    </div>

    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>   
</body>

</html>