<%-- Document : home.jsp Created on : Sep 13, 2022, 9:26:17 PM Author : admin --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <!DOCTYPE html>
            <html>

            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
                <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
                <link href='https://fonts.googleapis.com/css?family=Inter ' rel='stylesheet'>
                <link rel="stylesheet" href="../CSS/home.css">
                <title>Book'nStay</title>
            </head>

            <body>
                <header>
                    <div id="menu-bar" class="fas fa-bars"></div>
                    <div class="col-md-2">
                        <a href="home" class="logo">
                            <img src="images/Logo.png" alt="alt" />
                        </a>
                    </div>
                    <nav class="navbar">
                        <a href="home">Trang chủ</a>
                        <a href="#">Lưu trú</a>
                        <a href="#">Thuê xe</a>
                        <a href="#">Địa điểm </a>
                    </nav>

                    <c:choose>
                        <c:when test="${sessionScope.account == null && sessionScope.business == null}">
                            <div class="icons">
                                <a href="login"><i class="fas fa-user" id="login-btn"></i></a>
                                <a href="businessLogin"><i class="fas fa-building" id="business-login-btn"></i></a>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${sessionScope.account != null}">
                                    <div class="icons">
                                        <a href="logout"> <i class="fas fa-power-off" id="logout-btn"></i></a>
                                        <a href="userProfile?userID=${sessionScope.account.userID}"><i
                                                class="fas fa-user-cog"></i></a>
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <div class="icons">
                                        <a href="businessLogout"> <i class="fas fa-power-off" id="logout-btn"></i></a>
                                        <a href="hotelBusiness?busiID=${sessionScope.business.busiID}"><i
                                                class="fas fa-door-open"></i></a>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                </header>
                <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
                <!--<script src="../JS/Home_Events.js"></script>-->
            </body>

            </html>