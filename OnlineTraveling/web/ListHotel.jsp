<%-- Document : ListHotel Created on : Sep 30, 2022, 10:06:56 PM Author : HONG QUAN --%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hotel List</title>
        <link rel="stylesheet" href="CSS/rentailHistory.css" />
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    </head>

    <body>
        <%@include file="View/Header.jsp" %>
        <c:if test="${requestScope.listHo == null}">
            <h2>${requestScope.error}</h2>
        </c:if>
        <c:if test="${requestScope.listHo != null}">
            <div class="container">
                <div class ="row">
                    <div class="col-md-2">
                        </br></br></br></br></br></br></br></br></br>                                    
                        <form action="hotel" method="post">
                            <c:if test="${sessionScope.fromDate != null}">
                                <h2>Theo giá</h2>
                                <label for="price1">VND 0 - VND 1.000.000</label>
                                <input type="checkbox" id="price1" name="price1" value="price1">
                                <label for="price2">VND 1.000.000 - VND 2.000.000</label>
                                <input type="checkbox" id="price2" name="price2" value="price2">
                                <label for="price3">VND 2.000.000 - VND 3.000.000</label>
                                <input type="checkbox" id="price3" name="price3" value="price3">
                                <label for="price4">VND 3.000.000 - VND 4.000.000</label>
                                <input type="checkbox" id="price4" name="price4" value="price4">
                                <label for="price5">VND 4.000.000+</label>
                                <input type="checkbox" id="price5" name="price5" value="price5">
                            </c:if>
                            <br/><br/>
                            <h2>Tiện nghi</h2>
                            <label for="1">Wi-fi miễn phí</label>
                            <input type="checkbox" id="1" name="1" value="1"><br/>
                            <label for="2">Chỗ để xe</label>
                            <input type="checkbox" id="2" name="2" value="2"><br/>
                            <label for="3">Phòng gia đình</label>
                            <input type="checkbox" id="3" name="3" value="3"><br/>
                            <label for="4">Lễ tân 24/7</label>
                            <input type="checkbox" id="4" name="4" value="4"><br/>
                            <label for="5">Nhà hàng</label>
                            <input type="checkbox" id="5" name="5" value="5"><br/>
                            <label for="6">Hồ bơi</label>
                            <input type="checkbox" id="6" name="6" value="6"><br/>
                            <label for="7">Cho phép vật nuôi</label>
                            <input type="checkbox" id="7" name="7" value="7">
                            <br/><br/>
                            <h2>Tiện nghi phòng</h2>
                            <label for="ra1">Bồn tắm</label>
                            <input type="checkbox" id="ra1" name="ra1" value="1"><br/>
                            <label for="ra2">Điều hòa không khí</label>
                            <input type="checkbox" id="ra2" name="ra2" value="2"><br/>
                            <label for="ra3">Phòng tắm riêng</label>
                            <input type="checkbox" id="ra3" name="ra3" value="3"><br/>
                            <label for="ra4">Ban công</label>
                            <input type="checkbox" id="ra4" name="ra4" value="4"><br/>
                            <label for="ra5">Máy giặt</label>
                            <input type="checkbox" id="ra5" name="ra5" value="5"><br/>
                            <label for="ra6">Hệ thống cách ấm</label>
                            <input type="checkbox" id="ra6" name="ra6" value="6"><br/>
                            <label for="ra7">TV màn hình phẳng</label>
                            <input type="checkbox" id="ra7" name="ra7" value="7">
                            <br/>
                            <input type="hidden" name="destination" value="${sessionScope.destinationID}">
                            <input type="submit" value="Search">
                        </form>
                    </div>
                    <div class="main col-md-10">
                        <div class="container light-style flex-grow-1 container-p-y">
                            <div id="wrapper">
                                <h1>Danh sách khách sạn</h1>
                                <table id="keywords" cellspacing="0" cellpadding="0">
                                    <thead>
                                        <tr>
                                            <th><span>View</span></th>
                                            <th><span>Name</span></th>
                                            <th><span>Address</span></th>
                                            <th><span>Description</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${requestScope.listHo}" var="li">
                                            <tr>
                                                <td class="img-border">
                                                    <div><img src="${li.getHotelImage()}" alt="alt" /></div>
                                                </td>
                                                <td><a class="link" href="/OnlineTraveling/hotelDetail?hotelId=${li.getHotelId()}">${li.getHotelName()}</a>
                                                </td>
                                                <td>${li.getHotelAddress()}</td>
                                                <td>${li.getDescription()}</td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>    
            </div>    
        </c:if>
        <%@include file="View/Footer.jsp" %>
    </body>

</html>