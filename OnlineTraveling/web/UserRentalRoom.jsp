<%-- 
    Document   : UserRentalRoom
    Created on : Sep 30, 2022, 8:56:46 PM
    Author     : Dell
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.ArrayList" %>
<%@page import="dal.*" %>
<%@page import="models.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="CSS/rentailHistory.css" />
        <title>Book History</title>
    </head>

    <body>
        <%@include file="/View/Header.jsp" %>
        <div class="main">
            <div class="container light-style flex-grow-1 container-p-y"> 
                <div id="wrapper">
                    <h1>Lịch Sử Phòng Đã Thuê</h1>
                    <button class="button-28" role="button"><a class="link" href="bookedRoom">Chờ xác nhận</a></button>
                    <button class="button-28" role="button"><a class="link" href="bookCancelled">Hủy Phòng</a></button>
                    <table id="keywords" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th><span>Khách sạn</span></th>
                                <th><span>FromDate</span></th>
                                <th><span>ToDate</span></th>
                                <th><span>Status</span></th>
                                <th><span>RoomName</span></th>
                                <th><span>RoomPrice</span></th>
                                <th><span>Review</span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${bookListT}" var="r">
                                <tr>
                                    <td>${r.getHotelName()}</td>
                                    <td>${r.getFromDate()}</td>
                                    <td>${r.getToDate()}</td>
                                    <td>${r.getStatus()}</td>
                                    <td>${r.getRoomName()}</td>
                                    <td>${r.getPrice()}</td>
                                    <td><a href="submitRating?BookingId=${r.bookingId}">Review</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <%@include file="/View/Footer.jsp" %>
        <script src="JS/CheckEvents.js"></script>
    </body>
</html>
