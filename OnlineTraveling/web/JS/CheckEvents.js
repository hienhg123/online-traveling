let message = document.getElementById("message");

function checkFullForUser() {
    let checkUserAge = checkAge();
    let checkPass = checkPassword();
    let checkEmail = checkEmailLength();
    let checkUser = checkUserName();
    let checkFirstLastName = checkName();
    //check if these 5 are correct
    if (
        checkPass === true &&
        checkEmail === true &&
        checkUser === true &&
        checkFirstLastName === true &&
        checkUserAge === true
    ) {
        document.getElementById("send").type = "submit";
    }
}

function checkFullForUser() {
    let checkUserAge = checkAge();
    let checkPass = checkPassword();
    let checkEmail = checkEmailLength();
    let checkUser = checkUserName();
    let checkFirstLastName = checkName();
    //check if these 5 are correct
    if (
        checkPass === true &&
        checkEmail === true &&
        checkUser === true &&
        checkFirstLastName === true &&
        checkUserAge === true
    ) {
        document.getElementById("send").type = "submit";
    }
}
//check user age
function checkAge() {
    let dob = document.getElementById("dob").value;
    let today = new Date().getFullYear();
    var userDob = new Date(dob);
    let age = today - userDob.getFullYear();
    //check if user are at least 18 or not
    if (age > 18) {
        return true;
    } else {
        message.textContent = "You must be at least 18";
        return false;
    }
}
//check password
function checkPassword() {
    let password = document.getElementById("newPass").value;
    let confirm = document.getElementById("confirm").value;

    if (password.length > 7 && password.length < 26) {
        if (password === confirm) {
            return true;
        } else {
            message.textContent = "Password does not match";
            return false;
        }
    } else {
        message.textContent =
            "Password is invalid, must be greater than 8 and less than 25";
    }
}
//check phone number
function checkPhonenumber() {
    let phonenumber = document.getElementById("phonenumber").value;
    let regex = new RegExp("(84|0[3|5|7|8|9])+([0-9]{8})");
    if (phonenumber.length < 11) {
        if (regex.test(phonenumber)) {
            return true;
        }
    } else {
        message.textContent = "Incorrect phonenumber format";
        return false;
    }
}
//check the length of email
function checkEmailLength() {
    let email = document.getElementById("email").value;
    //check email length
    if (email.length > 9 && email.length < 51) {
        return true;
    } else {
        message.textContent =
            "Email is invalid, must be greate than 10 and smaller than 50 characters";
        return false;
    }
}
//check the length of username
function checkUserName() {
    let username = document.getElementById("username").value;
    //check username length
    if (username.length > 2 && username.length < 26) {
        return true;
    } else {
        message.textContent =
            "Invalid username, must be greater than 3 and smaller than 25";
        return false;
    }
}
//check user real name
function checkName() {
    let firstName = document.getElementById("firstName").value;
    let lastName = document.getElementById("lastName").value;
    //check the length
    if (firstName.length > 1 && firstName.length < 26) {
        //check user last name
        if (lastName.length > 1 && lastName.length < 26) {
            return true;
        }
    } else {
        message.textContent =
            "Invalid firstname/lastname, must be greater than 3 and smaller than 25";
        return false;
    }
}
//check business name
function checkBusiessName() {
    let busiName = document.getElementById("busiName").value;
    //check the business name length
    if (busiName.length > 0 && busiName.length < 26) {
        return true;
    } else {
        message.textContent =
            "Invalid business name, must greater than 0 and less than 25";
        return false;
    }
}
//check password for change
function checkPassToChange() {
    let password = document.getElementById("newPass").value;
    let confirm = document.getElementById("confirm").value;

    if (password.length > 7 && password.length < 26) {
        if (password === confirm) {
            document.getElementById("send").type = "submit";
            return true;
        } else {
            message.textContent = "Password does not match";
            return false;
        }
    } else {
        message.textContent =
            "Password is invalid, must be greater than 8 and less than 25";
    }
}
//check email to send
function checkEmailLengthToSend() {
    let email = document.getElementById("email").value;
    //check email length
    if (email.length > 9 && email.length < 51) {
        document.getElementById("send").type = "submit";
        return true;
    } else {
        message.textContent =
            "Email is invalid, must be greate than 10 and smaller than 50 characters";
        return false;
    }
}
//compare date
function compareDate() {
    let from_raw = document.getElementById("dateFrom").value;
    let to_raw = document.getElementById("dateTo").value;
    let from = new Date(from_raw);
    let to = new Date(to_raw);
    //check if to is behind from
    if (to.getTime() < from.getTime()) {
        return true;
    } else {
        return false;
    }
}
//check date
function checkDate() {
    let check = compareDate();
    if (check === true) {
        document.getElementById("send").type = "submit";
    } else {
        message.textContent = "Ngày nhận phòng phải sau ngày trả phòng";
    }
}

// Login animations
const inputs = document.querySelectorAll(".input");

function addcl() {
    let parent = this.parentNode.parentNode;
    parent.classList.add("focus");
}

function remcl() {
    let parent = this.parentNode.parentNode;
    if (this.value == "") {
        parent.classList.remove("focus");
    }
}

inputs.forEach((input) => {
    input.addEventListener("focus", addcl);
    input.addEventListener("blur", remcl);
});

$(function() {
    $("#keywords").tablesorter();
});