function validate(evt) {
    var theEvent = evt || window.event;

    // Handle paste
    if (theEvent.type === "paste") {
        key = event.clipboardData.getData("text/plain");
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault)
            theEvent.preventDefault();
    }
}

function checkFullForRoom(ele) {
    var hotelId = document.getElementById("hotelId").value;
    var name = document.getElementById("roomname").value;
    var description = document.getElementById("description").value;
    var image = document.getElementById("image").value;
    var price = document.getElementById("price").value;
    var size = document.getElementById("size").value;
    var roomtype = document.getElementById("roomtype").value;
    var quantity = document.getElementById("quantity").value;
    var msg = "";
    var flag = true;
    console.log(name + description + image + price + size + roomtype + quantity);
    if (name === "") {
        msg += "Room name not empty.\n";
        flag = false;
    }
    if (image === "") {
        msg += "Image not empty.\n";
        flag = false;
    }
    if (description === "") {
        msg += "Description not empty.\n";
        flag = false;
    }
    if (price === "") {
        msg += "Price not empty.\n";
        flag = false;
    }
    if (size === "") {
        msg += "Size not empty.\n";
        flag = false;
    }
    if (roomtype === "") {
        msg += "Roomtype not empty.\n";
        flag = false;
    }
    if (quantity === "") {
        msg += "Quantity not empty.\n";
        flag = false;
    }
    if (flag === false) {
        alert(msg);
        return false;
    }
    if (flag === true) {
        if (confirm("Are you sure you want to add this room?") === true) {
            return true;
        } else {
            return false;
        }
    }
}