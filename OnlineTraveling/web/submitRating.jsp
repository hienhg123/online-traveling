<%-- 
    Document   : submitRating
    Created on : Oct 2, 2022, 9:46:29 PM
    Author     : ACER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
        <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include  file="View/Header.jsp" %>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        <h1>Submit rating for your booking</h1>
        <p>Booking id ${requestScope.booking.bookingId}</p>
        <p></p>
        <form action="submitRating" method="post">
            <!--                    <input type="text" class="form-control rvContentAdd" aria-label="Small" aria-describedby="inputGroup-sizing-sm" name="content" autocomplete="off">-->
            <textarea name="comment" class="descBox"></textarea>
            <div class="rating-wrap">
                <div class="center">
                    <fieldset class="rating">
                        <input type="radio" id="star5" name="rating" value="5" checked/><label for="star5" class="full" title="Awesome"></label>
                        <input type="radio" id="star4.5" name="rating" value="4.5"/><label for="star4.5" class="half"></label>
                        <input type="radio" id="star4" name="rating" value="4"/><label for="star4" class="full"></label>
                        <input type="radio" id="star3.5" name="rating" value="3.5"/><label for="star3.5" class="half"></label>
                        <input type="radio" id="star3" name="rating" value="3"/><label for="star3" class="full"></label>
                        <input type="radio" id="star2.5" name="rating" value="2.5"/><label for="star2.5" class="half"></label>
                        <input type="radio" id="star2" name="rating" value="2"/><label for="star2" class="full"></label>
                        <input type="radio" id="star1.5" name="rating" value="1.5"/><label for="star1.5" class="half"></label>
                        <input type="radio" id="star1" name="rating" value="1"/><label for="star1" class="full"></label>
                        <input type="radio" id="star0.5" name="rating" value="0.5"/><label for="star0.5" class="half"></label>
                    </fieldset>
                </div>

                <h4 id="rating-value"></h4>
            </div>

            <input type="hidden" name="userID" value="${sessionScope.user.userID}"/>
            <input type="hidden" name="bookingId" value="${requestScope.booking.bookingId}"/>
            <button type="submit" class="submitbtn btn btn-dark">Submit</button>
        </form>
        <script src="Javascript/star-ratings.js"></script>
        <%@include  file="View/Footer.jsp" %>    
    </body>
</html>
