<%-- Document : ListHotel Created on : Sep 30, 2022, 10:06:56 PM Author : HONG QUAN --%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Rate Plan List</title>
        <link rel="stylesheet" href="CSS/rentailHistory.css" />
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    </head>

    <body>
        <%@include file="View/Header.jsp" %>
        <c:if test="${requestScope.listRoom == null}">
            <h2>${requestScope.error}</h2>
        </c:if>
        <c:if test="${requestScope.listRoom != null}">
            <div class="main">
                <div class="container light-style flex-grow-1 container-p-y">
                    <div id="wrapper">
                        <h1>Danh sách phòng</h1>
                        <c:set var="x" value="${requestScope.hotel}" />
                        <a class="link" href="/OnlineTraveling/hotelRoomCreate?hotelId=${x.getHotelId()}"><button
                                class="btn btn-primary">Tạo mới</button></a>
                        <a class="link" href="ratePlanList?hotelId=${requestScope.hotelId}"><button class="btn btn-primary">Tùy chỉnh gói giá</button></a>
                        <a class="link" href="cancelPolicy?hotelID=${requestScope.hotelId}"><button class="btn btn-primary">Chính sách hủy phòng</button></a>
                        <table id="keywords" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th><span>View</span></th>
                                    <th><span>Type</span></th>
                                    <th><span>Size</span></th>
                                    <th><span>Description</span></th>
                                    <th><span>Price</span></th>
                                    <th><span>Quantity</span></th>
                                    <th><span>Action</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.listRoom}" var="li">
                                    <tr>
                                        <td class="img-border">
                                            <div><img src="${li.getRoomImage()}" alt="alt" /></div>
                                        </td>
                                        <td>${li.getRoomName()}</td>
                                        <td>${li.getSize()}</td>
                                        <td>${li.getRoomDescription()}</td>
                                        <td>${li.getPrice()}</td>
                                        <td>${li.getQuantity()}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button class="btn btn-success">Update</button>
                                                <button class="btn btn-danger" style="background-color: red">Delete</button>
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </c:if>
        <%@include file="View/Footer.jsp" %>
    </body>

</html>