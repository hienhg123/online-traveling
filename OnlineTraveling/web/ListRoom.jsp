<%-- 
    Document   : ListRoom
    Created on : Sep 16, 2022, 6:47:25 PM
    Author     : Dell
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList" %>
<%@page import="dal.*" %>
<%@page import="models.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <style type="text/css">
        table, th, td{
            border:1px solid #868585;
        }
        table{
            border-collapse:collapse;
            width:100%;
        }
        th, td{
            text-align:left;
            padding:10px;
        }
        table tr:nth-child(odd){
            background-color:#eee;
        }
        table tr:nth-child(even){
            background-color:white;
        }
        table tr:nth-child(1){
            background-color:black;
            color:white;
        }
    </style>
    <body>
        <br><br>
        <h1>Quản Lý Room</h1>
        <a href="home"> Return home page</a>
        <br><br>
        <form action="searchroom" method="post" style="float: right">
            <input type="text" name="roomname" placeholder="RoomName" value="${rname}">
            <button>Search</button>
        </form>
            <br><br>
        <table border="1">
            <tr>
                <th>roomTypeID</th>
                <th>roomTypeName</th>
                <th>description</th>
                <th>Action</th>
            </tr>
            <%
               ArrayList<RoomType> room = (ArrayList<RoomType>) request.getAttribute("lromm");
               for(Room r : room){
            %>
            <tr>
                <td><%=r.getRoomID()%></td>
                <td><%=r.getRoomName()%></td>
                <td><%=r.getDescription()%></td>
                <td><a href="updateRoom?rid=<%=r.getRoomID()%>">Update</a></td>
            </tr>
            <%       
                }
            %>
        </table>
    </body>
</html>

