<%-- 
    Document   : HotelDetail
    Created on : Sep 27, 2022, 1:44:23 PM
    Author     : HONG QUAN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Book'nStay</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="CSS/hotelDetail.css">
        <style>
            table, th, td {
                border: 1px solid black;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <c:set var = "ahot" value = "${requestScope.aHotel}"/>
            <div class="row">
                <div class="col-md-4">
                    <div><button>Đặt ngay</button></div>
                    <div class="des">
                        <img class="des" src="images/dest1.jpg" alt="">
                    </div>
                    <div>Một trong những khu vực đẹp nhất ở Hà Nội</div>
                    <div>
                        <h5>Bạn thích chỗ nghỉ này nhưng vẫn chưa chắc chắn</h5>

                        <div><button>Xem các khách sạn tương tự</button></div>
                    </div>
                    <br><br>
                    <div class="container">
                        <h3 style="font-weight: bold">Danh sách gói giá</h3>
                        <table class="table table-striped">
                            <thead> 
                                <tr>
                                    <th scope="col">Tên gói giá</th>
                                    <th scope="col">Loại gói giá</th>
                                    <th scope="col">Phần trăm khuyến mãi</th>
                                    <th scope="col">Miêu tả</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.planList}" var="p">
                                    <tr>
                                        <td>${p.planName}</td>
                                        <td>
                                            <c:if test="${p.planType == 1}">
                                                Lần đầu đặt
                                            </c:if>
                                            <c:if test="${p.planType == 2}">
                                                Theo số ngày ở
                                            </c:if>
                                            <c:if test="${p.planType == 3}">
                                                Theo số ngày đặt trước
                                            </c:if>


                                        </td>
                                        <td>${p.percentage}</td>
                                        <td>
                                            <c:if test="${p.planType == 1}">
                                                Lần đầu đặt sẽ khuyến mãi ${p.percentage}%
                                            </c:if>
                                            <c:if test="${p.planType == 2}">
                                                Thuê trên <span style="font-weight: bold">${p.day}</span> đêm sẽ được khuyến mãi <span style="font-weight: bold;">${p.percentage}%</span>
                                            </c:if>
                                            <c:if test="${p.planType == 3}">
                                                Thuê trước <span style="font-weight: bold">${p.day}</span> ngày nhận phòng sẽ được khuyến mãi <span style="font-weight: bold;">${p.percentage}%</span>
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="col-md-8 row">
                    <h1>Thông tin khách sạn</h1>
                    <div class="col-md-6">${aHotel.hotelName}<i class="fa-solid fa-star"><span>${aHotel.avgRating}</span></i></div>
                    <div class="col-md-6">${aHotel.getHotelAddress()}</div>
                    <div class="col-md-6"><button>Đặt ngay</button></div>
                    <div class="col-md-12" ><img class="des"  src="${aHotel.getHotelImage()}" alt="" style="width: 50%"></div>
                    <div class="col-md-12">${aHotel.getDescription()}</div>
                    <c:if test="${requestScope.firstBookAvailable == true}">
                        <div class="col-md-12" style="color: blue; font-size: 30px;" >Có khuyến mãi book lần đầu</div>
                    </c:if>
                    <div class="col-md-12">
                        <br><br><br>
                        <h2>Phòng trống</h2>
                        <div class="col-md-8 row">
                            Bạn muốn nghỉ tại ${aHotel.hotelName} vào lúc nào
                            <form action="hotelDetail?hotelId=${aHotel.hotelId}">
                                <div class="col-md-8 row">
                                    <p>Điền ngày nhận và trả phòng để đặt</p>
                                    <c:if test="${requestScope.firstBook == true && requestScope.firstBookPlan != null}">
                                        <p class="firstTimeBooking">Bạn được sale ${requestScope.firstBookPlan.percentage}% khi đặt phòng lần đầu ở khách sạn này</p>
                                    </c:if>
                                    <c:if test="${requestScope.longStayPlan != null}">
                                        <p class="firstTimeBooking">Bạn được sale ${requestScope.longStayPlan.percentage}% nếu thuê trên ${requestScope.longStayPlan.day} đêm ở khách sạn này</p>
                                    </c:if>
                                    <div class="col-md-6">Ngày nhận phòng</div>
                                    <div class="col-md-6">Ngày trả phòng</div>
                                    <div class="col-md-6"> <input type="date" value="${sessionScope.fromDate}" name="FromDate"/></div>
                                    <div class="col-md-6"><input type="date" value="${sessionScope.toDate}" name="ToDate"/></div>

                                    <div class="col-md-8 mt-auto"><input type="submit" value="Kiểm tra phòng trống"/></div>
                                    <input type="hidden" value="${aHotel.hotelId}" name="hotelId"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <!-- Result Table -->
                    <c:if test="${sessionScope.fromDate == null && sessionScope.toDate == null}">
                        <div class="col-md-8 row">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Loại phòng nghỉ</th>
                                        <th scope="col">Phù hơp cho</th>
                                        <th scope="col">Đánh giá </th>
                                        <th scope="col">Thông tin phòng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${listRoom}" var="cRoom">
                                        <tr>
                                            <td>${cRoom.getRoomName()}</td>
                                            <td>${cRoom.getRoomType().getRoomTypeName()}</td>
                                            <td>${cRoom.avgRating}<i class="fa-solid fa-star"></i></td>
                                            <td><a href="/OnlineTraveling/RoomDetail?roomId=${cRoom.getRoomId()}"><button>Thông tin phòng</button></a></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>

                            </table>
                        </div>
                    </c:if>
                    <br>
                </div>


                <c:if test="${sessionScope.fromDate != null && sessionScope.toDate != null}">
            <!--<p>Session FromDate : ${sessionScope.fromDate}</p>
            <p>Session ToDate : ${sessionScope.toDate}</p>-->
                    <!--<form action="bookingConfirmation">
                        <div class="col-md-12 row">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Tên loại phòng</th>
                                        <th scope="col">Giá</th>
                                        <th scope="col">Số phòng còn lại</th>
                                        <th scope="col">Thông tin phòng</th>
                                        <th scope="col">Đánh giá</th>
                                        <th scope="col">Đặt phòng</th>
                                    </tr>
                                </thead>

                                <tbody>
                    <c:set var="discount" value="${requestScope.discount}"/>
                    <c:forEach var="r" items="${listRoom}">
                        <tr>
                            <td>${r.getRoomName()}</td>

                            <td>${r.price - r.price*discount/100} (Sale ${discount}%)</td>
                            <td>${r.available}</td>
                            <td><a href="/OnlineTraveling/RoomDetail?roomId=${r.getRoomId()}">Thông tin phòng</a></td>
                            <td>${r.avgRating}</td>
                        <c:if test="${sessionScope.account != null}">
                            <td>
                                <input type="hidden" name="rid" value="${r.roomId}">
                                <input type="number" name="amt" min="0" max="${r.available}" value="0" >
                            </td>
                        </c:if>
                        <c:if test="${sessionScope.account == null}">
                            <td>Đăng nhập để đặt phòng</td>
                        </c:if>
                    </tr>

                    </c:forEach>
                </tbody>
            </table>
            <input type="hidden" value="${sessionScope.fromDate}" name="fromDate"/>
            <input type="hidden" value="${sessionScope.toDate}" name="toDate"/>
                    <c:if test="${sessionScope.account == null}">
                        <p>Đăng nhập để đặt phòng</p>
                    </c:if>
                    <c:if test="${sessionScope.account != null}">
                        <input type="submit" value="Đặt phòng"/>
                    </c:if>
                </div>
            </form>
                </c:if>
                -->

                <c:if test="${sessionScope.fromDate != null && sessionScope.toDate != null}">
                        <!--<p>Session FromDate : ${sessionScope.fromDate}</p>
                        <p>Session ToDate : ${sessionScope.toDate}</p>-->
                    <form action="bookingConfirmation">
                        <div class="col-md-12 row">
                            <table class="table table-striped" style="border-collapse: collapse;">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Tên loại phòng</th>
                                        <th scope="col">Số phòng còn lại</th>
                                        <th scope="col">Giá</th>
                                        <th scope="col">Lựa chọn</th>
                                        <th scope="col">Đánh giá</th>
                                        <th scope="col">Đặt phòng</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <c:set var="discount" value="${requestScope.discount}"/>
                                    <c:forEach var="r" items="${listRoom}">
                                        <c:forEach var="rp" items="${roomPlanList}">


                                            <tr>
                                                <td> <a href="/OnlineTraveling/RoomDetail?roomId=${r.getRoomId()}">${r.getRoomName()}</td>
                                                <td>${r.available}</td>
                                                <td><c:if test="${rp.percentage > 0}">
                                                        <del style="color:red;">${r.price} VND</del>

                                                        <p>${r.price - r.price*rp.percentage/100} (Sale ${rp.percentage}%)
                                                        </c:if>
                                                        <c:if test="${rp.percentage <= 0}"><p>${r.price - r.price*rp.percentage/100}</c:if>
                                                        <p></td>
                                                    <td>
                                                    <c:if test="${rp.percentage > 0}"><p class="good">Giảm: ${rp.percentage}%, tiết kiệm cho bạn ${r.price - r.price * (100-rp.percentage)/100} VND</p></c:if>
                                                    <c:if test="${rp.meal == true}"><p class="good">Bao gồm bữa sáng ngon</p></c:if>
                                                    <c:if test="${rp.availablePayment == 1}"><p class="normal">Thanh toán tại nơi</p></c:if>
                                                    <c:if test="${rp.availablePayment == 2}"><p class="normal">Thanh toán ngay</p></c:if>
                                                    <c:if test="${rp.availablePayment == 3}"><p class="good">Thanh toán ngay hoặc tại nơi</p></c:if>
                                                    <c:if test="${rp.cancellationPolicy == 'Non-refundable'}"><p class="normal">Không hoàn tiền</p></c:if>
                                                    <c:if test="${rp.cancellationPolicy == 'Flexible'}"><p class="normal">Hoàn tiền theo chính sách của khách sạn</p></c:if>
                                                    </td>
                                                    <td>${r.avgRating}</td>
                                                <c:if test="${sessionScope.account != null}">
                                                    <td>
                                                        <input type="hidden" name="rid" value="${r.roomId}">
                                                        <input type="number" name="amt" min="0" max="${r.available}" value="0" >
                                                        <input type="hidden" name="rpid" value="${rp.planId}">
                                                    </td>
                                                </c:if>
                                                <c:if test="${sessionScope.account == null}">
                                                    <td>Đăng nhập để đặt phòng</td>
                                                </c:if>
                                            </tr>

                                        </c:forEach>
                                        <tr><td colspan="6"> </td></tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                            <h3 style="color: red">${requestScope.error}</h3>

                            <input type="hidden" value="${sessionScope.fromDate}" name="fromDate"/>
                            <input type="hidden" value="${sessionScope.toDate}" name="toDate"/>
                            <c:if test="${sessionScope.account == null}">
                                <p>Đăng nhập để đặt phòng</p>
                            </c:if>
                            <c:if test="${sessionScope.account != null}">
                                <input type="submit" value="Đặt phòng"/>
                            </c:if>
                        </div>
                    </form>
                </c:if>


                <div class="col-md-12 row">

                    <h2 style="margin-top: 20px">Danh sách các địa điểm gần khách sạn này</h2>
                    <c:forEach items="${requestScope.attList}" var="a">
                        <div class="col-md-4"><img src="${a.attractionImage}" style="width: 100%; margin:10px;"></div>
                        <div class="col-md-8 row">
                            <div class="col-md-12" style="font-weight: bold; font-size: 20px; margin-top: 15px;">${a.attractionName}</div>
                            <div class="col-md-12"><p>${a.attractionDescription}</p></div>
                            <div class="col-md-12"><span style="font-weight: bold">Địa chỉ: ${a.attractionAddress}</span></div>
                            <div class="col-md-12"><span style="font-weight: bold">Hotline: ${a.attractionHotline}</span></div>
                            <c:if test="${sessionScope.account.role == 1}">
                                <span><a href="updateAttraction?attId=${a.attractionId}">Sửa</a></span>
                                <span><a href="deleteAttraction?attId=${a.attractionId}">Xóa</a></span>
                            </c:if> 
                            <br>
                            <br>
                        </div>
                        <br>
                    </c:forEach>
                </div>
            </div>

        </div>
        <br><br><br><br><br><br><br>
    </div>
</body>
</html>
