<%-- 
    Document   : BusinessAnswer
    Created on : Oct 12, 2022, 11:45:43 PM
    Author     : HONG QUAN
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Business Management</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment.min.js"></script>
        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
        <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">

                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="home" class="nav-link">Home</a>
                    </li>

                </ul>

                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">
                    <!-- Navbar Search -->
                    <li class="nav-item">
                        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                            <i class="fas fa-search"></i>
                        </a>
                        <div class="navbar-search-block">
                            <form class="form-inline">
                                <div class="input-group input-group-sm">
                                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                                    <div class="input-group-append">
                                        <button class="btn btn-navbar" type="submit">
                                            <i class="fas fa-search"></i>
                                        </button>
                                        <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>

                    <!-- Messages Dropdown Menu -->
            </nav>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->

                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="info">
                            <span class="brand-link">${sessionScope.business.getUsername()}</span>
                        </div>
                    </div>


                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <!-- Add icons to the links using the .nav-icon class
                                 with font-awesome or any other icon font library -->

                            <li class="nav-item menu-open">
                                <a href="#" class="nav-link active">
                                    <i class="nav-icon fas fa-table"></i>
                                    <p>
                                       Trả lời
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                    
                                </a>
                                <a href="businessReport" class="nav-link active"><p>
                                       Quản lý báo cáo
                                        <i class="fas fa-angle-left right"></i>
                                    </p></a>
                            </li>

                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1>Quản lý hỏi của khách hàng.</h1>
                            </div>
                        </div>
                    </div><!-- /.container-fluid -->
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <!-- /.card-header -->
                                <div class="card">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Tên</th>
                                                <th>Liên hệ</th>
                                                <th>Email</th>
                                                <th>Khách san</th>
                                                <th>Phòng</th>
                                                <th>Kiểu phòng</th>
                                                <th>Giá phòng</th>
                                                <th>Câu hỏi</th>
                                                <th>Ngày đặt câu hỏi</th>
                                                <th>Trả lời câu hỏi</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${requestScope.listQ}" var="question">
                                                <tr>
                                                    <td>${question.getUser().getFirstName()} ${question.getUser().getLastName()}</td>
                                                    <td>${question.getUser().getPhone()}</td>

                                                    <td>${question.getUser().getEmail()}</td>
                                                    <td>${question.getRoom().getHotel().getHotelName()}</td>
                                                    <td>${question.getRoom().getRoomId()}</td>
                                                    <td>${question.getRoom().getRoomType().getRoomTypeName()}</td>
                                                    <td>${question.getRoom().getPrice()}VNĐ</td>
                                                    <td>${question.getQuestionContent()}</td>
                                                    <td>${question.getDateQuestion()}</td>
                                                    <c:if test="${question.getAnswerContent()== null}">
                                                        <td>
                                                            <form action="businessAnswer" method="post"/>
         
                                                              <input type="hidden" name="questionId" value="${question.getQuestionId()}"/>
                                                                <input type="text" name="busiAnswer" required=""/>
                                                                <input onclick="send()" type="submit" value="Trả lời"/>
                                                            </form>
                                                        </td>
                                                    </c:if>
                                                    <c:if test="${question.getAnswerContent()  != null}">
                                                        <td>${question.getAnswerContent()}</td>
                                                    </c:if>

                                                </tr> 
                                            </c:forEach>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <script>
            function send(){
                var arr = document.getElementsByTagName('input');
                var name = arr[4].value;
              if(name == ""){
                alert("Vui lòng dien cau tra loi!!");
                    }
            }
        </script>
    </body>
</html>



