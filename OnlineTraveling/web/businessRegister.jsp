<%-- 
    Document   : businessRegister
    Created on : Sep 18, 2022, 12:30:49 AM
    Author     : ACER
--%>

    <%@page contentType="text/html" pageEncoding="UTF-8"%>
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Register</title>

            <!-- Font Icon -->
            <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">

            <!-- Main css -->
            <link rel="stylesheet" href="CSS/register.css">
        </head>

        <body>
            <%@include  file="View/Header.jsp" %>
                <div class="main">
                    <!-- Sign up form -->
                    <section class="signup">
                        <div class="container">
                            <div class="signup-content">
                                <div class="signup-form">
                                    <h2 class="form-title">Đăng kí cho doanh nghiệp</h2>
                                    <span>${requestScope.error}</span>
                                    <form action="businessRegister" method="post" class="register-form" id="register-form">
                                        <div class="form-group">
                                            <label for="email"><i class="zmdi zmdi-email"></i></label> <input type="text" placeholder="Nhập email của doanh nghiệp bạn" name="email" id="email" required/>
                                        </div>
                                        <div class="form-group">
                                            <label for="busiName"><i class="zmdi zmdi-account material-icons-name"></i></label><input type="text" placeholder="Nhập tên của doanh nghiệp bạn" name="busiName" id="busiName" required/>
                                        </div>
                                        <div class="form-group">
                                            <label for="username"><i class="zmdi zmdi-account material-icons-name"></i></label><input type="text" placeholder="Chúng tôi có thể gọi bạn là gì?" name="username" id="username" required/>
                                        </div>
                                        <div class="form-group">
                                            <label for="phonenumber"><i class="zmdi zmdi-account material-icons-name"></i></label><input type="text" placeholder="Nhập số điện thoại của bạn" name="phonenumber" id="phonenumber" required/>
                                        </div>
                                        <div class="form-group">
                                            <label for="addrees"><i class="zmdi zmdi-account material-icons-name"></i></label><input type="text" placeholder="Nhập địa chỉ của doanh nghiệp bạn" name="address" id="address" required/>
                                        </div>
                                        <div class="form-group">
                                            <label for="firstName"><i class="zmdi zmdi-account material-icons-name"><input type="text" placeholder="Nhập tên của bạn" name="firstName" id="firstName" required/>
                                </div>
                                <div class="form-group">
                                    <label for="lastName"><i class="zmdi zmdi-account material-icons-name"></i></label><input type="text" placeholder="Nhập họ của bạn" name="lastName" id="lastName" required/>
                                        </div>
                                        <div class="form-group">
                                            <label for="newPass"><i class="zmdi zmdi-lock"></i></label><input type="password" placeholder="Nhập mật khẩu" name="password" id="newPass" required/>
                                        </div>
                                        <div class="form-group">
                                            <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
                                            <input type="password" name="re_pass" id="confirm" placeholder="Xác nhận lại mật khẩu" required/>
                                        </div>
                                        <input type="button" onclick="checkFullForBusiness()" id="send" class="form-submit" value="Đăng kí" />
                                        <p id="message"></p>
                                    </form>
                                </div>
                                <div class="signup-image">
                                    <figure>
                                        <img src="images/business-image.jpg" alt="sing up image">
                                    </figure>
                                    <a href="businessLogin" class="signup-image-link">Đã có tài khoản</a>
                                    <span><br><br></span>
                                    <a href="register" class="signup-image-link">
                                        <h3>Đăng kí cho cá nhân</h3>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <%@include  file="View/Footer.jsp" %>
                    <script src="JS/CheckEvents.js"></script>
        </body>

        </html>