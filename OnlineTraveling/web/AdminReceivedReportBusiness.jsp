<%-- 
    Document   : AdminReceivedReportBusiness
    Created on : Oct 16, 2022, 4:59:36 PM
    Author     : HONG QUAN
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
     
    </body>
    <c:set value="${requestScope.question}" var="q"/>
    <table border="1">
        <thead>
            <tr>
                <th>Noi dung bao cao</th>
                <th>Nguoi dung bi bao cao</th>
                <th>email</th>
                <th>Cau hoi bao cao</th>
                <th>Gui mail canh bao nguoi dung</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <c:forEach items="${requestScope.listR}" var="r">
                    
                    <td>${r.getReportContent()}</td>
                <td>${r.getQuestion().getUser().getUserName()}</td> 
                 <td>${r.getQuestion().getUser().getEmail()}</td>
                <td>${r.getQuestion().getQuestionContent()}</td>
                <td><a href="/OnlineTraveling/AdminWaningUser.jsp">Gửi cảnh báo</a></td>
          </c:forEach>
                </tr>
           
        </tbody>
    </table>

    
</html>
