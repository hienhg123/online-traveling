<%-- Document : BusinessLogin Created on : Sep 17, 2022, 10:02:02 PM Author : ACER --%>

    <%@page contentType="text/html" pageEncoding="UTF-8" %>
        <!DOCTYPE html>
        <html>

        <head>
            <title>Business Login Form</title>
            <link rel="stylesheet" href="./CSS/login.css">
            <link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
            <script src="https://kit.fontawesome.com/a81368914c.js"></script>
            <meta name="viewport" content="width=device-width, initial-scale=1">
        </head>

        <body>
            <%@include file="View/Header.jsp" %>
                <img class="wave" src="images/wave-orange.png">
                <div class="container">
                    <div class="img">
                        <img src="images/bg2.png">
                    </div>
                    <div class="login-content">
                        <form action="businessLogin" method="post">
                            <img src="images/avatar2.svg">
                            <h3 class="title">Chào Mừng</h3>
                            <h4 class="mb-0 mr-4 mt-2" style="color:red">${requestScope.error}</h4>
                            <div class="input-div one">
                                <div class="i">
                                    <i class="fas fa-user"></i>
                                </div>
                                <div class="div">
                                    <h5>Email</h5>
                                    <input type="email" class="input" name="email">
                                </div>
                            </div>
                            <div class="input-div pass">
                                <div class="i">
                                    <i class="fas fa-lock"></i>
                                </div>
                                <div class="div">
                                    <h5>Mật khẩu</h5>
                                    <input type="password" class="input" name="password">
                                </div>
                            </div>
                            <br>
                            <a class="login-a" href="businessRegister.jsp">Đăng ký</a>
                            <br>
                            <a class="login-a" href="Login.jsp">Bạn là cá nhân?</a>
                            <br>
                            <a class="login-a" href="forgot">Quên mật khẩu?</a>
                            <input type="submit" class="btn-business-login" value="Login">
                        </form>
                    </div>
                </div>
                <%@include file="View/Footer.jsp" %>
                    <script src="./JS/CheckEvents.js"></script>
        </body>

        </html>

        <!-- <!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
        <link href='https://fonts.googleapis.com/css?family=Inter' rel='stylesheet'>
        <link rel="stylesheet" href="CSS/banner.css">
        <link rel="stylesheet" href="CSS/homepage.css">
        <style>
            body {
                font-family: 'Inter';
                font-size: 22px;
            }
        </style>
        <title>Book'nStay</title>
    </head>

    <body>
        <div class="container-md">
            <div class="header">
                <div class="row">
                    <div class="col-md-2">
                        <a href="home">
                            <img src="images/Logo.png" alt="alt"/>
                        </a>
                    </div>

                    <div class="col-md-1 redir">
                        <a href="home">Trang chủ</a>
                    </div>
                    <div class="col-md-1 redir">
                        <a href="#">Lưu trú</a>
                    </div>
                    <div class="col-md-1 redir">
                        <a href="#">Thuê xe</a>
                    </div>
                    <div class="col-md-2 redir">
                        <a href="#">Địa điểm tham quan</a>
                    </div>
                    <div class="col-md-1">
                        <div></div>
                    </div>

                    <c:if test="${sessionScope.account == null}">
                        <div class="col-md-2 redir">
                            <a href="businesslogin">Đăng nhập doanh nghiệp</a>
                        </div>

                        <div class="col-md-1 redir">
                            <a href="login">Đăng nhập</a>
                        </div>
                        <div class="col-md-1 registerBtn">
                            <a href="register">Đăng ký</a>
                        </div>
                    </c:if>
                    <c:if test="${sessionScope.account != null}">
                      
                        <div class="col-md-1">
                        </div>



                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-2 profilePhoto">
                                    <a href="userprofile?userID=${sessionScope.account.userID}">
                                        <img src="images/user.png" alt="alt"/>
                                    </a> 
                                </div>
                                <div class="col-md-6 username">
                                    <a href="userprofile?userID=${sessionScope.account.userID}">${sessionScope.account.userName}</a>
                                </div>
                          
                                <div class="col-md-3 firstName">
                                    <a href="#">${sessionScope.account.firstName}</a>
                                </div>
                                <div class="col-md-3 logout">
                                    <a href="logout">Logout</a>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>
            <form action="businessLogin" method="post">
                <input type="textbox" class="" name="email">    
                <input type="password" name="password" placeholder="Mật khẩu">
                <input type="submit">
            </form>
            <a href="businessRegister">
                Register your business account.
            </a>


            <br/><br/><br/><br/><br/>
            <br/><br/><br/><br/><br/>
            <br/><br/><br/><br/><br/>

            <div class="footer">
                <div class="row">
                    <div class="col-md-6 contactInfo">
                        <p id="infoTitle">Địa chỉ và thông tin liên hệ</p>
                        <div class="informationCard">
                            <p>Đại Học FPT Hà Nội, Km29 Đường Cao Tốc 08, Thạch Hoà, Thạch Thất, Hà Nội</p>
                            <p>Tel: 0945066089</p>
                            <p>Email: CuongNDHE163098@fpt.edu.vn</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html> -->