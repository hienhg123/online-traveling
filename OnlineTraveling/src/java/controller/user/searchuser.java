/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.user;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import dal.*;
import java.util.ArrayList;
import models.*;

/**
 *
 * @author Dell
 */
public class searchuser extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ArrayList<User> user = new ArrayList<>();
        String name = request.getParameter("searchusername");
        String ade = request.getParameter("adeactive");
        String from = request.getParameter("from");
        String to = request.getParameter("to");
        //ủe

        if (name != null && ade.equalsIgnoreCase("full") && from.isEmpty() && to.isEmpty()) {
            user = new UserDAO().getUserbyName(name);
        } else if (name.isEmpty() && ade != null && from.isEmpty() && to.isEmpty()) {
            if (ade.equalsIgnoreCase("full")) {
                user = new UserDAO().getallUser();
            } else if (ade.equalsIgnoreCase("active")) {
                int sta = 1;
                user = new UserDAO().getUserbyStatus(sta);
            } else if (ade.equalsIgnoreCase("deactive")) {
                int sta = 0;
                user = new UserDAO().getUserbyStatus(sta);
            }
        } else if (name != null && ade != null && from.isEmpty() && to.isEmpty()) {
            if (ade.equalsIgnoreCase("full")) {
                user = new UserDAO().getUserbyName(name);
            } else if (ade.equalsIgnoreCase("active")) {
                int sta = 1;
                user = new UserDAO().getUserbyStaName(sta, name);
            } else if (ade.equalsIgnoreCase("deactive")) {
                int sta = 0;
                user = new UserDAO().getUserbyStaName(sta, name);
            }
        } else if (from != null && to != null && name.isEmpty() && ade.equalsIgnoreCase("full")) {
            Date d1 = Date.valueOf(from);
            Date d2 = Date.valueOf(to);
            user = new UserDAO().getUserbyDate(d1, d2);
        } else if (from != null && to != null && name.isEmpty() && ade.equalsIgnoreCase("full") == false) {
            Date d1 = Date.valueOf(from);
            Date d2 = Date.valueOf(to);
            if (ade.equalsIgnoreCase("full")) {
                user = new UserDAO().getUserbyDate(d1, d2);
            } else if (ade.equalsIgnoreCase("active")) {
                user = new UserDAO().getUserbyDateSta(1, d1, d2);
            } else if (ade.equalsIgnoreCase("deactive")) {
                user = new UserDAO().getUserbyDateSta(0, d1, d2);
            }
        } else if (from != null && to != null && name != null && ade.equalsIgnoreCase("full")) {
            Date d1 = Date.valueOf(from);
            Date d2 = Date.valueOf(to);
            user = new UserDAO().getUserbyDateName(name, d1, d2);
        } else if (from != null && to != null && name!= null && ade != null) {
            Date d1 = Date.valueOf(from);
            Date d2 = Date.valueOf(to);
            if (ade.equalsIgnoreCase("full")) {
                user = new UserDAO().getUserbyDateName(name, d1, d2);
            } else if (ade.equalsIgnoreCase("active")) {
                user = new UserDAO().getUserbyDateStaName(name, 1, d1, d2);
            } else if (ade.equalsIgnoreCase("deactive")) {
                user = new UserDAO().getUserbyDateStaName(name, 0, d1, d2);
            }
        } else {
            user = new UserDAO().getallUser();
        }
        if(from.isEmpty()!= false){
        request.setAttribute("fro", from);
        request.setAttribute("tto", to);
        }
        request.setAttribute("acde", ade);
        request.setAttribute("uname", name);
        request.setAttribute("searchname", user);
        request.getRequestDispatcher("ListUser.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
