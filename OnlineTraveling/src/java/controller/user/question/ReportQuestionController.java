/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.user.question;

import dal.QuestionDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;
import javax.mail.Session;
import models.Question;
import models.Report;
import models.User;

/**
 *
 * @author HONG QUAN
 */
public class ReportQuestionController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.getRequestDispatcher("reportQuestion.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int roomID = Integer.parseInt(request.getParameter("roomId"));
        int questionID = Integer.parseInt(request.getParameter("questionID"));
        QuestionDAO daoQ = new QuestionDAO();
        List<Report> listR = daoQ.listAllReport();
        request.setAttribute("listR", listR);
        request.setAttribute("roomID", roomID);
        request.setAttribute("questionID", questionID);
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");
       user.setUserID(user.getUserID());
        int questionId = Integer.parseInt(request.getParameter("questionId"));
        int roomID = Integer.parseInt(request.getParameter("roomID"));
        String reportContent = request.getParameter("reportContent");
         String reportname = request.getParameter("reportname");
        LocalDateTime now = LocalDateTime.now();
        Report report = new Report();
        report.setUser(user);
        report.setDateReport(now.toString());
        Question question = new Question();
        question.setQuestionId(questionId);
        report.setQuestion(question);
        if (reportContent == null) {
            report.setReportContent(reportname);
             
        } else {
            report.setReportContent(reportContent);
             
        }
         QuestionDAO dao = new QuestionDAO();
        dao.InsertReportQuestion(report);
        response.sendRedirect("/OnlineTraveling/RoomDetail?roomId=" + roomID);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
