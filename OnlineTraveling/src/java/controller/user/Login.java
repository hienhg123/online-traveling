/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.user;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import models.User;

/**
 *
 * @author quandhhe151315
 */
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Login</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Login at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = request.getParameter("url");
       
        if(url != null){
         request.setAttribute("url", url);
        }
        else{
             request.setAttribute("url", "0");
             }
        request.getRequestDispatcher("Login.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        PrintWriter out = response.getWriter();
        UserDAO udao = new UserDAO();
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        User user = udao.getUser(email, password);
        HttpSession session = request.getSession();
        String url = request.getParameter("url");
        request.setAttribute("url", "0");
        session.setAttribute("user", user);
        if (user == null) {
            request.setAttribute("error", "Username or password invalid!!!");
            request.getRequestDispatcher("Login.jsp").forward(request, response);
            
        } else {
            //check if user is blocked or not
            if (user.isStatus() == true) {
                session.setAttribute("account", user);
                //role 1 - admin
                switch (user.getRole()) {
                    case 1:
                        if(!url.equalsIgnoreCase("0")){
                            response.sendRedirect(url);
                        }
                        else{
                            response.sendRedirect("home");
                        }
                        break;
                    case 2:
                        if(!url.equalsIgnoreCase("0")){
                            response.sendRedirect(url);
                        }
                        else{
                            response.sendRedirect("home");
                        }
                        break;
                    case 3:
                       if(!url.equalsIgnoreCase("0")){
                            response.sendRedirect(url);
                        }
                       else{
                            response.sendRedirect("home");
                       }
                        break;
                    default:
                        break;
                }
            } else {
                request.setAttribute("error", "Username or password invalid!!!");
                request.getRequestDispatcher("Login.jsp").forward(request, response);
            }

        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
