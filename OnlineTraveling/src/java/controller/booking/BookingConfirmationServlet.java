/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.booking;

import dal.BookingDAO;
import dal.DestinationDAO;
import dal.DiscountDAO;
import dal.HotelDAO;
import dal.RatePlanDAO;
import dal.RoomDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import models.Hotel;
import models.RatePlan;
import models.Room;
import models.User;

/**
 *
 * @author ACER
 */
public class BookingConfirmationServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BookingConfirmationServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BookingConfirmationServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();
        DiscountDAO discountDAO = new DiscountDAO();
        BookingDAO bookingDAO = new BookingDAO();
        RoomDao roomDAO = new RoomDao();

        User user = (User) session.getAttribute("account");
        String[] RoomId_raw = request.getParameterValues("rid");
        String[] amt_raw = request.getParameterValues("amt");
        String[] rpid_raw = request.getParameterValues("rpid");
//        User user = (User) session.getAttribute("user");
        String FromDate_raw = request.getParameter("fromDate");
        String ToDate_raw = request.getParameter("toDate");
        Date fromDate = Date.valueOf(FromDate_raw);
        Date toDate = Date.valueOf(ToDate_raw);
        int RoomId[] = new int[RoomId_raw.length];
        int amt[] = new int[amt_raw.length];
        int rpid[] = new int[rpid_raw.length];
//        int RoomId = Integer.parseInt(RoomId_raw);
        try {
            for (int i = 0; i < RoomId_raw.length; i++) {
//            out.println("Converting RoomId: i = " + i +" , RoomId_raw = " + RoomId_raw[i]);
                RoomId[i] = Integer.parseInt(RoomId_raw[i]);
            }

            for (int j = 0; j < amt_raw.length; j++) {
                amt[j] = Integer.parseInt(amt_raw[j]);
            }

            for (int k = 0; k < rpid_raw.length; k++) {
                rpid[k] = Integer.parseInt(rpid_raw[k]);
            }
        } catch (NumberFormatException e) {
//            out.println(e.getMessage());
        }

        List<Room> roomList = new ArrayList<>();

        for (int i = 0; i < RoomId.length; i++) {
            Room room = new Room();
            if (amt[i] != 0) {
                room = roomDAO.getRoomForPaymentConfirmation(RoomId[i], amt[i], rpid[i]);
                roomList.add(room);
            }
        }

        Hotel hotel = roomList.get(0).getHotel();

        int checkRoomId = 0;
        int countRoomWithSameId = 0;
        for (Room room : roomList) {
            if (checkRoomId != room.getRoomId()) {
                checkRoomId = room.getRoomId();
                countRoomWithSameId = 0;
            }
            countRoomWithSameId = countRoomWithSameId + room.getAmount();
//            out.println("RoomId: "+room.getRoomId()+"current number of room: "+countRoomWithSameId +"Available Room: "+bookingDAO.getNumberOfAvailableRoom(fromDate, toDate, checkRoomId));
            if (countRoomWithSameId > bookingDAO.getNumberOfAvailableRoom(fromDate, toDate, checkRoomId)) {
                request.setAttribute("error", "Số phòng bạn chọn không hợp lệ. Vui lòng chọn lại");
                request.getRequestDispatcher("hotelDetail?hotelId=" + hotel.getHotelId()).forward(request, response);

            }
        }

        int DiscountPercentage = discountDAO.getPercentageByDate(fromDate, toDate);

        RatePlanDAO rpDAO = new RatePlanDAO();
        /*
        float planDiscount = 0;

        RatePlan firstTimePlan = new RatePlan();
        firstTimePlan.setPercentage(0);
        boolean firstTimeBooking = rpDAO.checkUserBookingFirstTime(user.getUserID(), hotel.getHotelId());
        if (firstTimeBooking == true) {
            firstTimePlan = rpDAO.getFirstBookPlan(hotel.getHotelId());
        }

        RatePlan longStayPlan = new RatePlan();
        longStayPlan.setPercentage(0);
        longStayPlan = rpDAO.getLongTimeStayPlan(fromDate, toDate, hotel.getHotelId());

        RatePlan earlyBookPlan = new RatePlan();
        earlyBookPlan.setPercentage(0);
        earlyBookPlan = rpDAO.getEarlyBookPlan(fromDate, toDate, hotel.getHotelId());

        RatePlan finalPlan = new RatePlan();
        finalPlan.setPercentage(0);

        //Calculate final discount
        if (longStayPlan != null) {
            if (planDiscount < longStayPlan.getPercentage()) {
                planDiscount = longStayPlan.getPercentage();
                finalPlan = longStayPlan;
            }
        }

        if (earlyBookPlan != null) {
            if (planDiscount < earlyBookPlan.getPercentage()) {
                planDiscount = earlyBookPlan.getPercentage();
                finalPlan = earlyBookPlan;
            }
        }

        if (firstTimePlan != null) {
            if (planDiscount < firstTimePlan.getPercentage()) {
                planDiscount = firstTimePlan.getPercentage();
                finalPlan = firstTimePlan;
            }
        }

         */

        //Calculate the day
        //Convert date to localdate
        LocalDate fromLocalDate = fromDate.toLocalDate();
        LocalDate toLocalDate = toDate.toLocalDate();
        //Convert localDate to LocalDateTime
        LocalDateTime fromLocalDateTime = fromLocalDate.atStartOfDay();
        LocalDateTime toLocalDateTime = toLocalDate.atStartOfDay();
        //Calculate the day in between using Duration.between
        long daysBetween = Duration.between(fromLocalDateTime, toLocalDateTime).toDays();

        //Calculate the price
        float price = 0;
        for (Room room : roomList) {
            price = price + (room.getPrice() * room.getAmount() * daysBetween * ((100 - rpDAO.getPlanById(room.getPlanId()).getPercentage()) / 100));
        }

        //Calcullate price for each room and convert price to str
        for (Room room : roomList) {
            float oneNightPrice = room.getPrice() * ((100 - rpDAO.getPlanById(room.getPlanId()).getPercentage()) / 100);
            String oneNightPrice_str = String.format("%.0f", oneNightPrice);
            room.setOneNightPrice_str(oneNightPrice_str);
        }

        for (Room room : roomList) {
            float manyNightPrice = room.getPrice() * daysBetween * ((100 - rpDAO.getPlanById(room.getPlanId()).getPercentage()) / 100);
            room.setManyNightPrice(manyNightPrice);
            String manyNightPrice_str = String.format("%.0f", manyNightPrice);
            room.setManyNightPrice_str(manyNightPrice_str);
        }

        for (Room room : roomList) {
            float oneRoomPrice = room.getPrice() * daysBetween * room.getAmount() * ((100 - rpDAO.getPlanById(room.getPlanId()).getPercentage()) / 100);
            String oneRoomPrice_str = String.format("%.0f", oneRoomPrice);
            room.setOneRoomPrice_str(oneRoomPrice_str);
        }

        //Format Float price number
        String price_str = String.format("%.0f", price);
        //Format Float number

        request.setAttribute("roomList", roomList);
        request.setAttribute("hotel", hotel);
        request.setAttribute("fromDate", fromDate);
        request.setAttribute("toDate", toDate);
//        request.setAttribute("available", available);
        request.setAttribute("priceStr", price_str);
        request.setAttribute("finalPrice", price);
        request.setAttribute("discount", DiscountPercentage);
        request.setAttribute("fromDate", fromDate);
        request.setAttribute("toDate", toDate);

//        request.setAttribute("firstTimePlan", firstTimePlan);
//        request.setAttribute("longStayPlan", longStayPlan);
//        request.setAttribute("earlyBookPlan", earlyBookPlan);
//        request.setAttribute("finalPlan", finalPlan);
//        request.setAttribute("planDiscount", planDiscount);
        request.setAttribute("days", daysBetween);

        request.getRequestDispatcher("BookingConfirmation.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //DAOs
        BookingDAO bookingDAO = new BookingDAO();
        DestinationDAO ddao = new DestinationDAO();
        HotelDAO hdao = new HotelDAO();
        //Definitions
        PrintWriter out = response.getWriter();
//        int RoomId = 0;

        int userID = 0;
        Date fromDate = null;
        Date toDate = null;

        HttpSession mySession = request.getSession();
        //Data processing
        String[] RoomId_raw = request.getParameterValues("rid");
        String[] amt_raw = request.getParameterValues("amt");
        String[] price_raw = request.getParameterValues("price");
        String[] rpid_raw = request.getParameterValues("rpid");
//        out.println(RoomId_raw.length +" " +amt_raw.length +" "+price_raw.length + " " + rpid_raw.length);
        String userID_raw = request.getParameter("userId");
        String fromDate_raw = request.getParameter("fromDate");
        String toDate_raw = request.getParameter("toDate");
//        String amount_raw = request.getParameter("amount");
        String hotelID = request.getParameter("hotelID");
        int[] RoomId = new int[RoomId_raw.length];
        int[] amt = new int[amt_raw.length];
        int[] rpid = new int[rpid_raw.length];
        float[] price = new float[price_raw.length];
        float finalPrice = 0;
        //send email for user
        String hotelName = hdao.getHotelByHotId(Integer.parseInt(hotelID)).getHotelName();
        String hotelAddres = hdao.getHotelByHotId(Integer.parseInt(hotelID)).getHotelAddress();
        String destination = ddao.getDestinationByHotel(Integer.parseInt(hotelID)).getDestinationName();
        User user = (User) mySession.getAttribute("account");
        String userPhone = user.getPhone();
        String fullname = user.getFirstName() + user.getLastName();
        String email = user.getEmail();
        LocalDateTime now = LocalDateTime.now();
        String mess = "";

        try {
            //Convert every raw RoomId value to Integer
            for (int i = 0; i < RoomId_raw.length; i++) {
                RoomId[i] = Integer.parseInt(RoomId_raw[i]);
//                out.println("RoomId: " + RoomId[i]);
            }

            //Convert every raw amount value to Integer
            for (int i = 0; i < amt_raw.length; i++) {
                amt[i] = Integer.parseInt(amt_raw[i]);
//                out.println("amt: " + amt[i]);
            }

            for (int i = 0; i < rpid_raw.length; i++) {
                rpid[i] = Integer.parseInt(rpid_raw[i]);

//                out.println("Price: " + price[i]);
            }
         
            for (int i = 0; i < price_raw.length; i++) {
                price[i] = Float.parseFloat(price_raw[i]);
                finalPrice = finalPrice + price[i]*amt[i];
//                out.println("Price: " + price[i]);
            }
            
            
            
            userID = Integer.parseInt(userID_raw);

            fromDate = Date.valueOf(fromDate_raw);
            toDate = Date.valueOf(toDate_raw);
//            for (int i = 0; i < RoomId.length; i++) {
//                bookingDAO.AddBooking(RoomId[i], userID, toDate, fromDate, price[i], amt[i], rpid[i], userID);
//            }

//            bookingDAO.AddBooking(RoomId, userID, toDate, fromDate, price, amount);
            long daysBetween = Math.abs(fromDate.getTime() - toDate.getTime());
            long day = TimeUnit.DAYS.convert(daysBetween, TimeUnit.MILLISECONDS);
//            out.println(RoomId);
//            out.println(userID);
//            out.println(fromDate);
//            out.println(toDate);
//            out.println(price);
//            out.println(amount);

            //set up message to in email
//            /*


            mess = "<p>Cam on " + fullname + " da dat phong cua ban o" + destination + "</p>"
                    + "<p>" + hotelName + "dang cho ban toi nghi vao: " + fromDate_raw + "</p>"
                    + "<p> thanh toan cua ban se duoc xu li boi " + hotelName + "</p>"
                    + "<p> <strong>Chi tiet dat phong" + "</strong></p>"
                    + "<table style=\"border: 1px solid; margin: auto; border-color: bisque; border-radius: 10px;\">"
                    + "<tr>"
                    + "<td style=\"padding:  20px;\"> Nhan phong: " + fromDate + "</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td style=\"padding:  20px;\"> Tra phong: " + toDate + "</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td style=\"padding:  20px;\"> So ngay o: " + day + "</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td style=\"padding:  20px;\"> So dien thoai: " + userPhone + "</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td style=\"padding:  20px;\"> Dia chi: " + hotelAddres + "</td>"
                    + "</tr>"
                    + "</table>";


//            response.sendRedirect("roomrental?userID=" + userID);
        } catch (NumberFormatException e) {

        }
        //Actions
//        response.sendRedirect("roomrental?userID=" + userID);
        //set up de chuyen sang thanh toan
        Double currencyAmount = new Double(finalPrice);
        Currency usCurrency = Currency.getInstance(Locale.US);
        Currency vnCurrency = Currency.getInstance(new Locale("vi", "VN"));
        NumberFormat currencyUSFormatter = NumberFormat.getCurrencyInstance(Locale.US);
        NumberFormat currencyVNFormatter = NumberFormat.getCurrencyInstance(new Locale("vi", "VN"));
        request.setAttribute("toDate", toDate);
        request.setAttribute("fromDate", fromDate);

        request.setAttribute("numberOfRoom", RoomId);
        request.setAttribute("roomAmt", amt);
        request.setAttribute("priceUS", currencyUSFormatter.format(currencyAmount / 23000));
        request.setAttribute("priceVN", currencyVNFormatter.format(currencyAmount));
        request.setAttribute("hotelID", hotelID);
        request.getRequestDispatcher("Payment.jsp").forward(request, response);
//        sendEmail(email, mess);
//        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
