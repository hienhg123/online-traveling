/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.booking;

import dal.BookingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.Booking;

/**
 *
 * @author ACER
 */
public class SubmitRating extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SubmitRating</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SubmitRating at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String BookingId_raw = request.getParameter("BookingId");
        BookingDAO bookingDAO = new BookingDAO();
        try {
            int BookingId = Integer.parseInt(BookingId_raw);
            Booking booking = bookingDAO.getBookingById(BookingId);
            request.setAttribute("booking", booking);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
        request.getRequestDispatcher("submitRating.jsp").forward(request, response);
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        BookingDAO bookingDAO = new BookingDAO();
        String BookingId_raw = request.getParameter("bookingId");
        String userID_raw = request.getParameter("userID");
        String rating_raw = request.getParameter("rating");
        String Comment = request.getParameter("comment");
        
        int BookingId = 0;
        int userID = 0;
        float Rating = 0;
        try {
            BookingId = Integer.parseInt(BookingId_raw);
            userID = Integer.parseInt(userID_raw);
            Rating = Float.parseFloat(rating_raw);
            bookingDAO.submitRating(BookingId, Rating, Comment);

//            response.sendRedirect("roomrental?userID=" + userID);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
        out.println("Booking_raw: " + BookingId_raw);
        out.println("Booking: " + BookingId);
        out.println("userID_raw: " + userID_raw);
        out.println("userID: " + userID);
        out.println("rating_raw: " + BookingId_raw);
        out.println("rating: " + BookingId);
        out.println("Comment: " + Comment);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
