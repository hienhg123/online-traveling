/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.booking;

import dal.BookingDAO;
import dal.CancellationDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.time.Duration;
import java.time.LocalDate;
import java.util.List;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import models.Booking;
import models.Cancellation;
import models.User;

/**
 *
 * @author Admin
 */
public class CancelBooking extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CancelBooking</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CancelBooking at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //
        PrintWriter out = response.getWriter();
        //lay cac gia tri tu front end ve
        String idBooking = request.getParameter("id");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");
        int userID = user.getUserID();
        BookingDAO bdao = new BookingDAO();
        Booking booked = bdao.getDetailBooking(idBooking, userID);

        //lay ngay ma nguoi dung nhan phong
        Date dateFrom = booked.getFromDate();

        //lay thoi diem ma nguoi dung quyet dinh huy phong
        LocalDate today = LocalDate.now();

        //lay khoang cach giua 2 ngay
        long daysbetween = Duration.between(today.atStartOfDay(), dateFrom.toLocalDate().atStartOfDay()).toDays();

        //lay chinh sach booking ra de check
        String hotelID = booked.getHotelID();
        CancellationDAO cdao = new CancellationDAO();
        List<Cancellation> cancelPolicy = cdao.getAllByHotel(hotelID);

        //lay ra chi phi huy phong
         int penalty = cdao.getPenalty(cancelPolicy, daysbetween);

//         out.println(hotelID);
//         out.println(dateFrom);
        //gui ra front end
        request.setAttribute("days", daysbetween);
        request.setAttribute("penalty", penalty);
        request.setAttribute("booked", booked);
        request.setAttribute("bookingID", idBooking);
        request.getRequestDispatcher("ConfirmCancel.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        //lay cac thong tin can thiet
        String bookingID = request.getParameter("bookingID");
        User user = (User) session.getAttribute("account");
        int userID = user.getUserID();
        
        //cancel booking va lấy ra list
        BookingDAO bdao = new BookingDAO();
        bdao.cancelBooking(bookingID);
        
        //setup de gui mail
        String email = user.getEmail();
        String mess = "Your room has been successfully canceled";
        
        //ve trang chu
        response.sendRedirect("home");
        
        //gui email
        sendEmail(email, mess);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
        private void sendEmail(String email, String mess) {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props, new Authenticator() {
            //overide the getPasswordAuthentication method
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("hientdhe163020@fpt.edu.vn", "xogvlkmpctagjnku");
            }
        });

        //set up message
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email));// change accordingly
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            message.setSubject("Online Traveling");
            message.setText(mess);
            // send message
            Transport.send(message);
        } catch (MessagingException e) {

        }
    }
}
