/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.business;

import dal.QuestionDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.List;
import models.Business;
import models.Question;
import models.Room;
import models.User;

/**
 *
 * @author HONG QUAN
 */
public class BusinessAnswerServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.getRequestDispatcher("BusinessAnswer.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        QuestionDAO dao = new QuestionDAO();
        Business busi = (Business) request.getSession().getAttribute("business");
        if (busi != null) {
            List<Question> questions = dao.getAllListQuestionByBusiness(busi.getBusiID());
            request.setAttribute("listQ", questions);
            processRequest(request, response);
        } else {
            request.getRequestDispatcher("500.jsp").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        try {
            Business busi = (Business) request.getSession().getAttribute("business");
             QuestionDAO dao = new QuestionDAO();
            int questionId = Integer.parseInt(request.getParameter("questionId"));
            String busiAnswer = request.getParameter("busiAnswer");
            Question question = new Question();
            question.setAnswerContent(busiAnswer);
            question.setBusiness(busi);
            question.setQuestionId(questionId);
            LocalDateTime now = LocalDateTime.now();
            question.setDateAnswer(now.toString());
            dao.answerCustomerQuestion(question);
             response.sendRedirect("/OnlineTraveling/businessAnswer");
        } catch (Exception ex) {
            response.sendRedirect("500.jsp");
        }
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
