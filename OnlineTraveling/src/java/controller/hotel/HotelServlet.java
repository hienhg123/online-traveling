/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.hotel;

import dal.AmenitiesDAO;
import dal.DestinationDAO;
import dal.HotelDAO;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import models.Amenities;
import models.Destination;
import models.Hotel;

/**
 *
 * @author HONG QUAN
 */
public class HotelServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int dest = Integer.parseInt(request.getParameter("desId"));
        HotelDAO hoteldao = new HotelDAO();
        HttpSession session = request.getSession();
        AmenitiesDAO amenitiesDAO = new AmenitiesDAO();
        List<Amenities> listAmen = amenitiesDAO.getAll();
        List<Hotel> listHo = hoteldao.getAllHotelsByDesId(dest);

//         try {
//        if (listHo != null) {
//            int page, numberpage = 6;
//            int size = listHo.size();
//            int num = (size % 6 == 0 ? (size / 6) : ((size / 6)) + 1);
//            String xpage = request.getParameter("page");
//            if (xpage == null) {
//                page = 1;
//            } else {
//                page = Integer.parseInt(xpage);
//            }
//            int start, end;
//            start = (page - 1) * numberpage;
//            end = Math.min(page * numberpage, size);
//            List<Hotel> hotel = hoteldao.getListByPage(listHo, start, end);
//     
        session.setAttribute("destinationID", dest);
        request.setAttribute("listHo", listHo);
        //request.setAttribute("hotel", hotel);
        request.getRequestDispatcher("ListHotel.jsp").forward(request, response);
//        }
//        }catch(IOException | ServletException e){
//            System.out.println(e);
//        }
//        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String wifi = request.getParameter("1");
        String dexe = request.getParameter("2");
        String giadinh = request.getParameter("3");
        String letan = request.getParameter("4");
        String nhahang = request.getParameter("5");
        String hoboi = request.getParameter("6");
        String vatnuoi = request.getParameter("7");
        String bontam = request.getParameter("ra1");
        String dieuhoa = request.getParameter("ra2");
        String phongtamrieng = request.getParameter("ra3");
        String bancong = request.getParameter("ra4");
        String maygiat = request.getParameter("ra5");
        String cacham = request.getParameter("ra6");
        String tv = request.getParameter("ra7");
        String price1 = request.getParameter("price1");
        String price2 = request.getParameter("price2");
        String price3 = request.getParameter("price3");
        String price4 = request.getParameter("price4");
        String price5 = request.getParameter("price5");
//        String destination = request.getParameter("destination");
        int desID = Integer.parseInt(request.getParameter("destination"));
        AmenitiesDAO amenitiesDAO = new AmenitiesDAO();
        List<Hotel> listHo = amenitiesDAO.searchHotelByOptions(wifi, dexe, giadinh, letan, nhahang, hoboi, vatnuoi,
         desID, price1, price2, price3, price4, price5,bontam,dieuhoa,phongtamrieng,bancong,maygiat,cacham,tv);
        request.setAttribute("wifi", wifi);
        request.setAttribute("listHo", listHo);
//        out.print(listHo.size());
        request.getRequestDispatcher("ListHotel.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
