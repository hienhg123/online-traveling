/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.hotel;

import dal.AttractionDAO;
import dal.BookingDAO;
import dal.DestinationDAO;
import dal.DiscountDAO;
import dal.HotelDAO;
import dal.RatePlanDAO;
import dal.RoomDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import models.Attraction;
import models.Destination;
import models.DiscountByDate;
import models.Hotel;
import models.RatePlan;
import models.Room;
import models.User;

/**
 *
 * @author HONG QUAN
 */
public class HotelDetailServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            request.getRequestDispatcher("HotelDetail.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int hotId = Integer.parseInt(request.getParameter("hotelId"));
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        RatePlanDAO rpDAO = new RatePlanDAO();
        String fromDate_raw = request.getParameter("FromDate");
        String toDate_raw = request.getParameter("ToDate");
        Date fromDate_session = (Date) session.getAttribute("fromDate");
        Date toDate_session = (Date) session.getAttribute("toDate");
        User user = (User) session.getAttribute("account");

        Date fromDate;
        Date toDate;
        LocalDate checkDiscountDate = null;

//        String Cases;
        //Cai nay tam thoi de day thoi, sau co chuc nang cua Hien thi` se xoa
        //Neu 2 input chua duoc dien thi lay thoi gian tu session thay the vao
        if (fromDate_raw == null && toDate_raw == null) {
//            //Get util date object contains current time
//            java.util.Date currentUtilDate = new java.util.Date();
//            java.util.Date nextUtilDate = new java.util.Date();
//            //User calendar to 
//            Calendar c = Calendar.getInstance(); //get current time
//            c.setTime(nextUtilDate);
//            c.add(Calendar.DATE, 1);
//            Date currentDate = new Date(currentUtilDate.getTime());
//            Date nextDate = new Date(nextUtilDate.getTime());
//            fromDate = currentDate;
//            toDate = nextDate;
//            Cases = "All null";
            listAllRoom(request, response);

            fromDate = fromDate_session;
            toDate = toDate_session;
//            checkDiscountDate = fromDate.toLocalDate();
//            Cases = "getting from session. if null = session empty.";

        } //Khong thi lay gia tri da dien vao roi.
        else {
            fromDate = Date.valueOf(fromDate_raw);
//            checkDiscountDate = fromDate.toLocalDate();
            toDate = Date.valueOf(toDate_raw);
//            Cases = "Got new, not null";
        }

//        request.setAttribute("cases", Cases);
        HotelDAO hotDAO = new HotelDAO();

        request.setAttribute("fromDate", fromDate);
        request.setAttribute("toDate", toDate);

        BookingDAO bookingDAO = new BookingDAO();
        if (fromDate != null || toDate != null) {
            checkDiscountDate = fromDate.toLocalDate();
            ArrayList<Room> roomList = bookingDAO.getAvailableRooms(fromDate, toDate, hotId);

            ArrayList<RatePlan> roomPlanList;
            if (user == null) {
                roomPlanList = rpDAO.getAvailableRatePlansNoLogin(hotId, fromDate, toDate);
                for (Room r : roomList) {
                    r.setAvailablePlan(rpDAO.countAvailablePlan(roomPlanList));
                }
            } else {
                roomPlanList = rpDAO.getAvailableRatePlans(user.getUserID(), hotId, fromDate, toDate);
                for (Room r : roomList) {
                    r.setAvailablePlan(rpDAO.countAvailablePlan(roomPlanList));
                }
            }

            request.setAttribute("listRoom", roomList);
            request.setAttribute("roomPlanList", roomPlanList);
            session.setAttribute("fromDate", fromDate);
            session.setAttribute("toDate", toDate);
        }
        Hotel hot = hotDAO.getHotelByHotId(hotId);

        request.setAttribute("aHotel", hot);

        //Discount by Hien
        DiscountDAO discountDAO = new DiscountDAO();
        List<DiscountByDate> listDiscount = discountDAO.getAllDiscount();
        int discount = 0;
        //check discount
        if (checkDiscountDate != null) {
            //loop to check 
            for (DiscountByDate d : listDiscount) {
                //check the discount date with the date user enter
                if (checkDiscountDate.isEqual(d.getDiscountFrom().toLocalDate()) == true) {
                    discount = d.getPercentage();
                }
            }
        }
        request.setAttribute("discount", discount);

        //Check if user has ever booked that hotel before
//        boolean HotelApplyFirstBook = rpDAO.checkIfHotelApplyFirstTimePlan(hotId);
//        request.setAttribute("firstBookAvailable", HotelApplyFirstBook);
        boolean firstBook = false;
        if (user != null) {
            firstBook = rpDAO.checkUserBookingFirstTime(user.getUserID(), hotId);
        }
        request.setAttribute("firstBook", firstBook);
        RatePlan firstBookPlan = rpDAO.getFirstBookPlan(hotId);
        if (firstBookPlan != null) {
            request.setAttribute("firstBookPlan", firstBookPlan);
        }
//        out.println("FromDate = " + fromDate);
//        out.println("ToDate = " + toDate);
//        out.println("HotelId= " + hotId);
        RatePlan longStayPlan = null;
        if (fromDate != null || toDate != null) {
            longStayPlan = rpDAO.getLongTimeStayPlan(fromDate, toDate, hotId);
        }
//        out.println((longStayPlan == null));
        if (longStayPlan != null) {
//            out.println("Khac null");
            request.setAttribute("longStayPlan", longStayPlan);
        } else {
//            out.println("Null");
            request.setAttribute("longStayPlan", null);

        }

        ArrayList<RatePlan> planList = rpDAO.getAllRatePlansByHotelId(hotId);
        request.setAttribute("planList", planList);
//        out.println("OK");
//        request.getRequestDispatcher("hotelDetail?hotelId=" + hotId);

        //Attraction printing
        AttractionDAO attDAO = new AttractionDAO();
        DestinationDAO dDAO = new DestinationDAO();
        Destination dest = dDAO.getDestinationByHotel(hotId);
        ArrayList<Attraction> attList = attDAO.getAllAttractionByDestinationId(dest.getDestinationID());
        request.setAttribute("attList", attList);

        processRequest(request, response);
    }

    protected void listAllRoom(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int hotId = Integer.parseInt(request.getParameter("hotelId"));
        HotelDAO hotDAO = new HotelDAO();
        RoomDao roomDAO = new RoomDao();
        Hotel hot = hotDAO.getHotelByHotId(hotId);
//        int star;
//         star =hotDAO.getRatingStar(hotId);
        List<Room> roomList = roomDAO.getAllRoomByHotelId(hotId);
        List<Hotel> hotL = hotDAO.getAllHotelsByDesId(hotId);
//        request.setAttribute("star", star);
        request.setAttribute("listRoom", roomList);
        request.setAttribute("aHotel", hot);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int hotId = Integer.parseInt(request.getParameter("hotelId"));
        request.setAttribute("test", "");
        response.sendRedirect("hotelDetail?hotelId=" + hotId);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
