/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.hotel;

import dal.CancellationDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.List;
import models.Cancellation;

/**
 *
 * @author Admin
 */
public class AddCancelPolicy extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddCancelPolicy</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddCancelPolicy at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String hotelID = request.getParameter("hotelID");
        request.setAttribute("hotelID", hotelID);
        request.getRequestDispatcher("AddNewPolicy.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        //khoi tai dao
        CancellationDAO cdao = new CancellationDAO();
        
        //lay gia tri tu front end
        String dayType = request.getParameter("dayType");
        String dateFrom = request.getParameter("dateFrom");
        String dateTo = request.getParameter("dateTo");
        int hotelID = Integer.parseInt(request.getParameter("hotelID"));
        String[] after = request.getParameterValues("after[]");
        String[] before = request.getParameterValues("before[]");
        String[] penalty = request.getParameterValues("penalty[]");
        String[] detail = request.getParameterValues("detail[]");
        //lap de kiem tra xem co ngay thu cho nao ko co ngay thu 2 khong
        for (int i = 0; i < before.length; i++) {
            //kiem tra xem nguoi dung co nhap vao gia tri thu 2 ko
            if (before[i].isEmpty()) {
                before[i] = null;
            }
        }

        // check xem nguoi dung co su dung den 2 truong thong tin nay hay ko
        if (dateFrom.isEmpty()) {
            dateFrom = null;
            dateTo = null;
        }
       
        boolean checkDup = false;
        int count = 0;
//        kiem tra xem la loai chinh sach vao ngay nao
        if (dayType.equalsIgnoreCase("Holiday")) {
            //duyet qua tung phan tu ma nguoi dung nhap
            for (int i = 0; i < after.length; i++) {
                checkDup = cdao.checkDupHoliday(after[i], before[i], penalty[i], hotelID, dateFrom, dateTo);
                //kiem tra xem co bi duplicate chinh sach khong
                if (checkDup == true) {
                    count++;
                }
            }
        } else {
            //duyet qua tung phan tu ma nguoi dung nhap
            for (int i = 0; i < after.length; i++) {
                checkDup = cdao.checkDupRegular(after[i], before[i], penalty[i], hotelID);
//                out.println(after[i]);
//                out.println(before[i]);
//                out.println(penalty[i]);
                //kiem tra xem co bi duplicate chinh sach khong
                if (checkDup == true) {
                    count++;
                }
            }
        }
//        out.println(count);
        //sau khi duyet xong neu tat co 1 cai bi trung lap
        if (count != 0) {
            request.setAttribute("error", "Chính sách đã tồn tại");
            request.setAttribute("hotelId", hotelID);
            request.getRequestDispatcher("AddNewPolicy.jsp").forward(request, response);
        } else {
            //lap de add tung phan tu vao trong db
            for(int i=0;i<after.length;i++){
                cdao.AddPolicy(dayType, dateFrom, dateTo, detail[i], after[i], before[i], penalty[i], hotelID);
            }
            response.sendRedirect("cancelPolicy?hotelID=" + hotelID);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
