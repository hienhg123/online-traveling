/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.hotel;

import dal.BusinessDAO;
import dal.HotelDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import models.Business;
import models.Hotel;

/**
 *
 * @author huyhi
 */
public class HotelBusinessServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HotelList</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet HotelList at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        BusinessDAO businessDao = new BusinessDAO();
        int busiID = Integer.parseInt(request.getParameter("busiID"));
        HotelDAO hotelDao = new HotelDAO();
        List<Hotel> listHotel = hotelDao.getAllHotelByBusinessId(busiID);
        request.setAttribute("listHotel", listHotel);
        Business b = null;
        int currentBusinessID = Integer.parseInt(request.getParameter("busiID"));
        try {
            b = (Business) session.getAttribute("business");
        } catch (Exception e) {
        }
        Business b1 = businessDao.getBusinessById(busiID);
        if (b != null) {
            session.removeAttribute("business");
            session.setAttribute("business", b1);
        }
        try {
            request.setAttribute("currentBusinessID", currentBusinessID);
            Business currentBusiness = businessDao.getBusinessById(currentBusinessID);
            request.setAttribute("currentBusiness", currentBusiness);
            request.getRequestDispatcher("HotelBusiness.jsp").forward(request, response);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }

        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
