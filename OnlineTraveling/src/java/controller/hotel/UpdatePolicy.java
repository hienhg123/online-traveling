/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.hotel;

import dal.CancellationDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import models.Cancellation;

/**
 *
 * @author Admin
 */
public class UpdatePolicy extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdatePolicy</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdatePolicy at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CancellationDAO cdao = new CancellationDAO();
        String cancelID = request.getParameter("cancelID");
        String hotelId = request.getParameter("hotelID");
        Cancellation c = cdao.getCancelPolicyByID(cancelID);
        request.setAttribute("hotelID", hotelId);
        request.setAttribute("cancel", c);
        request.getRequestDispatcher("UpdatePolicy.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CancellationDAO cdao = new CancellationDAO();

        
        //lay gia tri tu front end

        String cancelID = request.getParameter("cancelID");
        String dayType = request.getParameter("dayType");
        String dateFrom = null;
        String dateTo = null;
        int hotelID = Integer.parseInt(request.getParameter("hotelID"));
        String before = request.getParameter("before");
      
        
        //lay list tat ca cac chinh sach ra de kiem tra
        List<Cancellation> list = cdao.getAllByHotel(request.getParameter("hotelID"));
        
        
        //lay phan tu dang cap nhat
        Cancellation c = cdao.getCancelPolicyByID(cancelID);
        
        
        //kiem tra xem nguoi dung co nhap vao gia tri thu 2 ko
        if (before.isEmpty()) {
            before = null;
        }
        // check tuong tu voi 2 ngay

        String after = request.getParameter("after");
        String detail = request.getParameter("detail");
        String penalty = request.getParameter("penalty");
        boolean checkDup = false;

        //kiem tra xem la loai chinh sach vao ngay nao
        if (dayType.equalsIgnoreCase("Holiday")) {
            dateFrom = request.getParameter("dateFrom");
            dateTo = request.getParameter("dateTo");

            //kiem tra xem da ton tai chinh sach nay chua
            checkDup = cdao.checkDupHoliday(after, before, penalty, hotelID, dateFrom, dateTo);
        } else {
            checkDup = cdao.checkDupRegular(after, before, penalty, hotelID);
        }

        //check xem co bi lap hay khong
        if (checkDup == true) {
            request.setAttribute("error", "Chính sách đã tồn tại");
            request.setAttribute("hotelID", hotelID);
            request.setAttribute("cancel", c);
            request.getRequestDispatcher("UpdatePolicy.jsp").forward(request, response);
            //check xem co bi va cham hay khong
        }  else {
            cdao.updateCancelPolicy(dayType, dateFrom, dateTo, detail, after, before, penalty, hotelID, cancelID);
            request.setAttribute("hotelID", hotelID);
            request.setAttribute("policy", list);
            request.setAttribute("mess", "Cập nhật thành công");
            request.getRequestDispatcher("CancelPolicy.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
