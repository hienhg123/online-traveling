/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.rateplan;

import dal.HotelDAO;
import dal.RatePlanDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.Hotel;
import models.RatePlan;

/**
 *
 * @author ACER
 */
public class UpdateCustomPlanServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateCustomPlanServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateCustomPlanServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String PlanId_raw = request.getParameter("PlanId");
        PrintWriter out = response.getWriter();
        HotelDAO hotelDAO = new HotelDAO();
        try {
            RatePlanDAO planDAO = new RatePlanDAO();
            int PlanId = Integer.parseInt(PlanId_raw);
            
            RatePlan Plan = planDAO.getPlanById(PlanId);
            int HotelId = Plan.getHotelId().getHotelId();
            
            request.setAttribute("plan", Plan);
            request.setAttribute("hotelId", HotelId);
            
            Hotel hotel = hotelDAO.getHotelByHotId(HotelId);
            
            request.setAttribute("hotel", hotel);
            
            request.getRequestDispatcher("UpdateFirstBookPlan.jsp").forward(request, response);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String PlanId_raw = request.getParameter("planId");
        String PlanName = request.getParameter("planName");
        String HotelId_raw = request.getParameter("hotelId");
        String percentage_raw = request.getParameter("percentage");
        String availablePayment_raw = request.getParameter("availablePayment");
        String CancellationPolicy = request.getParameter("cancellationPolicy");
        String meal_raw = request.getParameter("meal");

        RatePlanDAO rpDAO = new RatePlanDAO();
        try {
            int PlanId = Integer.parseInt(PlanId_raw);

            int HotelId = Integer.parseInt(HotelId_raw);
            float percentage = Float.parseFloat(percentage_raw);
            int availablePayment = Integer.parseInt(availablePayment_raw);
            boolean meal = Boolean.parseBoolean(meal_raw);
            rpDAO.updateFirstTimeBookingPlan(PlanId, PlanName, percentage, availablePayment, CancellationPolicy, meal);
            response.sendRedirect("ratePlanList?hotelId=" + HotelId);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
