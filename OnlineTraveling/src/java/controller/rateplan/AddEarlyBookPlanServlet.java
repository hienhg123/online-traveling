/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.rateplan;

import dal.HotelDAO;
import dal.RatePlanDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.Hotel;
import models.RatePlan;

/**
 *
 * @author ACER
 */
public class AddEarlyBookPlanServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddEarlyBookPlanServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddEarlyBookPlanServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String HotelId_raw = request.getParameter("hotelId");
        PrintWriter out = response.getWriter();
        HotelDAO hotelDAO = new HotelDAO();

        try {
            RatePlanDAO planDAO = new RatePlanDAO();
            int HotelId = Integer.parseInt(HotelId_raw);
            request.setAttribute("hotelId", HotelId);
            Hotel hotel = hotelDAO.getHotelByHotId(HotelId);
            request.setAttribute("hotel", hotel);
            request.getRequestDispatcher("AddEarlyBookPlan.jsp").forward(request, response);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String planName = request.getParameter("planName");
        String day_raw = request.getParameter("day");
        String percentage_raw = request.getParameter("percentage");
        String HotelId_raw = request.getParameter("hotelId");
        int HotelId;
        RatePlanDAO rpDAO = new RatePlanDAO();
        String error = "";

        try {

            HotelId = Integer.parseInt(HotelId_raw);
            if (planName.equals("")) {

                error = "Bạn chưa nhập tên gói giá trị";
                request.setAttribute("error", error);
                request.setAttribute("hotelId", HotelId);
                request.getRequestDispatcher("AddEarlyBookPlan.jsp").forward(request, response);
            }
            if (day_raw.equals("")) {
                error = "Bạn chưa nhập số ngày cần thiết của gói giá trị";
                request.setAttribute("error", error);
                request.setAttribute("hotelId", HotelId);
                request.getRequestDispatcher("AddEarlyBookPlan.jsp").forward(request, response);
            }
            if (percentage_raw.equals("")) {
                error = "Bạn chưa nhập tỉ lệ khuyến mãi của gói giá trị";
                request.setAttribute("error", error);
                request.setAttribute("hotelId", HotelId);
                request.getRequestDispatcher("AddEarlyBookPlan.jsp").forward(request, response);
            }

            float percentage = Float.parseFloat(percentage_raw);
            int day = Integer.parseInt(day_raw);
            rpDAO.addNewEarlyBookPlan(planName, percentage, day, HotelId);
            response.sendRedirect("ratePlanList?hotelId=" + HotelId);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
