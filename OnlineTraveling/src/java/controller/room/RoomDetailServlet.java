/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.room;

import dal.QuestionDAO;
import dal.RatePlanDAO;
import dal.RoomDao;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Question;
import models.RatePlan;
import models.Room;
import models.User;

/**
 *
 * @author HONG QUAN
 */
public class RoomDetailServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int roomID = Integer.parseInt(request.getParameter("roomId"));
        RoomDao roomdao = new RoomDao();
        Room room = roomdao.GetRoomByRoomId(roomID);
        request.setAttribute("roomR", room);

        request.getRequestDispatcher("RoomDetail.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        QuestionDAO dao = new QuestionDAO();
        int roomID = Integer.parseInt(request.getParameter("roomId"));
        List<Question> questions = dao.getALlListQuestionByRoomId(roomID);
        request.setAttribute("listQ", questions);
        
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String content = request.getParameter("content");
        int roomID = Integer.parseInt(request.getParameter("roomId"));
        Question question = new Question();
        User user = (User) request.getSession().getAttribute("account");
        Room room = new Room();
        room.setRoomId(roomID);
        question.setQuestionContent(content);
        question.setRoom(room);
        question.setUser(user);
        LocalDateTime now = LocalDateTime.now();
        question.setDateQuestion(now.toString());
        QuestionDAO dao = new QuestionDAO();
        dao.insert(question);
        
        response.sendRedirect("/OnlineTraveling/RoomDetail?roomId=" + roomID);
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
