/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import models.Cancellation;

/**
 *
 * @author Admin
 */
public class CancellationDAO extends DBContext {

    public List<Cancellation> getAllByHotel(String hotelID) {
        List<Cancellation> list = new ArrayList<>();
        String sql = "select * from Cancellation c "
                + " where hotelID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, hotelID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Cancellation c = new Cancellation(rs.getInt(1), rs.getString(2), rs.getDate(3),
                        rs.getDate(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getBoolean(9),
                        rs.getInt(10));
                list.add(c);
            }
        } catch (SQLException e) {
        }

        return list;

    }

    public void AddPolicy(String cancelDayType, String cancelDateFrom, String cancelDateTo, String policyDetail,
            String after, String before, String penalty, int hotelID) {
        String sql = "INSERT INTO [dbo].[Cancellation]\n"
                + "           ([CancelDayType]\n"
                + "           ,[CancelDateFrom]\n"
                + "           ,[CancelDateTo]\n"
                + "           ,[CancelPolicyDetail]\n"
                + "           ,[CancelAfter]\n"
                + "           ,[CancelBefore]\n"
                + "           ,[CancelPenalty]\n"
                + "           ,[CancelStatus]\n"
                + "           ,[HotelId])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, cancelDayType);
            st.setString(2, cancelDateFrom);
            st.setString(3, cancelDateTo);
            st.setString(4, policyDetail);
            st.setString(5, after);
            st.setString(6, before);
            st.setString(7, penalty);
            st.setBoolean(8, true);
            st.setInt(9, hotelID);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public Cancellation getCancelPolicyByID(String id) {
        String sql = "select * from Cancellation c "
                + " where cancelID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Cancellation c = new Cancellation(rs.getInt(1), rs.getString(2), rs.getDate(3),
                        rs.getDate(4), rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getBoolean(9),
                        rs.getInt(10));
                return c;
            }
        } catch (SQLException e) {
        }

        return null;
    }

    public boolean checkDupRegular(String after, String before, String penalty, int hotelID) {
        String sql = "select * from Cancellation c\n"
                + "where c.CancelAfter = ? and c.CancelBefore = ?\n"
                + " and c.CancelPenalty = ? and c.HotelId = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, after);
            st.setString(2, before);
            st.setString(3, penalty);
            st.setInt(4, hotelID);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
        }
        return false;
    }

    public boolean checkDupHoliday(String after, String before, String penalty, int hotelID, String fromDate, String toDate) {
        String sql = "select * from Cancellation c\n"
                + "where c.CancelAfter = ? and c.CancelBefore = ?\n"
                + " and c.CancelPenalty = ? and c.HotelId = ? "
                + "and c.CancelDateFrom = ? and c.CancelDateTo = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, after);
            st.setString(2, before);
            st.setString(3, penalty);
            st.setInt(4, hotelID);
            st.setString(5, fromDate);
            st.setString(6, toDate);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
        }
        return false;
    }

    public void updateCancelPolicy(String cancelDayType, String cancelDateFrom, String cancelDateTo, String policyDetail,
            String after, String before, String penalty, int hotelID, String cancelID) {
        String sql = "UPDATE [dbo].[Cancellation]\n"
                + "   SET [CancelDayType] = ?\n"
                + "      ,[CancelDateFrom] = ?\n"
                + "      ,[CancelDateTo] = ?\n"
                + "      ,[CancelPolicyDetail] = ?\n"
                + "      ,[CancelAfter] = ?\n"
                + "      ,[CancelBefore] = ?\n"
                + "      ,[CancelPenalty] = ?\n"
                + "      ,[CancelStatus] = ?\n"
                + "      ,[HotelId] = ?\n"
                + " WHERE CancelID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, cancelDayType);
            st.setString(2, cancelDateFrom);
            st.setString(3, cancelDateTo);
            st.setString(4, policyDetail);
            st.setString(5, after);
            st.setString(6, before);
            st.setString(7, penalty);
            st.setBoolean(8, true);
            st.setInt(9, hotelID);
            st.setString(10, cancelID);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    public void ActiveStatus(String cancelID) {
        String sql = "UPDATE [dbo].[Cancellation]\n"
                + "   SET [CancelStatus] = ? \n"
                + " WHERE CancelID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setBoolean(1, true);
            st.setString(2, cancelID);
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    public void DeactiveStatus(String cancelID) {
        String sql = "UPDATE [dbo].[Cancellation]\n"
                + "   SET [CancelStatus] = ? \n"
                + " WHERE CancelID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setBoolean(1, false);
            st.setString(2, cancelID);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }


    public int getPenalty(List<Cancellation> cancelPolicy, long daysbetween) {
        int penalty = 0;
        for (int i = 0; i < cancelPolicy.size(); i++) {
            //kiem tra xem chinh sach co hoat dong hay khong
            if (cancelPolicy.get(i).isCancelStatus() == true) {                
                //kiem tra xem co phai chinh sach cho ngay thuong hay khong
                if (cancelPolicy.get(i).getCancelDayType().equalsIgnoreCase("Regular")) {
                    //kiem tra xem co gia tri dang truoc khong
                    //neu khong co
                    if (cancelPolicy.listIterator(i).hasPrevious() != true) {
                        //kiem tra xem gia tri hien tai co may gia tri ngay
                        //neu co 1
                        if (cancelPolicy.get(i).getCancelBefore() == 0) {
                            //kiem tra xem ngay huy
                            if (cancelPolicy.get(i).getCancelAfter() < daysbetween) {
                                penalty = cancelPolicy.get(i).getCancelPenalty();
                                break;
                            }
                        }
                        //neu dang truoc ton tai gia tri
                    } else {
                        //kiem tra xem gia tri hien tai co may gia tri ngay
                        //neu co 1
                        if (cancelPolicy.get(i).getCancelBefore() == 0) {
                            //kiem tra xem ngay huy xem co lon hon hay ko
                            if (cancelPolicy.get(i).getCancelAfter() < daysbetween) {
                                penalty = cancelPolicy.get(i - 1).getCancelPenalty();
                                //neu ko lon hon    
                            } else {
                                penalty = cancelPolicy.get(i).getCancelPenalty();
                            }

                            //neu co 2 gia tri ngay
                        } else {
                            //kiem tra xem co lon hon hay khong
                            if (cancelPolicy.get(i).getCancelAfter() < daysbetween) {
                                //neu lon hon
                                if (cancelPolicy.get(i - 1).getCancelAfter() >= daysbetween
                                        && cancelPolicy.get(i - 1).getCancelBefore() <= daysbetween) {
                                    penalty = cancelPolicy.get(i - 1).getCancelPenalty();
                                }
                                //neu khong lon hon thi lay gia tri
                            } else {
                                penalty = cancelPolicy.get(i).getCancelPenalty();
                            }
                        }
                    }
                } else {
                    //kiem tra xem co gia tri dang truoc khong
                    //neu khong co
                    if (cancelPolicy.listIterator(i).hasPrevious() != true) {
                        //kiem tra xem gia tri hien tai co may gia tri ngay
                        //neu co 1
                        if (cancelPolicy.get(i).getCancelBefore() == 0) {
                            //kiem tra xem ngay huy
                            if (cancelPolicy.get(i).getCancelAfter() < daysbetween) {
                                penalty = cancelPolicy.get(i).getCancelPenalty();
                                break;
                            }
                        }
                        //neu dang truoc ton tai gia tri
                    } else {
                        //kiem tra xem gia tri hien tai co may gia tri ngay
                        //neu co 1
                        if (cancelPolicy.get(i).getCancelBefore() == 0) {
                            //kiem tra xem ngay huy xem co lon hon hay ko
                            if (cancelPolicy.get(i).getCancelAfter() < daysbetween) {
                                penalty = cancelPolicy.get(i - 1).getCancelPenalty();
                                //neu ko lon hon    
                            } else {
                                penalty = cancelPolicy.get(i).getCancelPenalty();
                            }

                            //neu co 2 gia tri ngay
                        } else {
                            //kiem tra xem co lon hon hay khong
                            if (cancelPolicy.get(i).getCancelAfter() < daysbetween) {
                                //neu lon hon
                                if (cancelPolicy.get(i - 1).getCancelAfter() >= daysbetween
                                        && cancelPolicy.get(i - 1).getCancelBefore() <= daysbetween) {
                                    penalty = cancelPolicy.get(i - 1).getCancelPenalty();
                                }
                                //neu khong lon hon thi lay gia tri
                            } else {
                                penalty = cancelPolicy.get(i).getCancelPenalty();
                            }
                        }
                    }
                }

            }
        }
        return penalty;
    }
    public static void main(String[] args) {
        CancellationDAO cdao = new CancellationDAO();
        Date from = Date.valueOf("2022-10-25");
        Date to = Date.valueOf("2022-11-5");
        List<Cancellation> cancelPolicy = cdao.getAllByHotel("1");

        int a = 17;

        LocalDate today = LocalDate.now();
        LocalDate toLocal = to.toLocalDate();
        long daysbetween = Duration.between(today.atStartOfDay(), toLocal.atStartOfDay()).toDays();
        int penalty = cdao.getPenalty(cancelPolicy, daysbetween);

        cdao.AddPolicy("Regular", null, null, "Huy Truoc 15 Ngay, Mat 20% Phi", "15", null, "20", 1);
        
        cdao.AddPolicy("Regular", null, "", "hehe", "15", "", "20", 2);

//        LocalDate today = LocalDate.now();
//        LocalDate toLocal = to.toLocalDate();
//        long daysbetween = Duration.between(today.atStartOfDay(), toLocal.atStartOfDay()).toDays();
//        int penalty = cdao.getPenalty(cancelPolicy, daysbetween);
//       
//        System.out.println(daysbetween);
//        System.out.println(penalty);
//      for (Cancellation c : list) {
//           //kiem tra xem c co nam trong khoang huy phong ko
//            System.out.println(c.getCancelAfter());
//            System.out.println(c.getCancelBefore());
//       }
//                if(cancelPolicy.get(i).getCancelBefore() == 0){
//                    //kiem tra xem dang truoc co gia gia tri hay khong
//                    if(cancelPolicy.listIterator(i).hasPrevious() != true){
//                        //kiem tra xem co lon hon gia tri duy nhat khong
//                        if(cancelPolicy.get(i).getCancelAfter() < daysbetween){
//                            penalty = cancelPolicy.get(i).getCancelPenalty();
//                        } 
//                    //neu co ton tai gia tri phia truoc
//                    } else {
//                        if(cancelPolicy.get(i).getCancelBefore() < daysbetween){
//                            penalty = cancelPolicy.get(i-1).getCancelPenalty();
//                        }
//                    }
//                }
    }
}
