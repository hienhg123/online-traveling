/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.User;

/**
 *
 * @author Admin
 */
public class UserDAO extends DBContext {

    //insert user 
    public void insertUser(String email, String username, String password, String lastName, String firstName, Date dob) {
        try {
            String sql = "INSERT INTO [dbo].[User]\n"
                    + "           ([username]\n"
                    + "           ,[password]\n"
                    + "           ,[lastName]\n"
                    + "           ,[firstName]\n"
                    + "           ,[dob]\n"
                    + "           ,[role]\n"
                    + "           ,[status]\n"
                    + "           ,[email])\n"
                    + "     VALUES\n"
                    + "           (?,?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setString(3, lastName);
            ps.setString(4, firstName);
            ps.setDate(5, dob);
            ps.setInt(6, 2);
            ps.setBoolean(7, true);
            ps.setString(8, email);
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    // get User by username
    public User getUserByUserName(User u) {
        try {
            String sql = "select * from [User] where username = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, u.getUserName());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new User(rs.getInt("userID"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("lastName"),
                        rs.getString("firstName"),
                        rs.getBoolean("gender"),
                        rs.getDate("dob"),
                        rs.getString("address"),
                        rs.getInt("role"),
                        rs.getBoolean("status"),
                        rs.getString("phone"),
                        rs.getString("email"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    //check username
    public User checkValidUserName(String username) {
        try {
            String sql = "select * from User where username=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new User(rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("lastName"),
                        rs.getString("firstName"),
                        rs.getBoolean("gender"),
                        rs.getDate("dob"),
                        rs.getString("address"),
                        rs.getInt("role"),
                        rs.getBoolean("status"),
                        rs.getString("phone"),
                        rs.getString("email")
                );
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
    //check user account

    public User getUser(String email, String password) {

        try {
            String sql = "select * from [User] where email = ? and password = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new User(rs.getInt("userID"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("lastName"),
                        rs.getString("firstName"),
                        rs.getBoolean("gender"),
                        rs.getDate("dob"),
                        rs.getString("address"),
                        rs.getInt("role"),
                        rs.getBoolean("status"),
                        rs.getString("phone"),
                        rs.getString("email")
                );
            }
        } catch (SQLException ex) {
        }
        return null;
    }

   

    // Get User by ID 
    public User getUserById(int userID) {

        try {
            String sql = "select * from [User] where userID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new User(rs.getInt("userID"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("lastName"),
                        rs.getString("firstName"),
                        rs.getBoolean("gender"),
                        rs.getDate("dob"),
                        rs.getString("address"),
                        rs.getInt("role"),
                        rs.getBoolean("status"),
                        rs.getString("phone"),
                        rs.getString("email")
                );
            }
        } catch (SQLException ex) {
        }
        return null;
    }
    // check user password 

    public boolean checkPass(String pass) {
        String sql = "select * from [User] where password = ?";
        boolean check = false;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, pass);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                check = true;
            }
        } catch (SQLException e) {
        }
        return check;
    }

    //update password
    public void updatePassword(String email, String pass) {
        String sql = "UPDATE [dbo].[User]\n"
                + "   SET [password] = ?  \n"
                + " WHERE [email] = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, pass);
            st.setString(2, email);
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    //check user email
    public boolean checkEmail(String email) {
        String sql = "select * from [User] where email = ?";
        boolean check = false;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                check = true;
            }
        } catch (SQLException e) {
        }
        return check;
    }

    public void updateUser(String username, String lastName, String firstName, boolean gender, Date dob, String address, String phone, String email) {
        try {
            String sql = "UPDATE [dbo].[User]\n"
                    + "   SET [username]=?\n"
                    + "      ,[lastName]=? \n"
                    + "      ,[firstName]=?\n"
                    + "      ,[gender]=?\n"
                    + "      ,[dob]=?\n"
                    + "      ,[address]=?\n"
                    + "      ,[phone]=?\n"
                    + "      ,[email]=?\n"
                    + " WHERE email =? and username = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, lastName);
            ps.setString(3, firstName);
            ps.setBoolean(4, gender);
            ps.setDate(5, dob);
            ps.setString(6, address);
            ps.setString(7, phone);
            ps.setString(8, email);
            ps.setString(9, email);
            ps.setString(10, username);
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public ArrayList<User> getallUser() {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "select * from [User]";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int Uid = rs.getInt("userID");
                String us = rs.getString("username");
                String pass = rs.getString("password");
                String Lname = rs.getString("lastName");
                String Fname = rs.getString("firstName");
                boolean gen = rs.getBoolean("gender");
                Date dob = rs.getDate("dob");
                String addr = rs.getString("address");
                int role = rs.getInt("role");
                boolean sta = rs.getBoolean("status");
                String phone = rs.getString("phone");
                String email = rs.getString("email");
                user.add(new User(Uid, us, pass, Lname, Fname, gen, dob, addr, role, sta, phone, email));

            }
        } catch (SQLException ex) {
        }
        return user;
    }

    public void ActiceUser(int uID) {
        try {
            String sql = "UPDATE  [User] set status ='" + 1 + "' where userID='" + uID + "'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("DELETE product 1");
        }
    }

    public void deactiveUser(int uID) {
        try {
            String sql = "UPDATE  [User] set status ='" + 0 + "' where userID='" + uID + "'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("DELETE product 1");
        }
    }

    public ArrayList<User> getUserbyName(String username) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "select * from [User] WHERE username LIKE '%" + username + "%'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int Uid = rs.getInt("userID");
                String us = rs.getString("username");
                String pass = rs.getString("password");
                String Lname = rs.getString("lastName");
                String Fname = rs.getString("firstName");
                boolean gen = rs.getBoolean("gender");
                Date dob = rs.getDate("dob");
                String addr = rs.getString("address");
                int role = rs.getInt("role");
                boolean sta = rs.getBoolean("status");
                String phone = rs.getString("phone");
                String email = rs.getString("email");
                user.add(new User(Uid, us, pass, Lname, Fname, gen, dob, addr, role, sta, phone, email));

            }
        } catch (SQLException ex) {
        }
        return user;
    }

    public ArrayList<User> getUserbyStatus(int status) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "select * from [User] WHERE status ='" + status + "'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int Uid = rs.getInt("userID");
                String us = rs.getString("username");
                String pass = rs.getString("password");
                String Lname = rs.getString("lastName");
                String Fname = rs.getString("firstName");
                boolean gen = rs.getBoolean("gender");
                Date dob = rs.getDate("dob");
                String addr = rs.getString("address");
                int role = rs.getInt("role");
                boolean sta = rs.getBoolean("status");
                String phone = rs.getString("phone");
                String email = rs.getString("email");
                user.add(new User(Uid, us, pass, Lname, Fname, gen, dob, addr, role, sta, phone, email));

            }
        } catch (SQLException ex) {
        }
        return user;
    }

    public ArrayList<User> getUserbyStaName(int status, String name) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "select * from [User] WHERE username LIKE '%" + name + "%' and status ='" + status + "'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int Uid = rs.getInt("userID");
                String us = rs.getString("username");
                String pass = rs.getString("password");
                String Lname = rs.getString("lastName");
                String Fname = rs.getString("firstName");
                boolean gen = rs.getBoolean("gender");
                Date dob = rs.getDate("dob");
                String addr = rs.getString("address");
                int role = rs.getInt("role");
                boolean sta = rs.getBoolean("status");
                String phone = rs.getString("phone");
                String email = rs.getString("email");
                user.add(new User(Uid, us, pass, Lname, Fname, gen, dob, addr, role, sta, phone, email));

            }
        } catch (SQLException ex) {
        }
        return user;
    }

    public ArrayList<User> getUserbyDate(Date from, Date to) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "select * from [User] WHERE dob BETWEEN '" + from + "' AND'" + to + "'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int Uid = rs.getInt("userID");
                String us = rs.getString("username");
                String pass = rs.getString("password");
                String Lname = rs.getString("lastName");
                String Fname = rs.getString("firstName");
                boolean gen = rs.getBoolean("gender");
                Date dob = rs.getDate("dob");
                String addr = rs.getString("address");
                int role = rs.getInt("role");
                boolean sta = rs.getBoolean("status");
                String phone = rs.getString("phone");
                String email = rs.getString("email");
                user.add(new User(Uid, us, pass, Lname, Fname, gen, dob, addr, role, sta, phone, email));

            }
        } catch (SQLException ex) {
        }
        return user;
    }

    public ArrayList<User> getUserbyDateName(String name, Date from, Date to) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "select * from [User] WHERE username LIKE '%" + name + "%' AND dob BETWEEN '" + from + "' AND'" + to + "'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int Uid = rs.getInt("userID");
                String us = rs.getString("username");
                String pass = rs.getString("password");
                String Lname = rs.getString("lastName");
                String Fname = rs.getString("firstName");
                boolean gen = rs.getBoolean("gender");
                Date dob = rs.getDate("dob");
                String addr = rs.getString("address");
                int role = rs.getInt("role");
                boolean sta = rs.getBoolean("status");
                String phone = rs.getString("phone");
                String email = rs.getString("email");
                user.add(new User(Uid, us, pass, Lname, Fname, gen, dob, addr, role, sta, phone, email));

            }
        } catch (SQLException ex) {
        }
        return user;
    }

    public ArrayList<User> getUserbyDateSta(int status, Date from, Date to) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "select * from [User] WHERE  dob BETWEEN '" + from + "' AND'" + to + "' and status ='" + status + "'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int Uid = rs.getInt("userID");
                String us = rs.getString("username");
                String pass = rs.getString("password");
                String Lname = rs.getString("lastName");
                String Fname = rs.getString("firstName");
                boolean gen = rs.getBoolean("gender");
                Date dob = rs.getDate("dob");
                String addr = rs.getString("address");
                int role = rs.getInt("role");
                boolean sta = rs.getBoolean("status");
                String phone = rs.getString("phone");
                String email = rs.getString("email");
                user.add(new User(Uid, us, pass, Lname, Fname, gen, dob, addr, role, sta, phone, email));

            }
        } catch (SQLException ex) {
        }
        return user;
    }

    public ArrayList<User> getUserbyDateStaName(String name, int status, Date from, Date to) {
        ArrayList<User> user = new ArrayList<>();
        try {
            String sql = "select * from [User] WHERE username LIKE '%" + name + "%' AND dob BETWEEN '" + from + "' AND'" + to + "' and status ='" + status + "'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int Uid = rs.getInt("userID");
                String us = rs.getString("username");
                String pass = rs.getString("password");
                String Lname = rs.getString("lastName");
                String Fname = rs.getString("firstName");
                boolean gen = rs.getBoolean("gender");
                Date dob = rs.getDate("dob");
                String addr = rs.getString("address");
                int role = rs.getInt("role");
                boolean sta = rs.getBoolean("status");
                String phone = rs.getString("phone");
                String email = rs.getString("email");
                user.add(new User(Uid, us, pass, Lname, Fname, gen, dob, addr, role, sta, phone, email));

            }
        } catch (SQLException ex) {
        }
        return user;
    }

    public static void main(String[] args) {
//        String a = "1990-01-10";
//        Date d1 = Date.valueOf(a);
//        String b = "2003-01-10";
//        Date d2 = Date.valueOf(b);
//        System.out.println(new UserDAO().getUserbyDateStaName("e", 0, d1, d2));
         
    }

}
