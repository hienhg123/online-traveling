/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import models.Business;

/**
 *
 * @author ACER
 */
public class BusinessDAO extends DBContext {

    public Business getBusiness(String email, String password) {
        try {
            String sql = "select * from [Business] where email = ? and password = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new Business(rs.getInt("busiID"), rs.getString("username"), rs.getString("password"), rs.getString("busiName"), rs.getString("email"), rs.getString("phonenumber"), rs.getBoolean("status"));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public Business getBusinessByEmail(String email) {
        try {
            String sql = "select * from [Business] where email = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new Business(rs.getInt("busiID"), rs.getString("username"), rs.getString("password"), rs.getString("busiName"), rs.getString("email"), rs.getString("phonenumber"), rs.getBoolean("status"));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
    
    public Business getBusinessById(int busiID) {
        try {
            String sql = "select * from [Business] where busiID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, busiID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new Business(rs.getInt("busiID"), rs.getString("username"), rs.getString("password"), rs.getString("busiName"), rs.getString("email"), rs.getString("phonenumber"), rs.getBoolean("status"));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public void insertBusiness(String email, String busiName, String username, String phonenumber, String address, String password, String firstName, String lastName) {
        try {
            String sql = "INSERT INTO [dbo].[Business]\n"
                    + "           ([username]\n"
                    + "           ,[busiName]\n"
                    + "           ,[email]\n"
                    + "           ,[phonenumber]\n"
                    + "           ,[status]\n"
                    + "           ,[password]\n"
                    + "           ,[address]\n"
                    + "           ,[firstName]\n"
                    + "           ,[lastName])\n"
                    + "     VALUES(?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, busiName);
            ps.setString(3, email);
            ps.setString(4, phonenumber);
            ps.setBoolean(5, true);
            ps.setString(6, password);
            ps.setString(7, address);
            ps.setString(8, firstName);
            ps.setString(9, lastName);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    //check valid business account

    public boolean checkValidUsername(String username) {
        try {
            Business busi1 = null;
            String sql = "select * from Business where username=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                busi1 = new Business(rs.getInt("busiID"), rs.getString("username"), rs.getString("password"), rs.getString("busiName"), rs.getString("email"), rs.getString("phonenumber"), rs.getBoolean("status"), rs.getString("firstName"), rs.getString("lastName"));
            }
            if (busi1 != null) {
                return false;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return true;
    }

    public boolean checkValidEmail(String email) {
        try {
            Business busi2 = null;
            String sql1 = "select * from Business where email=?";
            PreparedStatement ps1 = connection.prepareStatement(sql1);
            ps1.setString(1, email);
            ResultSet rs1 = ps1.executeQuery();
            if (rs1.next()) {
                busi2 = new Business(rs1.getInt("busiID"), rs1.getString("username"), rs1.getString("password"), rs1.getString("busiName"), rs1.getString("email"), rs1.getString("phonenumber"), rs1.getBoolean("status"), rs1.getString("firstName"), rs1.getString("lastName"));
            }
            if (busi2 != null) {
                return false;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return true;
    }

    public static void main(String[] args) {
        BusinessDAO busiDAO = new BusinessDAO();
        System.out.println(busiDAO.checkValidUsername("busi"));
    }
}
