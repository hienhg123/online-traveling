/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Amenities;
import models.Hotel;

/**
 *
 * @author Admin
 */
public class AmenitiesDAO extends DBContext {

    public List<Amenities> getAll() {
        List<Amenities> list = new ArrayList<>();
        String sql = "select * from Amenities";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Amenities a = new Amenities(rs.getInt(1), rs.getString(2));
                list.add(a);
            }
        } catch (SQLException e) {
        }

        return list;
    }

    public List<Amenities> getAmenitiesByHotel(int hotelID) {
        List<Amenities> list = new ArrayList<>();
        String sql = "select a.AmenitiesID,a.AmenitiesName from Hotel_Amenities ha\n"
                + "JOIN Amenities a ON ha.AmenitiesID = a.AmenitiesID\n"
                + "where ha.HotelId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, hotelID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Amenities a = new Amenities(rs.getInt(1), rs.getString(2));
                list.add(a);
            }
        } catch (SQLException e) {
        }

        return list;
    }

    public List<Hotel> searchHotelByOptions(String wifi, String dexe, String giadinh, String letan,
            String nhahang, String hoboi, String vatnuoi, int desID, String price1, String price2, String price3,
            String price4, String price5, String bontam, String dieuhoa, String phongtamrieng, String bancong,
            String maygiat, String cacham, String tv) {
        List<Hotel> list = new ArrayList<>();
        boolean checkDup = false;
        String sql = "select h.HotelId,h.HotelName,h.HotelAddress,h.[Description],h.HotelImage from Hotel h \n"
                + "JOIN Hotel_Amenities ha on h.HotelId = ha.HotelId\n"
                + "JOIN Destination d on h.DestinationId = d.DestinationId\n"
                + "JOIN Room r ON h.HotelId = r.HotelId\n"
                + "JOIN Room_Utility ru ON r.RoomId = ru.RoomId\n"
                + "where 1=1 AND d.DestinationId = " + desID+"\n";
        String checkPrice = "";
        String checkAmenities = "\nAND ha.AmenitiesID IN (0,0,0,0,0,0,0)";
        String checkRoomAmenities = "\nAND ru.UtilityId IN (0,0,0,0,0,0,0)";
        int countAmenities =0;
        int countRoomAmenities =0;
        //check khoang gia
        if (price1 != null) {
            checkPrice += "AND r.Price BETWEEN 0 AND 999999\n";
        }
        if (price2 != null) {
            if (checkPrice.contains("AND")) {
                checkPrice += "OR r.Price BETWEEN 1000000 AND 1999999\n";
            } else {
                checkPrice += "AND r.Price BETWEEN 1000000 AND 1999999\n";
            }
        }
        if (price3 != null) {
            if (checkPrice.contains("AND")) {
                checkPrice += "OR r.Price BETWEEN 2000000 AND 2999999\n";
            } else {
                checkPrice += "AND r.Price BETWEEN 2000000 AND 2999999\n";
            }
        }
        if (price4 != null) {
            if (checkPrice.contains("AND")) {
                checkPrice += "OR r.Price BETWEEN 3000000 AND 3999999\n";
            } else {
                checkPrice += "AND r.Price BETWEEN 3000000 AND 3999999\n";
            }
        }
        if (price5 != null) {
            if (checkPrice.contains("AND")) {
                checkPrice += "OR r.Price >= 4000000";
            } else {
                checkPrice += "AND r.Price >= 4000000";
            }
        }
        //them sql cua price vao trong cau lenh 
        sql += checkPrice;
        //check cac tien nghi khach san
        if (wifi != null) {
            checkAmenities = changeCharInPosition(24, '1', checkAmenities);
            countAmenities++;
        }
        if (dexe != null) {
            checkAmenities = changeCharInPosition(26, '2', checkAmenities);
            countAmenities++;
        }
        if (giadinh != null) {
            checkAmenities = changeCharInPosition(28, '3', checkAmenities);
            countAmenities++;
        }
        if (letan != null) {
            checkAmenities = changeCharInPosition(30, '4', checkAmenities);
            countAmenities++;
        }
        if (nhahang != null) {
            checkAmenities = changeCharInPosition(32, '5', checkAmenities);
            countAmenities++;
        }
        if (hoboi != null) {
            checkAmenities = changeCharInPosition(34, '6', checkAmenities);
            countAmenities++;
        }
        if (vatnuoi != null) {
            checkAmenities = changeCharInPosition(36, '7', checkAmenities);
            countAmenities++;
        }
        //check tien nghi phong 
        if (bontam != null) {
            checkRoomAmenities = changeCharInPosition(22, '1', checkRoomAmenities);
            countRoomAmenities++;
        }
        if (dieuhoa != null) {
            checkRoomAmenities = changeCharInPosition(24, '2', checkRoomAmenities);
            countRoomAmenities++;
        }
        if (phongtamrieng != null) {
            checkRoomAmenities = changeCharInPosition(26, '3', checkRoomAmenities);
            countRoomAmenities++;
        }
        if (bancong != null) {
            checkRoomAmenities = changeCharInPosition(28, '4', checkRoomAmenities);
            countRoomAmenities++;
        }
        if (maygiat != null) {
            checkRoomAmenities = changeCharInPosition(30, '5', checkRoomAmenities);
            countRoomAmenities++;
        }
        if (cacham != null) {
            checkRoomAmenities = changeCharInPosition(32, '6', checkRoomAmenities);
            countRoomAmenities++;
        }
        if (tv != null) {
            checkRoomAmenities = changeCharInPosition(34, '7', checkRoomAmenities);
            countRoomAmenities++;
        }

        //add cau lenh cua tien nghi vao cau sql chinh
        checkAmenities += checkRoomAmenities;
        sql += checkAmenities;
        sql += "\nGROUP BY h.HotelId,h.HotelName,h.HotelAddress,h.[Description],h.HotelImage";  
        String addCheckDistinct = "\nHAVING COUNT (DISTINCT ha.AmenitiesID) = "+countAmenities+" AND COUNT (DISTINCT ru.UtilityId) = "+countRoomAmenities;
        sql+=addCheckDistinct;
        System.out.println(sql);
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Hotel hot = new Hotel();
                hot.setHotelId(rs.getInt(1));
                hot.setHotelName(rs.getString(2));
                hot.setHotelAddress(rs.getString(3));
                hot.setDescription(rs.getString(4));
                hot.setHotelImage(rs.getString(5));
                list.add(hot);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public String changeCharInPosition(int position, char ch, String str) {
        char[] charArray = str.toCharArray();
        charArray[position] = ch;
        return new String(charArray);
    }

    public static void main(String[] args) {
        String check = "ha.AmenitiesID IN (0,0,0,0,0,0,0)";
        String check1 = "mot";
        String check2 = "hai";
        AmenitiesDAO adao = new AmenitiesDAO();
        List<Hotel> list = adao.searchHotelByOptions(check1, check1, null, null, null, null, null, 1, check1, null, 
                null, null, null,null,check1,null,null,null,null,null);
        System.out.println(list.isEmpty());
        for (Hotel h : list) {
            System.out.println(h.getHotelName());
        }
    }
}
