/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import models.Attraction;
import models.Destination;

/**
 *
 * @author ACER
 */
public class AttractionDAO extends DBContext {

    public ArrayList<Attraction> getAllAttractionByDestinationId(int DestinationId) {
        String sql = "SELECT * FROM Attraction WHERE DestinationId = ?";
        DestinationDAO dDAO = new DestinationDAO();
        ArrayList<Attraction> attList = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, DestinationId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Attraction att = new Attraction();
                att.setAttractionId(rs.getInt("AttractionId"));
                att.setAttractionName(rs.getString("AttractionName"));
                att.setDestinationId(dDAO.getDestinationById(DestinationId));
                att.setAttractionImage(rs.getString("AttractionImage"));
                att.setAttractionDescription(rs.getString("AttractionDescription"));
                att.setAttractionAddress(rs.getString("AttractionAddress"));
                att.setAttractionHotline(rs.getString("AttractionHotline"));
                attList.add(att);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return attList;
    }

    public void deleteAttractionById(int attId) {
        String sql = "DELETE FROM [dbo].[Attraction]\n"
                + "      WHERE AttractionId = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, attId);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public Attraction getAttractionById(int attId) {
        String sql = "SELECT * FROM Attraction WHERE AttractionId = ?";
        DestinationDAO dDAO = new DestinationDAO();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, attId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Attraction att = new Attraction();
                att.setAttractionId(rs.getInt("AttractionId"));
                att.setAttractionName(rs.getString("AttractionName"));
                att.setDestinationId(dDAO.getDestinationById(rs.getInt("DestinationId")));
                att.setAttractionImage(rs.getString("AttractionImage"));
                att.setAttractionDescription(rs.getString("AttractionDescription"));
                att.setAttractionAddress(rs.getString("AttractionAddress"));
                att.setAttractionHotline(rs.getString("AttractionHotline"));
                return att;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public void updateAttraction(int attId, String attName, String attImage, String attDesc, String attAddress, String attHotline) {
        String sql = "UPDATE [dbo].[Attraction]\n"
                + "   SET [AttractionName] = ?\n"
                + "      ,[AttractionImage] = ?\n"
                + "      ,[AttractionDescription] = ?\n"
                + "      ,[AttractionAddress] = ?\n"
                + "      ,[AttractionHotline] = ?\n"
                + " WHERE AttractionId = ?";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, attName);
            ps.setString(2, attImage);
            ps.setString(3, attDesc);
            ps.setString(4, attAddress);
            ps.setString(5, attHotline);
            ps.setInt(6, attId);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        AttractionDAO attDAO = new AttractionDAO();
//        ArrayList<Attraction> attList = attDAO.getAllAttractionByDestinationId(1);
//        for(Attraction att: attList){
//            System.out.println(att);
//        }
        System.out.println(attDAO.getAttractionById(1).getAttractionImage());
    }
}
