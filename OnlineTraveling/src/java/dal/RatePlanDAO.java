/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import models.Hotel;
import models.RatePlan;

/**
 *
 * @author ACER
 */
public class RatePlanDAO extends DBContext {

    public boolean checkHotelFirstBookPlan(int HotelId) {
        String sql = "SELECT * FROM RatePlan r JOIN Hotel_Plan hp ON r.PlanId = hp.PlanId WHERE hp.HotelId = ? AND r.PlanType = 1";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, HotelId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    public ArrayList<RatePlan> getAllRatePlansByHotelId(int HotelId) {
        HotelDAO hotelDAO = new HotelDAO();
        String sql = "SELECT * FROM RatePlan WHERE HotelId = ?";
        ArrayList<RatePlan> PlanList = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, HotelId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                RatePlan rp = new RatePlan();
                rp.setPlanId(rs.getInt("PlanId"));
                rp.setPlanName(rs.getString("PlanName"));
                rp.setPlanType(rs.getInt("PlanType"));
                rp.setPercentage(rs.getFloat("Percentage"));
                rp.setDay(rs.getInt("Day"));
                rp.setHotelId(hotelDAO.getHotelByHotId(HotelId));

                rp.setAvailablePayment(rs.getInt("AvailablePayment"));
                rp.setCancellationPolicy(rs.getString("CancellationPolicy"));
                rp.setMeal(rs.getBoolean("Meal"));
                PlanList.add(rp);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return PlanList;
    }

    public boolean checkUserBookingFirstTime(int userID, int HotelId) {
        String sql = "SELECT [userID], [HotelId] FROM Booking b JOIN Room r ON b.RoomId = r.RoomId WHERE [userID] = ? AND HotelId = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userID);
            ps.setInt(2, HotelId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return false;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return true;
    }

    public RatePlan getFirstBookPlan(int HotelId) {
        String sql = "SELECT * FROM RatePlan WHERE HotelId = ? AND PlanType = 1";
        HotelDAO hotelDAO = new HotelDAO();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, HotelId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                RatePlan rp = new RatePlan();
                rp.setPlanId(rs.getInt("PlanId"));
                rp.setPlanName(rs.getString("PlanName"));
                rp.setPlanType(rs.getInt("PlanType"));
                rp.setPercentage(rs.getFloat("Percentage"));
                rp.setDay(rs.getInt("Day"));
                rp.setHotelId(hotelDAO.getHotelByHotId(HotelId));

                rp.setAvailablePayment(rs.getInt("AvailablePayment"));
                rp.setCancellationPolicy(rs.getString("CancellationPolicy"));
                rp.setMeal(rs.getBoolean("Meal"));
                return rp;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public void addFirstTimeBookingPlan(String PlanName, float Percentage, int HotelId, int availablePayment, String CancellationPolicy, boolean meal) {

        String sql = "INSERT INTO [dbo].[RatePlan]\n"
                + "           ([PlanName]\n"
                + "           ,[PlanType]\n"
                + "           ,[Percentage]\n"
                + "           ,[Day]\n"
                + "           ,[HotelId]\n"
                + "           ,[AvailablePayment]\n"
                + "           ,[CancellationPolicy]\n"
                + "           ,[Meal])"
                + "     VALUES(?,?,?,NULL,?,?,?,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, PlanName);
            ps.setInt(2, 1);
            ps.setFloat(3, Percentage);

            ps.setInt(4, HotelId);
            ps.setInt(5, availablePayment);
            ps.setString(6, CancellationPolicy);
            ps.setBoolean(7, meal);

            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    public void updateFirstTimeBookingPlan(int PlanId, String PlanName, float Percentage, int availablePayment, String CancellationPolicy, boolean meal) {
        String sql = "UPDATE [dbo].[RatePlan]\n"
                + "   SET [PlanName] = ?\n"
                + "      ,[PlanType] = 1\n"
                + "      ,[Percentage] = ?\n"
                + "      ,[AvailablePayment] = ?\n"
                + "      ,[CancellationPolicy] = ?\n"
                + "      ,[Meal] = ?\n"
                + " WHERE PlanId = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, PlanName);
            ps.setFloat(2, Percentage);
            ps.setInt(3, availablePayment);
            ps.setString(4, CancellationPolicy);
            ps.setBoolean(5, meal);
            ps.setInt(6, PlanId);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void deleteRatePlan(int PlanId) {
        String sql = "DELETE FROM [dbo].[RatePlan]\n"
                + "      WHERE PlanId = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, PlanId);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    public float getLongTimeStayPercentage(Date fromDate, Date toDate, int HotelId) {
        long daysBetween = getDaysBetween(fromDate, toDate);
        System.out.println("So dem thue: " + daysBetween);
        float percentage = 0;
        ArrayList<RatePlan> planList = getAllRatePlansByHotelId(HotelId);
        for (RatePlan plan : planList) {
            if (daysBetween >= plan.getDay() && plan.getPercentage() >= percentage && plan.getDay() != 0 && plan.getPlanType() == 2) {
                System.out.println("Plan " + plan.getPlanId() + ", ngay: " + plan.getDay());
                percentage = plan.getPercentage();
            }
        }
        return percentage;
    }

    public RatePlan getLongTimeStayPlan(Date fromDate, Date toDate, int HotelId) {
        long daysBetween = getDaysBetween(fromDate, toDate);
//        System.out.println("So dem thue: " + daysBetween);
        float percentage = 0;
        RatePlan resultPlan = null;
        ArrayList<RatePlan> planList = getAllRatePlansByHotelId(HotelId);
        for (RatePlan plan : planList) {
            if (daysBetween >= plan.getDay() && plan.getPercentage() >= percentage && plan.getDay() != 0 && plan.getPlanType() == 2) {
//                System.out.println("Plan "+plan.getPlanId() + ", ngay: "+ plan.getDay());
                percentage = plan.getPercentage();
                resultPlan = plan;
            }
        }
        return resultPlan;
    }

    public long getDaysBetween(Date fromDate, Date toDate) {
        long daysBetween = 0;
        //Calculate the day
        //Convert date to localdate
        LocalDate fromLocalDate = fromDate.toLocalDate();
        LocalDate toLocalDate = toDate.toLocalDate();
        //Convert localDate to LocalDateTime
        LocalDateTime fromLocalDateTime = fromLocalDate.atStartOfDay();
        LocalDateTime toLocalDateTime = toLocalDate.atStartOfDay();
        //Calculate the day in between using Duration.between
        daysBetween = Duration.between(fromLocalDateTime, toLocalDateTime).toDays();
        return daysBetween;
    }

    public RatePlan getEarlyBookPlan(Date fromDate, Date toDate, int HotelId) {
        java.util.Date utilDate = new java.util.Date();
        java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());
        float percentage = 0;
        long daysBetween = getDaysBetween(currentDate, fromDate);
        System.out.println("Dat som: " + daysBetween + " dem");
        RatePlan resultPlan = null;
        ArrayList<RatePlan> planList = getAllRatePlansByHotelId(HotelId);
        for (RatePlan plan : planList) {
            if (plan.getPlanType() == 3 && daysBetween >= plan.getDay() && plan.getPercentage() >= percentage) {
//                System.out.println("Plan "+plan.getPlanId() + ", ngay: "+ plan.getDay());
                percentage = plan.getPercentage();
                resultPlan = plan;
            }
        }
        return resultPlan;
    }

    public boolean checkIfHotelApplyFirstTimePlan(int HotelId) {
        String sql = "SELECT * FROM RatePlan WHERE HotelId = ? AND PlanType = 1 AND [Percentage] > 0";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, HotelId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    public void addNewLongStayPlan(String PlanName, float Percentage, int Day, int HotelId, int availablePayment, String CancellationPolicy, boolean meal) {
        String sql = "INSERT INTO [dbo].[RatePlan]\n"
                + "           ([PlanName]\n"
                + "           ,[PlanType]\n"
                + "           ,[Percentage]\n"
                + "           ,[Day]\n"
                + "           ,[HotelId]\n"
                + "           ,[AvailablePayment]\n"
                + "           ,[CancellationPolicy]\n"
                + "           ,[Meal])"
                + "     VALUES(?,2,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, PlanName);
            ps.setFloat(2, Percentage);
            ps.setInt(3, Day);
            ps.setInt(4, HotelId);
            ps.setInt(5, availablePayment);
            ps.setString(6, CancellationPolicy);
            ps.setBoolean(7, meal);
            ps.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void updatePlan(int PlanId, String PlanName, float Percentage, int Day, int availablePayment, String CancellationPolicy, boolean meal) {
        String sql = "UPDATE [dbo].[RatePlan]\n"
                + "   SET [PlanName] = ?\n"
                + "      ,[Percentage] = ?\n"
                + "      ,[Day] = ?\n"
                + "      ,[AvailablePayment] = ?\n"
                + "      ,[CancellationPolicy] = ?\n"
                + "      ,[Meal] = ?\n"
                + " WHERE PlanId = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, PlanName);
            ps.setFloat(2, Percentage);
            ps.setInt(3, availablePayment);
            ps.setInt(4, Day);
            ps.setString(5, CancellationPolicy);
            ps.setBoolean(6, meal);
            ps.setInt(7, PlanId);
            ps.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public RatePlan getPlanById(int PlanId) {
        String sql = "SELECT * FROM RatePlan WHERE PlanId = ?";
        HotelDAO hotelDAO = new HotelDAO();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, PlanId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                RatePlan rp = new RatePlan();
                rp.setPlanId(rs.getInt("PlanId"));
                rp.setPlanName(rs.getString("PlanName"));
                rp.setPlanType(rs.getInt("PlanType"));
                rp.setPercentage(rs.getFloat("Percentage"));
                rp.setDay(rs.getInt("Day"));
                rp.setHotelId(hotelDAO.getHotelByHotId(rs.getInt("HotelId")));

                rp.setAvailablePayment(rs.getInt("AvailablePayment"));
                rp.setCancellationPolicy(rs.getString("CancellationPolicy"));
                rp.setMeal(rs.getBoolean("Meal"));
                return rp;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public ArrayList<RatePlan> getSameTypeAndHotelPlan(int PlanId) {
        RatePlan plan = getPlanById(PlanId);
        ArrayList<RatePlan> planList = new ArrayList<>();
        HotelDAO hotelDAO = new HotelDAO();
        String sql = "SELECT * FROM RatePlan WHERE PlanType = ? AND HotelId = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, plan.getPlanType());
            ps.setInt(2, plan.getHotelId().getHotelId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                RatePlan rp = new RatePlan();
                rp.setPlanId(rs.getInt("PlanId"));
                rp.setPlanName(rs.getString("PlanName"));
                rp.setPlanType(rs.getInt("PlanType"));
                rp.setPercentage(rs.getFloat("Percentage"));
                rp.setDay(rs.getInt("Day"));
                rp.setHotelId(hotelDAO.getHotelByHotId(rs.getInt("HotelId")));

                rp.setAvailablePayment(rs.getInt("AvailablePayment"));
                rp.setCancellationPolicy(rs.getString("CancellationPolicy"));
                rp.setMeal(rs.getBoolean("Meal"));
                planList.add(rp);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return planList;
    }

    public boolean checkReasonablePlan(int PlanId) {
        ArrayList<RatePlan> planList = getSameTypeAndHotelPlan(PlanId);
        RatePlan plan = getPlanById(PlanId);
        for (RatePlan loopPlan : planList) {
            if (plan.getPercentage() < loopPlan.getPercentage() && plan.getDay() > loopPlan.getDay()) {
                return false;
            }
        }
        return true;
    }

    public void addNewEarlyBookPlan(String PlanName, float Percentage, int Day, int HotelId) {
        String sql = "INSERT INTO [dbo].[RatePlan]\n"
                + "           ([PlanName]\n"
                + "           ,[PlanType]\n"
                + "           ,[Percentage]\n"
                + "           ,[Day]\n"
                + "           ,[HotelId])\n"
                + "     VALUES(?,3,?,?,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, PlanName);
            ps.setFloat(2, Percentage);
            ps.setInt(3, Day);
            ps.setInt(4, HotelId);
            ps.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public ArrayList<RatePlan> getLongTimeStayPlanList(Date fromDate, Date toDate, int HotelId) {
        long daysBetween = getDaysBetween(fromDate, toDate);
        ArrayList<RatePlan> longStayList = new ArrayList<>();
//        System.out.println("So dem thue: " + daysBetween);

        ArrayList<RatePlan> planList = getAllRatePlansByHotelId(HotelId);
        for (RatePlan plan : planList) {
            if (daysBetween >= plan.getDay() && plan.getDay() != 0 && plan.getPlanType() == 2) {
//                System.out.println("Plan "+plan.getPlanId() + ", ngay: "+ plan.getDay());
                longStayList.add(plan);
            }
        }
        return longStayList;
    }

    public ArrayList<RatePlan> getEarlyBookPlanList(Date fromDate, Date toDate, int HotelId) {
        java.util.Date utilDate = new java.util.Date();
        java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());
        long daysBetween = getDaysBetween(currentDate, fromDate);
        ArrayList<RatePlan> planList = getAllRatePlansByHotelId(HotelId);
        ArrayList<RatePlan> earlyBookList = new ArrayList<>();
        for (RatePlan plan : planList) {
            if (plan.getPlanType() == 3 && daysBetween >= plan.getDay()) {
//                System.out.println("Plan "+plan.getPlanId() + ", ngay: "+ plan.getDay());
                earlyBookList.add(plan);
            }
        }
        return earlyBookList;
    }

    public ArrayList<RatePlan> getAvailableRatePlans(int userID, int HotelId, Date fromDate, Date toDate) {
        ArrayList<RatePlan> planList = new ArrayList<>();

        //Adding first time booking rate plan to the list
        RatePlan firstBookPlan = null;
        if (checkUserBookingFirstTime(userID, HotelId) == true) {
            firstBookPlan = getFirstBookPlan(HotelId);
        }
        if (firstBookPlan != null) {
            planList.add(firstBookPlan);
        }

        //Adding long stay rate plan to the list
        ArrayList<RatePlan> longStayList = getLongTimeStayPlanList(fromDate, toDate, HotelId);
        for (RatePlan longStay : longStayList) {
            planList.add(longStay);
        }
        //Adding early booking rate plan
        ArrayList<RatePlan> earlyBookList = getEarlyBookPlanList(fromDate, toDate, HotelId);
        for (RatePlan earlyBook : earlyBookList) {
            planList.add(earlyBook);
        }
        ArrayList<RatePlan> standardPlanList = getCustomPlanList(HotelId);
        for (RatePlan standardPlan : standardPlanList) {
            planList.add(standardPlan);
        }
        return planList;
    }

    public ArrayList<RatePlan> getAvailableRatePlansNoLogin(int HotelId, Date fromDate, Date toDate) {
        ArrayList<RatePlan> planList = new ArrayList<>();

        //Adding long stay rate plan to the list
        ArrayList<RatePlan> longStayList = getLongTimeStayPlanList(fromDate, toDate, HotelId);
        for (RatePlan longStay : longStayList) {
            planList.add(longStay);
        }
        //Adding early booking rate plan
        ArrayList<RatePlan> earlyBookList = getEarlyBookPlanList(fromDate, toDate, HotelId);
        for (RatePlan earlyBook : earlyBookList) {
            planList.add(earlyBook);
        }
        return planList;
    }

    public int countAvailablePlan(ArrayList<RatePlan> planList) {
        int count;
        count = planList.size();
        return count;
    }

    public void addCustomPlan(String PlanName, float Percentage, int HotelId, int availablePayment, String CancellationPolicy, boolean meal) {

        String sql = "INSERT INTO [dbo].[RatePlan]\n"
                + "           ([PlanName]\n"
                + "           ,[PlanType]\n"
                + "           ,[Percentage]\n"
                + "           ,[Day]\n"
                + "           ,[HotelId]\n"
                + "           ,[AvailablePayment]\n"
                + "           ,[CancellationPolicy]\n"
                + "           ,[Meal])"
                + "     VALUES(?,?,?,NULL,?,?,?,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, PlanName);
            ps.setInt(2, 4);
            ps.setFloat(3, Percentage);

            ps.setInt(4, HotelId);
            ps.setInt(5, availablePayment);
            ps.setString(6, CancellationPolicy);
            ps.setBoolean(7, meal);

            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    public RatePlan getCustomPlan(int HotelId) {
        String sql = "SELECT * FROM RatePlan WHERE HotelId = ? AND PlanType = 4";
        HotelDAO hotelDAO = new HotelDAO();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, HotelId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                RatePlan rp = new RatePlan();
                rp.setPlanId(rs.getInt("PlanId"));
                rp.setPlanName(rs.getString("PlanName"));
                rp.setPlanType(rs.getInt("PlanType"));
                rp.setPercentage(rs.getFloat("Percentage"));
                rp.setDay(rs.getInt("Day"));
                rp.setHotelId(hotelDAO.getHotelByHotId(HotelId));

                rp.setAvailablePayment(rs.getInt("AvailablePayment"));
                rp.setCancellationPolicy(rs.getString("CancellationPolicy"));
                rp.setMeal(rs.getBoolean("Meal"));
                return rp;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public ArrayList<RatePlan> getCustomPlanList(int HotelId) {
        ArrayList<RatePlan> planList = getAllRatePlansByHotelId(HotelId);
        ArrayList<RatePlan> CustomPlanList = new ArrayList<>();
        for (RatePlan plan : planList) {
            if (plan.getPlanType() == 4) {
//                System.out.println("Plan "+plan.getPlanId() + ", ngay: "+ plan.getDay());
                CustomPlanList.add(plan);
            }
        }
        return CustomPlanList;
    }

    public static void main(String[] args) {
        RatePlanDAO RPDAO = new RatePlanDAO();
//        ArrayList<RatePlan> PlanList = RPDAO.getAllRatePlansByHotelId(1);
//        for (RatePlan rp : PlanList) {
//            System.out.println(rp.getPlanName() + "     " + rp.getPercentage() + "      " + rp.getDay());
//        }
//        RPDAO.getFirstBookPlan(1);
//        System.out.println(RPDAO.getFirstBookPlan(2));

        String from = "2022-10-30";
        Date d1 = Date.valueOf(from);
        String to = "2022-11-30";
        Date d2 = Date.valueOf(to);
        ArrayList<RatePlan> PlanList = RPDAO.getCustomPlanList(1);
        for (RatePlan rp : PlanList) {
            System.out.println(rp.getPlanId());
        }
//        RPDAO.updatePlan(3, "Duh", 11, 7, 2, "Flexible", true);
//        //Convert to local date
//        LocalDate date1 = d1.toLocalDate();
//        LocalDate date2 = d2.toLocalDate();
//        //Convert local date to LocalDateTime
//        LocalDateTime localDateTime1 = date1.atStartOfDay();
//        LocalDateTime localDateTime2 = date2.atStartOfDay();
//        long daysBetween = Duration.between(localDateTime1, localDateTime2).toDays();
//        System.out.println("Days: " + daysBetween);
//        RPDAO.addFirstTimeBookingPlan(12, 1);
//        RPDAO.deleteRatePlan(9);
//        System.out.println(RPDAO.getLongTimeStayPercentage(d1, d2, 1));
//        System.out.println(RPDAO.getLongTimeStayPlan(d1, d2, 1).getPercentage());
//        java.util.Date utilDate = new java.util.Date();
//        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
//        System.out.println(sqlDate);
//        System.out.println(RPDAO.getEarlyBookPlan(d1, d2, 1));
//        System.out.println(RPDAO.checkIfHotelApplyFirstTimePlan(3));
//        RPDAO.updateLongStayPlan(5, "Funny Update", 5, 8);
//        System.out.println(RPDAO.getPlanById(6).getHotelId().getHotelName());
//        System.out.println(RPDAO.checkReasonablePlan(14));

//        ArrayList<RatePlan> rplist = RPDAO.getAllRatePlansByHotelId(1);
//
//        for (RatePlan rp : rplist) {
//            System.out.println(rp);
//        }
    }

}
