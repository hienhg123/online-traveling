/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Booking;
import models.Hotel;
import models.Room;
import models.RoomType;

/**
 *
 * @author ACER
 */
public class BookingDAO extends DBContext {

    //Lay danh sach cac phong co the thue trong khoang nay
    public ArrayList<Room> getAvailableRooms(Date FromDate, Date ToDate, int HotelId) {
        ArrayList<Room> roomList = new ArrayList<>();
        RoomDao roomDAO = new RoomDao();
        String sql = "SELECT * FROM Room WHERE HotelId = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, HotelId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (getNumberOfAvailableRoom(FromDate, ToDate, rs.getInt("RoomId")) > 0) {
                    roomList.add(new Room(rs.getInt("RoomId"),
                            rs.getString("RoomName"),
                            rs.getString("RoomDescription"),
                            rs.getString("RoomImage"),
                            rs.getInt("Price"),
                            rs.getFloat("Size"),
                            rs.getInt("Quantity"),
                            getRoomTypeByRoomTypeId(rs.getInt("RoomTypeId")),
                            getHotelByHotelId(rs.getInt("HotelId")),
                            getNumberOfAvailableRoom(FromDate, ToDate, rs.getInt("RoomId")),
                            roomDAO.getRoomAverageRating(rs.getInt("RoomId"))));
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return roomList;
    }

    //Kiem tra tu ngay FromDate, den ngay ToDate voi phong RoomId thi co bao nhieu phong co the cho thue.
    public int getNumberOfAvailableRoom(Date FromDate, Date ToDate, int RoomId) {
        int Quantity = getRoomQuantity(RoomId);
        int Occupied = 0;
        String sql = "select * from Booking WHERE RoomId = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, RoomId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (ToDate.before(rs.getDate("FromDate")) || FromDate.after(rs.getDate("ToDate"))) {

                } else {
                    Occupied++;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return Quantity - Occupied;
    }

    public int getRoomQuantity(int RoomId) {
        String sql = "SELECT Quantity FROM Room WHERE RoomId = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, RoomId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("Quantity");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }

    public RoomType getRoomTypeByRoomTypeId(int roomTypeID) {
        String sql = "SELECT * FROM RoomType WHERE roomTypeID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, roomTypeID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new RoomType(rs.getInt(1), rs.getString(2), rs.getString(3));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public Hotel getHotelByHotelId(int HotelId) {
        String sql = "SELECT * FROM Hotel WHERE HotelId = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, HotelId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new Hotel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6), rs.getInt(7));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

//    public void AddBookingWithDiscount(int RoomId, int userID, int DiscountId, Date ToDate, Date FromDate, float Price) {
//        String sql = "INSERT INTO [dbo].[Booking]\n"
//                + "           ([RoomId]\n"
//                + "           ,[userID]\n"
//                + "           ,[DiscountId]\n"
//                + "           ,[ToDate]\n"
//                + "           ,[FromDate]\n"
//                + "           ,[Status]\n"
//                + "           ,[Price])\n"
//                + "     VALUES(?,?,?,?,?,?,?)";
//
//        try {
//            PreparedStatement ps = connection.prepareStatement(sql);
//            ps.setInt(1, RoomId);
//            ps.setInt(2, userID);
//            ps.setInt(3, DiscountId);
//            ps.setDate(4, ToDate);
//            ps.setDate(5, FromDate);
//            ps.setString(6, "Check-in");
//            ps.setFloat(7, Price);
//            ps.executeUpdate();
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }
//    }
    public void AddBooking(int RoomId, int userID, Date ToDate, Date FromDate, float Price, int amount, int PlanId, int PaymentID) {
        DiscountDAO discountDAO = new DiscountDAO();
        int discountId = 0;
        try {
            discountId = discountDAO.getDiscountIdByDate(FromDate, ToDate);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        //If there's no discount
        if (discountId == 0) {
            for (int i = 0; i < amount; i++) {
                String sql = "INSERT INTO [dbo].[Booking]\n"
                        + "           ([RoomId]\n"
                        + "           ,[userID]\n"
                        + "           ,[ToDate]\n"
                        + "           ,[FromDate]\n"
                        + "           ,[Status]\n"
                        + "           ,[Price]\n"
                        + "           ,[PlanId]\n"
                        + "           ,[PaymentID])\n"
                        + "     VALUES(?,?,?,?,?,?,?,?)";

                try {
                    PreparedStatement ps = connection.prepareStatement(sql);
                    ps.setInt(1, RoomId);
                    ps.setInt(2, userID);
                    ps.setDate(3, ToDate);
                    ps.setDate(4, FromDate);
                    ps.setString(5, "Check-in");
                    ps.setFloat(6, Price);
                    ps.setInt(7, PlanId);
                    ps.setInt(8, PaymentID);
                    ps.executeUpdate();
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }
            }
        } //If there is discount available
        else {
            for (int i = 0; i < amount; i++) {
                String sql = "INSERT INTO [dbo].[Booking]\n"
                        + "           ([RoomId]\n"
                        + "           ,[userID]\n"
                        + "           ,[DiscountId]\n"
                        + "           ,[ToDate]\n"
                        + "           ,[FromDate]\n"
                        + "           ,[Status]\n"
                        + "           ,[Price])\n"
                        + "     VALUES(?,?,?,?,?,?,?)";

                try {
                    PreparedStatement ps = connection.prepareStatement(sql);
                    ps.setInt(1, RoomId);
                    ps.setInt(2, userID);
                    ps.setInt(3, discountId);
                    ps.setDate(4, ToDate);
                    ps.setDate(5, FromDate);
                    ps.setString(6, "Check-in");
                    ps.setFloat(7, Price);
                    ps.executeUpdate();
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    public List<Booking> getAllHistoryBooking(int userID) {
        List<Booking> listBook = new ArrayList<>();
        try {
            String sql = "select h.HotelName,b.BookingID,b.FromDate,b.ToDate,b.[Status],b.RoomId,r.RoomName,b.Price,b.DiscountId from Booking b\n"
                    + "JOIN Room r on b.RoomId = r.RoomId\n"
                    + "JOIN Hotel h ON h.HotelId = r.HotelId\n"
                    + "where b.userID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userID);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Booking b = new Booking(rs.getInt(2), rs.getInt(6), userID, rs.getInt(9),
                        rs.getDate(4), rs.getDate(3), rs.getString(5), 0, rs.getFloat(8), rs.getString(7),
                        rs.getString(1));
                listBook.add(b);
            }

        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listBook;
    }

    public boolean checkInvalidBooking(int useID) {
        try {
            boolean check;
            String sql = "select b.BookingId from [User] u left join Booking b \n"
                    + "  on u.userID = b.userID where u.userID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, useID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Booking b = new Booking();
                b.setBookingId(rs.getInt("BookingId"));
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public float getPriceByRoomAndDate(int RoomId, Date fromDate, Date toDate) {
        RoomDao RoomDAO = new RoomDao();
        Room room = RoomDAO.GetRoomByRoomId(RoomId);
        DiscountDAO discountDAO = new DiscountDAO();
        int percentage = discountDAO.getPercentageByDate(fromDate, toDate);
        float price = room.getPrice() - (room.getPrice() * percentage);
        return price;
    }

    public void cancelBooking(String id) {
        String sql = "UPDATE [dbo].[Booking]\n"
                + "   SET [Status] = ?\n"
                + " WHERE BookingId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "Cancelled");
            st.setString(2, id);
            st.executeUpdate();
            st.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void submitRating(int BookingId, float Rating, String Comment) {
        String sql = "UPDATE [dbo].[Booking]\n"
                + "   SET [Rating] = ?\n"
                + "      ,[Comment] = ?\n"
                + " WHERE [BookingId] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setFloat(1, Rating);
            st.setString(2, Comment);
            st.setInt(3, BookingId);
            st.executeUpdate();
            st.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public Booking getBookingById(int BookingId) {
        String sql = "SELECT [BookingId]\n"
                + "      ,[RoomId]\n"
                + "      ,[userID]\n"
                + "      ,[DiscountId]\n"
                + "      ,[ToDate]\n"
                + "      ,[FromDate]\n"
                + "      ,[Status]\n"
                + "      ,[Rating]\n"
                + "      ,[Price]\n"
                + "      ,[Comment]\n"
                + "  FROM [dbo].[Booking]\n"
                + "  WHERE BookingId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, BookingId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Booking(rs.getInt("BookingId"), rs.getInt("RoomId"), rs.getInt("userID"), rs.getInt("DiscountId"), rs.getDate("ToDate"), rs.getDate("FromDate"), rs.getString("Status"), rs.getFloat("Rating"), rs.getFloat("Price"), rs.getString("Comment"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public Booking getDetailBooking(String bookingID, int userID) {
        String sql = "select b.BookingId,b.RoomId,b.userID,b.DiscountId,b.ToDate,b.FromDate,b.[Status],b.Price,\n"
                + "r.RoomName,h.HotelName,h.HotelId from Booking b\n"
                + "JOIN Room r on b.RoomId = r.RoomId\n"
                + "JOIN Hotel h ON h.HotelId = r.HotelId\n"
                + "where b.userID = ? and b.BookingId = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userID);
            st.setString(2, bookingID);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Booking b = new Booking(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getDate(5),
                        rs.getDate(6), rs.getString(7), rs.getFloat(8),
                        rs.getString(9), rs.getString(10), rs.getString(11));
                return b;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public static void main(String[] args) {
        BookingDAO bookingDAO = new BookingDAO();
//        System.out.println(bookingDAO.getRoomQuantity(1));
        String from = "2022-10-10";
        Date d1 = Date.valueOf(from);
        String to = "2022-10-12";
        Date d2 = Date.valueOf(to);
//        ArrayList<Room> roomList = bookingDAO.getAvailableRooms(d1, d2, 1);
//        for (Room r : roomList) {
//            System.out.println(r.getRoomId() + " Danh gia: " + r.getAvgRating());
//        }
        bookingDAO.AddBooking(1, 1, d1, d2, 2500000, 2, 1, 1);
//        List<Booking> bookingList = new ArrayList<>();
//        bookingList = bookingDAO.getAllHistoryBooking(1);
//        Booking booked = bookingDAO.getDetailBooking("4", 1);
//        System.out.println(booked.getFromDate());
//        bookingDAO.cancelBooking("6");
    }

}
