/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Business;
import models.Hotel;
import models.Question;
import models.Report;
import models.Room;
import models.RoomType;
import models.User;

/**
 *
 * @author HONG QUAN
 */
public class QuestionDAO extends DBContext {

    public ArrayList<Question> getALlListQuestionByRoomId(int RoomId) {
        ArrayList<Question> questions = new ArrayList<>();
        try {
            String sql = "SELECT q.QuestionId,q.userID,q.AnswerContent,q.dateAnswer,q.dateQuestion,q.QuestionContent,u.username,r.RoomId FROM Question q LEFT JOIN [User] u\n"
                    + "                ON q.userID = u.userID\n"
                    + "			  LEFT JOIN Room r\n"
                    + "  		  ON q.RoomId = r.RoomId\n"
                    + "			  where r.RoomId = ?\n"
                    + "              ORDER BY q.QuestionId DESC";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, RoomId);
            ResultSet rs = ps.executeQuery();
            Question current = new Question();
            current.setQuestionId(0);
            while (rs.next()) {
                int qid = rs.getInt("QuestionId");
                if (qid != current.getQuestionId()) {
                    current = new Question();
                    current.setQuestionId(qid);
                    current.setQuestionContent(rs.getString("QuestionContent"));
                    current.setDateQuestion(rs.getString("dateQuestion"));
                    current.setAnswerContent(rs.getString("AnswerContent"));
                    current.setDateAnswer(rs.getString("dateAnswer"));
                    User u_user = new User();
                    u_user.setUserName(rs.getString("username"));
                    u_user.setUserID(rs.getInt("userID"));
                    Room r_room = new Room();
                    r_room.setRoomId(rs.getInt("RoomId"));
                    current.setUser(u_user);
                    current.setRoom(r_room);
                    questions.add(current);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return questions;
    }

    public ArrayList<Question> getAllListQuestionByBusiness(int busi) {
        ArrayList<Question> questions = new ArrayList<>();
        try {
            String sql = "  select h.busiID,h.HotelName,r.RoomId,q.QuestionId,q.AnswerContent,q.dateQuestion,q.QuestionContent,q.RoomId,u.username,u.firstName,u.lastName,rt.roomTypeName,u.phone,u.email,r.Price from Business b inner join Hotel h on b.busiID = h.busiID\n"
                    + "							inner join Room r on h.HotelId = r.HotelId inner join RoomType rt on r.RoomTypeId = rt.roomTypeID \n"
                    + "							 right join Question q on r.RoomId = q.RoomId\n"
                    + "							left join [User] u on q.userID = u.userID\n"
                    + "							where h.busiId = ?\n"
                    + "							ORDER BY q.QuestionId DESC";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, busi);
            ResultSet rs = ps.executeQuery();
            Question current = new Question();
            current.setQuestionId(0);
            while (rs.next()) {
                int qid = rs.getInt("QuestionId");
                if (qid != current.getQuestionId()) {
                    current = new Question();
                    current.setQuestionId(qid);
                    current.setQuestionContent(rs.getString("QuestionContent"));
                    current.setDateQuestion(rs.getString("dateQuestion"));
                    current.setAnswerContent(rs.getString("AnswerContent"));
                    User u_user = new User();
                    u_user.setFirstName(rs.getString("firstName"));
                    u_user.setLastName(rs.getString("lastName"));
                    u_user.setUserName(rs.getString("username"));
                    u_user.setEmail(rs.getString("email"));
                    u_user.setPhone(rs.getString("phone"));
                    Room r_room = new Room();
                    Hotel h_hotel = new Hotel();
                    h_hotel.setHotelName(rs.getString("HotelName"));
                    r_room.setHotel(h_hotel);
                    r_room.setRoomId(rs.getInt("RoomId"));
                    r_room.setPrice(rs.getInt("price"));
                    RoomType r_type = new RoomType();
                    r_type.setRoomTypeName(rs.getString("roomTypeName"));
                    r_room.setRoomType(r_type);
                    current.setUser(u_user);
                    current.setRoom(r_room);
                    questions.add(current);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return questions;
    }

    // function use to insert question for room
    public void insert(Question question) {

        try {
            String sql = "INSERT INTO [dbo].[Question]\n"
                    + "           ([QuestionContent]\n"
                    + "           ,[RoomId]\n"
                    + "           ,[userID]\n"
                    + "           ,[dateQuestion])\n"
                    + "     VALUES\n"
                    + "           (?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);

            ps.setString(1, question.getQuestionContent());
            ps.setInt(2, question.getRoom().getRoomId());
            ps.setInt(3, question.getUser().getUserID());
            ps.setString(4, question.getDateQuestion());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    // function use to answer question for user from business
    public void answerCustomerQuestion(Question question) {
        try {
            String sql = "UPDATE [dbo].[Question]\n"
                    + "   SET [AnswerContent] = ?\n"
                    + "      ,[busiID] = ?\n"
                    + "      ,[dateAnswer] = ?\n"
                    + " WHERE QuestionId = ?";
            PreparedStatement ps = connection.prepareStatement(sql);

            ps.setString(1, question.getAnswerContent());
            ps.setInt(2, question.getBusiness().getBusiID());
            ps.setString(3, question.getDateAnswer());
            ps.setInt(4, question.getQuestionId());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    public void deleteQuestion(int Questionid) {
        String sql = "DELETE FROM [dbo].[Question] WHERE QuestionId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, Questionid);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void deleteReport(int ReportID) {
        String sql = "DELETE FROM [dbo].[ReportQuestion]\n"
                + "   WHERE ReportID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, ReportID);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    // this function will show all report
    public List<Report> listAllReport() {
        List<Report> listReport = new ArrayList<>();
        try {
            String sql = "select rq.dateReport,rq.ReportId,ur.username as [usernamereport],rq.reportContent,rq.userID as [reportuserID],q.userID,u.username,q.QuestionContent,u.email,q.QuestionId from \n"
                    + "			[User] ur inner join\n"
                    + "			ReportQuestion rq\n"
                    + "			on ur.userID = rq.userID\n"
                    + "			inner join Question q\n"
                    + "				on rq.QuestionId = q.QuestionId\n"
                    + "				inner join [User] u \n"
                    + "				on q.userID = u.userID";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Report report = new Report();
                report.setReportId(rs.getInt("ReportId"));
                User u_report = new User();
                u_report.setUserID(rs.getInt("reportuserID"));
                u_report.setUserName(rs.getString("usernamereport"));
                report.setUser(u_report);
                Question q = new Question();
                q.setQuestionContent(rs.getString("QuestionContent"));
                q.setQuestionId(rs.getInt("QuestionId"));
                User u = new User();
                u.setUserName(rs.getString("username"));
                u.setUserID(rs.getInt("userID"));
                u.setEmail(rs.getString("email"));
                q.setUser(u);
                report.setDateReport(rs.getString("dateReport"));

                report.setReportContent(rs.getString("reportContent"));
                report.setQuestion(q);
                listReport.add(report);
            }

        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return listReport;
    }

    // This function will update report 
    public void InsertReportQuestion(Report report) {
        String sql = "INSERT INTO [dbo].[ReportQuestion]\n"
                + "           ([reportContent]\n"
                + "           ,[dateReport]\n"
                + "           ,[userID]\n"
                + "           ,[QuestionId]\n"
                + " ,[status])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,0)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, report.getReportContent());
            ps.setString(2, report.getDateReport());
            ps.setInt(3, report.getUser().getUserID());
            ps.setInt(4, report.getQuestion().getQuestionId());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    // update static report
    public void UpdateReportWithStatus(int reportid) {
        try {
            String sql = "UPDATE [dbo].[ReportQuestion] SET [status] = 1 WHERE ReportId = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, reportid);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    public List<Report> getQuestionReport() {
        List<Report> listR = new ArrayList<>();
        try {
            String sql = "select rq.[status],rq.reportContent,q.QuestionId,q.QuestionContent,q.userID,u.username,u.email from ReportQuestion rq inner join Question q\n"
                    + "			on rq.QuestionId = q.QuestionId inner join [User] u on q.userID = u.userID\n"
                    + "where rq.[status] = 1";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Report report = new Report();
                report.setReportContent(rs.getString("reportContent"));
                Question question = new Question();
                User user = new User();
                user.setUserName(rs.getString("username"));
                user.setEmail(rs.getString("email"));
                question.setUser(user);
                question.setQuestionId(rs.getInt("QuestionId"));
                question.setQuestionContent(rs.getString("QuestionContent"));
                report.setQuestion(question);
                 listR.add(report);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listR;
    }

    public static void main(String[] args) {
        QuestionDAO question = new QuestionDAO();
       List<Report> report = question.getQuestionReport();
        for (Report report1 : report) {
            System.out.println(report1);
        }
    }

}
