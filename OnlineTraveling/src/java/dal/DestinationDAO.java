/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Destination;

/**
 *
 * @author Admin
 */
public class DestinationDAO extends DBContext {

    public List<Destination> getAll() {
        List<Destination> list = new ArrayList<>();
        String sql = "select * from Destination";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            //loop until there is no row 
            while (rs.next()) {
                Destination a = new Destination(rs.getInt(1), rs.getString(2), rs.getString(3));
                list.add(a);
            }
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DestinationDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return list;
    }

    public Destination getDestinationByHotel(int hotelID) {
        String sql = "select d.DestinationId,d.DestinationName,d.DestinationImage from Destination d JOIN Hotel h\n"
                + "ON d.DestinationId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, hotelID);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Destination d = new Destination(rs.getInt(1), rs.getString(2), rs.getString(3));
                return d;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public Destination getDestinationById(int DestinationId) {
        String sql = "SELECT * FROM Destination WHERE DestinationId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, DestinationId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Destination d = new Destination(rs.getInt(1), rs.getString(2), rs.getString(3));
                return d;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    
    
    public static void main(String[] args) {
        DestinationDAO ddao = new DestinationDAO();
        Destination d = ddao.getDestinationByHotel(1);
        System.out.println(d.getDestinationName());
    }
}
