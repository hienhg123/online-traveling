/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import models.DiscountByDate;

/**
 *
 * @author Admin
 */
public class DiscountDAO extends DBContext {

    public List<DiscountByDate> getAllDiscount() {
        List<DiscountByDate> list = new ArrayList<>();
        String sql = "SELECT * FROM DiscountByDate";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                DiscountByDate dbd = new DiscountByDate(rs.getInt(1),
                        rs.getDate(2),
                        rs.getDate(3),
                        rs.getInt(4),
                        rs.getBoolean(5));
                list.add(dbd);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public int getPercentageByDate(Date fromDate, Date toDate) {
        List<DiscountByDate> dbd = getAllDiscount();
        int percentage = 0;
        LocalDate fromDate_localDate = fromDate.toLocalDate();
        for (DiscountByDate d : dbd) {
            if (fromDate_localDate.isEqual(d.getDiscountFrom().toLocalDate())) {
                percentage = d.getPercentage();
            }
        }
        return percentage;
    }

    public int getDiscountIdByDate(Date fromDate, Date toDate) {
        List<DiscountByDate> dbd = getAllDiscount();
        int id = 0;
        LocalDate fromDate_localDate = fromDate.toLocalDate();
        for (DiscountByDate d : dbd) {
            if (fromDate_localDate.isEqual(d.getDiscountFrom().toLocalDate())) {
                id = d.getDiscountId();
            }
        }
        return id;
    }

    public static void main(String[] args) {
        String from = "2022-10-02";
        Date d1 = Date.valueOf(from);
        LocalDate checkd1 = d1.toLocalDate();
        String to = "2022-10-03";
        Date d2 = Date.valueOf(to);
        DiscountDAO ddao = new DiscountDAO();
//        List<DiscountByDate> dbd = ddao.getAllDiscount();
//        int a = 0;
//        for (DiscountByDate d : dbd) {
//            if (checkd1.isEqual(d.getDiscountFrom().toLocalDate())) {
//                a = d.getPercentage();
//            }
//        }

        System.out.println("Sale: " + ddao.getDiscountIdByDate(d1, d2));
    }
}
