/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Hotel;

/**
 *
 * @author admin
 */
public class HotelDAO extends DBContext {
    
    public List<Hotel> getAllHotelByBusinessId(int busiID) {
        List<Hotel> listHotel = new ArrayList<>();
        try {
            String sql = "select * from Hotel where busiID = ?";

            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, busiID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Hotel hot = new Hotel();
                hot.setHotelId(rs.getInt("HotelId"));
                hot.setHotelName(rs.getString("HotelName"));
                hot.setHotelAddress(rs.getString("HotelAddress"));
                hot.setDescription(rs.getString("Description"));
                hot.setHotelImage(rs.getString("HotelImage"));
                listHotel.add(hot);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(HotelDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(HotelDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return listHotel;
    }

    public List<Hotel> getAllHotelsByDesId(int desId) {
        List<Hotel> listHotels = new ArrayList<>();
        try {
            String sql = "  select b.HotelId, b.HotelName,b.Description,b.HotelAddress,b.HotelImage from\n"
                    + " (select distinct h.HotelId from Hotel h join Room r\n"
                    + "on h.HotelId = r.HotelId\n"
                    + "where r.Price in (select min(Price) from Room group by HotelId) and h.DestinationId = ?) AS A JOIN hotel b\n"
                    + "on a.HotelId = b.HotelId";

            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, desId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                Hotel hot = new Hotel();
                hot.setHotelId(rs.getInt("HotelId"));
                hot.setHotelName(rs.getString("HotelName"));
                hot.setHotelAddress(rs.getString("HotelAddress"));
                hot.setDescription(rs.getString("Description"));
                hot.setHotelImage(rs.getString("HotelImage"));
                listHotels.add(hot);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(HotelDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(HotelDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return listHotels;
    }

    public Hotel getHotelByHotId(int hotId) {
        try {
            String sql = "SELECT [HotelId], [HotelName], [HotelAddress],[Description], [HotelImage], [busiID] from Hotel where HotelId = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, hotId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Hotel hot = new Hotel();
                hot.setHotelId(rs.getInt("HotelId"));
                hot.setHotelName(rs.getString("HotelName"));
                hot.setHotelAddress(rs.getString("HotelAddress"));
                hot.setDescription(rs.getString("Description"));
                hot.setHotelImage(rs.getString("HotelImage"));
                hot.setAvgRating(getAverageHotelRating(rs.getInt("HotelId")));
                hot.setBusiID(rs.getInt("busiID"));
                return hot;
            }
        } catch (SQLException ex) {
            Logger.getLogger(HotelDAO.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;

    }

    public int getRatingStar(int HotelId) {
        int star = 0;
        String sql = "select (SUM(Rating)/Count(*)) from Booking b left join Room r on b.RoomId = r.RoomId where r.HotelId = ? and b.Rating IS NOT NULL";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, HotelId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                star = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HotelDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(HotelDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return star;
    }

    public List<Hotel> getListByPage(List<Hotel> list, int start, int end) {
        ArrayList<Hotel> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public List<Hotel> getHotelByDestination(String destination) {
        List<Hotel> list = new ArrayList<>();
        String sql = "select h.HotelId,h.HotelName,h.HotelImage,h.HotelAddress,d.DestinationId,h.busiID from Hotel h \n"
                + "JOIN Destination d ON h.DestinationId = d.DestinationId\n"
                + "where d.DestinationName = N'" + destination + "' ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Hotel h = new Hotel(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(4),
                        null,
                        rs.getString(3),
                        rs.getInt(6),
                        rs.getInt(5));
                list.add(h);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public void insertHo(String hname, String hadd, String des, String img, int buid, int desid) {
        try {
            String sql = "INSERT INTO [dbo].[CheckHotel]\n"
                    + "           ([HotelName]\n"
                    + "           ,[HotelAddress]\n"
                    + "           ,[Description]\n"
                    + "           ,[HotelImage]\n"
                    + "           ,[busiID]\n"
                    + "           ,[DestinationId])\n"
                    + "     VALUES\n"
                    + "           (?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, hname);
            ps.setString(2, hadd);
            ps.setString(3, des);
            ps.setString(4, img);
            ps.setInt(5, buid);
            ps.setInt(6, desid);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            System.out.println("oke");

        }
    }

    public ArrayList<Hotel> getallCheckHotel() {
        ArrayList<Hotel> hotel = new ArrayList<>();
        try {
            String sql = "select * from [CheckHotel]";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int hoid = rs.getInt("HotelId");
                String honame = rs.getString("HotelName");
                String hoadd = rs.getString("HotelAddress");
                String des = rs.getString("Description");
                String img = rs.getString("HotelImage");
                int busiid = rs.getInt("busiID");
                int desid = rs.getInt("DestinationId");
                hotel.add(new Hotel(hoid, honame, hoadd, des, img, busiid, desid));

            }
        } catch (SQLException ex) {
        }
        return hotel;
    }

    public Hotel getCheckHotelbyID(int hoid) {
        Hotel hotel = null;
        try {
            String sql = "select * from [CheckHotel] where HotelId ='" + hoid + "'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int hoids = rs.getInt("HotelId");
                String honame = rs.getString("HotelName");
                String hoadd = rs.getString("HotelAddress");
                String des = rs.getString("Description");
                String img = rs.getString("HotelImage");
                int busiid = rs.getInt("busiID");
                int desid = rs.getInt("DestinationId");
                hotel = new Hotel(hoids, honame, hoadd, des, img, busiid, desid);

            }
        } catch (SQLException ex) {
        }
        return hotel;
    }

    public void insertHotel(String hname, String hadd, String des, String img, int buid, int desid) {
        try {
            String sql = "INSERT INTO [dbo].[Hotel]\n"
                    + "           ([HotelName]\n"
                    + "           ,[HotelAddress]\n"
                    + "           ,[Description]\n"
                    + "           ,[HotelImage]\n"
                    + "           ,[busiID]\n"
                    + "           ,[DestinationId])\n"
                    + "     VALUES\n"
                    + "           (?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, hname);
            ps.setString(2, hadd);
            ps.setString(3, des);
            ps.setString(4, img);
            ps.setInt(5, buid);
            ps.setInt(6, desid);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            System.out.println("oke");
        }
    }

    public void DeletecHo(int hoid) {
        try {
            String sql = "DELETE FROM [CheckHotel] where HotelId ='" + hoid + "'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {

        }
    }

    public float getAverageHotelRating(int HotelId) {
        float avgRating = 0;
        String sql = "SELECT AVG(avgRating) avgRating FROM (SELECT r.RoomId, b1.avgRating FROM (SELECT RoomId, AVG(Rating) avgRating FROM Booking \n"
                + "GROUP BY RoomId) b1 JOIN Room r ON b1.RoomId = r.RoomId WHERE HotelId = ?) b2";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, HotelId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                avgRating = rs.getFloat("avgRating");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return avgRating;
    }

    public static void main(String[] args) {
        HotelDAO dao = new HotelDAO();
//        List<Hotel> listHotel = dao.getAllHotelsByDesId(1);
//        for (Hotel hotel : listHotel) {
//            System.out.println(hotel);
        System.out.println(dao.getHotelByHotId(2).getAvgRating());
    }

}
