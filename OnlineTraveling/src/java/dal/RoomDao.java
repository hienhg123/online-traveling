/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Hotel;
import models.Room;
import models.RoomType;

/**
 *
 * @author Dell
 */
public class RoomDao extends DBContext {

    public Room GetRoomByRoomId(int roomId) {
        try {
            String sql = "SELECT r.HotelId,r.RoomId,r.RoomDescription,r.RoomImage,r.Price,r.Quantity,r.Size,r.RoomName,rt.RoomTypeName,rt.description from RoomType rt inner join Room r on rt.roomTypeID = r.RoomTypeId where r.RoomId = ?";

            PreparedStatement ps = connection.prepareStatement(sql);

            ps.setInt(1, roomId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Room roomHotel = new Room();
                roomHotel.setRoomId(rs.getInt("RoomId"));
                roomHotel.setRoomName(rs.getString("RoomName"));
                roomHotel.setRoomDescription(rs.getString("RoomDescription"));
                roomHotel.setRoomImage(rs.getString("RoomImage"));
                roomHotel.setSize(rs.getFloat("Size"));
                roomHotel.setPrice(rs.getInt("Price"));
                roomHotel.setQuantity(rs.getInt("Quantity"));
                RoomType type = new RoomType();
                type.setRoomTypeName(rs.getString("roomTypeName"));
                type.setDescription(rs.getString("description"));
                roomHotel.setRoomType(type);

                int hotelId = rs.getInt("HotelId");
                HotelDAO hotelDAO = new HotelDAO();
                Hotel hotel = hotelDAO.getHotelByHotId(hotelId);
                roomHotel.setHotel(hotel);
                return roomHotel;
            }
        } catch (SQLException ex) {
            Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public Room getRoomForPaymentConfirmation(int roomId, int amount, int rpid) {
        try {
            String sql = "SELECT r.HotelId,r.RoomId,r.RoomDescription,r.RoomImage,r.Price,r.Quantity,r.Size,r.RoomName,rt.RoomTypeName,rt.description from RoomType rt inner join Room r on rt.roomTypeID = r.RoomTypeId where r.RoomId = ?";

            PreparedStatement ps = connection.prepareStatement(sql);

            ps.setInt(1, roomId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Room roomHotel = new Room();
                roomHotel.setRoomId(rs.getInt("RoomId"));
                roomHotel.setRoomName(rs.getString("RoomName"));
                roomHotel.setRoomDescription(rs.getString("RoomDescription"));
                roomHotel.setRoomImage(rs.getString("RoomImage"));
                roomHotel.setSize(rs.getFloat("Size"));
                roomHotel.setPrice(rs.getInt("Price"));
                roomHotel.setQuantity(rs.getInt("Quantity"));
                RoomType type = new RoomType();
                type.setRoomTypeName(rs.getString("roomTypeName"));
                type.setDescription(rs.getString("description"));
                roomHotel.setRoomType(type);

                int hotelId = rs.getInt("HotelId");
                HotelDAO hotelDAO = new HotelDAO();
                Hotel hotel = hotelDAO.getHotelByHotId(hotelId);
                
                roomHotel.setHotel(hotel);
                roomHotel.setAmount(amount);
                roomHotel.setPlanId(rpid);
                return roomHotel;
            }
        } catch (SQLException ex) {
            Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }

    public List<Room> getAllRoomByHotelId(int hotId) {
        List<Room> listRom = new ArrayList<>();
        try {
            String sql = "select r.RoomId,r.Quantity,r.RoomName,r.RoomImage,r.Size,r.Price,r.HotelId,r.RoomDescription,rt.roomTypeName,h.* from"
                    + " [Hotel] h,[Room] r,[RoomType] rt\n"
                    + "where h.HotelId = r.HotelId"
                    + " and r.RoomTypeId = rt.roomTypeID and h.HotelId = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, hotId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Room roomHotel = new Room();
                roomHotel.setRoomId(rs.getInt("RoomId"));
                roomHotel.setRoomName(rs.getString("RoomName"));
                roomHotel.setRoomDescription(rs.getString("RoomDescription"));
                roomHotel.setRoomImage(rs.getString("RoomImage"));
                roomHotel.setSize(rs.getFloat("Size"));
                roomHotel.setPrice(rs.getInt("Price"));
                roomHotel.setQuantity(rs.getInt("Quantity"));
                RoomType type = new RoomType();
                type.setRoomTypeName(rs.getString("roomTypeName"));
                Hotel hot = new Hotel();
                hot.setHotelName(rs.getString("HotelName"));
                hot.setHotelId(rs.getInt("HotelId"));
                roomHotel.setRoomType(type);
                roomHotel.setHotel(hot);

                float avgRating = getRoomAverageRating(rs.getInt("RoomId"));
                roomHotel.setAvgRating(avgRating);
                listRom.add(roomHotel);

            }
        } catch (SQLException ex) {
            Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(RoomDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return listRom;
    }

    public ArrayList<RoomType> getallRoom() {
        ArrayList<RoomType> Room = new ArrayList<>();
        try {
            String sql = "select * from RoomType";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int Rid = rs.getInt("roomTypeID");
                String Rname = rs.getString("roomTypeName");
                String des = rs.getString("description");
                Room.add(new RoomType(Rid, Rname, des));
            }
        } catch (SQLException ex) {
        }
        return Room;
    }

    public RoomType getRoom(int rID) {
        RoomType Room = new RoomType();
        try {
            String sql = "select * from RoomType where roomTypeID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, rID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int Rid = rs.getInt("roomTypeID");
                String Rname = rs.getString("roomTypeName");
                String des = rs.getString("description");
                return new RoomType(Rid, Rname, des);
            }
        } catch (SQLException ex) {
        }
        return null;
    }

    public void updateRoom(int rID, String rName, String rDes) {
        try {
            String sql = "UPDATE  [RoomType] set roomTypeName ='" + rName + "'"
                    + " ,[description] = '" + rDes + "'where roomTypeID='" + rID + "'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("DELETE product 1");
        }
    }

    public void insertRoom(String description, String image, String price, String size, String roomtype, String quantity, String hotelId, String name) {
        try {
            String sql = "INSERT INTO [dbo].[Room]\n"
                    + "           ([RoomDescription]\n"
                    + "           ,[RoomImage]\n"
                    + "           ,[Price]\n"
                    + "           ,[Size]\n"
                    + "           ,[RoomTypeId]\n"
                    + "           ,[Quantity]\n"
                    + "           ,[HotelId]\n"
                    + "           ,[RoomName])\n"
                    + "     VALUES\n"
                    + "           (?,?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, description);
            ps.setString(2, image);
            ps.setString(3, price);
            ps.setString(4, size);
            ps.setString(5, roomtype);
            ps.setString(6, quantity);
            ps.setString(7, hotelId);
            ps.setString(8, name);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            System.out.println("oke");

        }
    }

    public ArrayList<RoomType> getRoombyname(String rName) {
        ArrayList<RoomType> room = new ArrayList<>();
        try {
            String sql = "select * from RoomType where roomTypeName Like '%" + rName + "%'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int Rid = rs.getInt("roomTypeID");
                String Rname = rs.getString("roomTypeName");
                String des = rs.getString("description");
                room.add(new RoomType(Rid, Rname, des));
            }
        } catch (SQLException ex) {
        }
        return room;
    }

    public ArrayList<Room> getRoombyUid(int uid) {
        ArrayList<Room> room = new ArrayList<>();
        try {
            String sql = "SELECT Room.RoomDescription , Room.RoomImage , Room.Price , Room.RoomName\n"
                    + "FROM Room\n"
                    + "INNER JOIN Booking\n"
                    + "ON Room.RoomId = Booking.RoomId\n"
                    + "Where Booking.userID = '" + uid + "'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String des = rs.getString("RoomDescription");
                String img = rs.getString("RoomImage");
                int price = rs.getInt("Price");
                String rname = rs.getString("RoomName");
                room.add(new Room(uid, rname, des, img, price, price, uid, null, null, price));
            }
            ps.close();
        } catch (SQLException ex) {
        }
        return room;
    }

    public float getRoomAverageRating(int RoomId) {
        float rating = 0;
        String sql = "SELECT RoomId, AVG(Rating) avgRating FROM Booking \n"
                + "GROUP BY RoomId HAVING RoomId = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, RoomId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                rating = rs.getFloat("avgRating");
            }
            ps.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return rating;
    }

    public static void main(String[] args) {
        RoomDao ROOM = new RoomDao();

//        List<Room> list = ROOM.getAllRoomByHotelId(1);
//        for (Room r : list) {
//            System.out.println("RoomId: " + r.getRoomId() + "  AvgMark: " + r.getAvgRating());
//        }
//        System.out.println(ROOM.getRoomForPaymentConfirmation(1, 2).getAmount());
//        System.out.println(ROOM.getRoomForPaymentConfirmation(3, 1).getAmount());
//        System.out.println(ROOM.getRoomForPaymentConfirmation(6, 1).getAmount());
    }

}
