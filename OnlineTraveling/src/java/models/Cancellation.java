/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import java.sql.Date;

/**
 *
 * @author Admin
 */
public class Cancellation {
    private int cancelID;
    private String cancelDayType;
    private Date cancelDateFrom,cancelDateTo;
    private String cancelPolicyDetail;
    private int cancelAfter,cancelBefore,cancelPenalty;
    private boolean cancelStatus;
    private int hotelID;

    public Cancellation() {
    }

    public Cancellation(int cancelID, String cancelDayType, Date cancelDateFrom, Date cancelDateTo, String cancelPolicyDetail, int cancelAfter, int cancelBefore, int cancelPenalty, int hotelID) {
        this.cancelID = cancelID;
        this.cancelDayType = cancelDayType;
        this.cancelDateFrom = cancelDateFrom;
        this.cancelDateTo = cancelDateTo;
        this.cancelPolicyDetail = cancelPolicyDetail;
        this.cancelAfter = cancelAfter;
        this.cancelBefore = cancelBefore;
        this.cancelPenalty = cancelPenalty;
        this.hotelID = hotelID;
    }

    public Cancellation(int cancelID, String cancelDayType, Date cancelDateFrom, Date cancelDateTo, String cancelPolicyDetail, int cancelAfter, int cancelBefore, int cancelPenalty, boolean cancelStatus, int hotelID) {
        this.cancelID = cancelID;
        this.cancelDayType = cancelDayType;
        this.cancelDateFrom = cancelDateFrom;
        this.cancelDateTo = cancelDateTo;
        this.cancelPolicyDetail = cancelPolicyDetail;
        this.cancelAfter = cancelAfter;
        this.cancelBefore = cancelBefore;
        this.cancelPenalty = cancelPenalty;
        this.cancelStatus = cancelStatus;
        this.hotelID = hotelID;
    }
  
    
    public Cancellation(int cancelID, String cancelPolicyDetail, int cancelAfter, int cancelBefore, int cancelPenalty, int hotelID) {
        this.cancelID = cancelID;
        this.cancelPolicyDetail = cancelPolicyDetail;
        this.cancelAfter = cancelAfter;
        this.cancelBefore = cancelBefore;
        this.cancelPenalty = cancelPenalty;
        this.hotelID = hotelID;
    }
   
    public int getCancelID() {
        return cancelID;
    }

    public void setCancelID(int cancelID) {
        this.cancelID = cancelID;
    }

    public String getCancelDayType() {
        return cancelDayType;
    }

    public void setCancelDayType(String cancelDayType) {
        this.cancelDayType = cancelDayType;
    }

    public Date getCancelDateFrom() {
        return cancelDateFrom;
    }

    public void setCancelDateFrom(Date cancelDateFrom) {
        this.cancelDateFrom = cancelDateFrom;
    }

    public Date getCancelDateTo() {
        return cancelDateTo;
    }

    public void setCancelDateTo(Date cancelDateTo) {
        this.cancelDateTo = cancelDateTo;
    }
  
    public String getCancelPolicyDetail() {
        return cancelPolicyDetail;
    }

    public void setCancelPolicyDetail(String cancelPolicyDetail) {
        this.cancelPolicyDetail = cancelPolicyDetail;
    }

    public int getCancelAfter() {
        return cancelAfter;
    }

    public void setCancelAfter(int cancelAfter) {
        this.cancelAfter = cancelAfter;
    }

    public int getCancelBefore() {
        return cancelBefore;
    }

    public void setCancelBefore(int cancelBefore) {
        this.cancelBefore = cancelBefore;
    }

    public int getCancelPenalty() {
        return cancelPenalty;
    }

    public void setCancelPenalty(int cancelPenalty) {
        this.cancelPenalty = cancelPenalty;
    }

    public boolean isCancelStatus() {
        return cancelStatus;
    }

    public void setCancelStatus(boolean cancelStatus) {
        this.cancelStatus = cancelStatus;
    }
    

    public int getHotelID() {
        return hotelID;
    }

    public void setHotelID(int hotelID) {
        this.hotelID = hotelID;
    }
    
    
}
