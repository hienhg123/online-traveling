/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import java.sql.Date;

/**
 *
 * @author ACER
 */
public class Booking {

    private int BookingId;
    private int RoomId;
    private int userID;
    private int DiscountId;
    private Date ToDate;
    private Date FromDate;
    private String Status;
    private float Rating;
    private float Price;
    private String Comment;
    private String RoomName;
    private String hotelName;
    private String hotelID;
    private int RatePlanId;
    public Booking() {
    }

    public Booking(int BookingId, int RoomId, int userID, int DiscountId, Date ToDate, Date FromDate, String Status, float Rating, float Price, String RoomName, String hotelName, String hotelID) {
        this.BookingId = BookingId;
        this.RoomId = RoomId;
        this.userID = userID;
        this.DiscountId = DiscountId;
        this.ToDate = ToDate;
        this.FromDate = FromDate;
        this.Status = Status;
        this.Rating = Rating;
        this.Price = Price;
        this.RoomName = RoomName;
        this.hotelName = hotelName;
        this.hotelID = hotelID;
    }

    public Booking(int BookingId, int RoomId, int userID, int DiscountId, Date ToDate, Date FromDate, String Status, float Rating, float Price, String RoomName, String hotelName) {
        this.BookingId = BookingId;
        this.RoomId = RoomId;
        this.userID = userID;
        this.DiscountId = DiscountId;
        this.ToDate = ToDate;
        this.FromDate = FromDate;
        this.Status = Status;
        this.Rating = Rating;
        this.Price = Price;
        this.RoomName = RoomName;
        this.hotelName = hotelName;
    }



    public Booking(int BookingId, int RoomId, int userID, int DiscountId, Date ToDate, Date FromDate, String Status, float Rating, float Price, String RoomName) {
        this.BookingId = BookingId;
        this.RoomId = RoomId;
        this.userID = userID;
        this.DiscountId = DiscountId;
        this.ToDate = ToDate;
        this.FromDate = FromDate;
        this.Status = Status;
        this.Rating = Rating;
        this.Price = Price;
        this.RoomName = RoomName;
    }

    public Booking(int BookingId, int RoomId, int userID, int DiscountId, Date ToDate, Date FromDate, String Status, float Price, String RoomName, String hotelName, String hotelID) {
        this.BookingId = BookingId;
        this.RoomId = RoomId;
        this.userID = userID;
        this.DiscountId = DiscountId;
        this.ToDate = ToDate;
        this.FromDate = FromDate;
        this.Status = Status;
        this.Price = Price;
        this.RoomName = RoomName;
        this.hotelName = hotelName;
        this.hotelID = hotelID;
    }
   
    public String getComment() {
        return Comment;
    }

    public void setComment(String Comment) {
        this.Comment = Comment;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotelID() {
        return hotelID;
    }

    public void setHotelID(String hotelID) {
        this.hotelID = hotelID;
    }



    public int getBookingId() {
        return BookingId;
    }

    public void setBookingId(int BookingId) {
        this.BookingId = BookingId;
    }

    public int getRoomId() {
        return RoomId;
    }

    public void setRoomId(int RoomId) {
        this.RoomId = RoomId;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getDiscountId() {
        return DiscountId;
    }

    public void setDiscountId(int DiscountId) {
        this.DiscountId = DiscountId;
    }

    public Date getToDate() {
        return ToDate;
    }

    public void setToDate(Date ToDate) {
        this.ToDate = ToDate;
    }

    public Date getFromDate() {
        return FromDate;
    }

    public void setFromDate(Date FromDate) {
        this.FromDate = FromDate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public float getRating() {
        return Rating;
    }

    public void setRating(float Rating) {
        this.Rating = Rating;
    }

    public float getPrice() {
        return Price;
    }

    public void setPrice(float Price) {
        this.Price = Price;
    }

    public String getRoomName() {
        return RoomName;
    }

    public void setRoomName(String RoomName) {
        this.RoomName = RoomName;
    }

    @Override
    public String toString() {
        return "Booking{" + "BookingId=" + BookingId + ", RoomId=" + RoomId + ", userID=" + userID + ", DiscountId=" + DiscountId + ", ToDate=" + ToDate + ", FromDate=" + FromDate + ", Status=" + Status + ", Rating=" + Rating + ", Price=" + Price + ", RoomName=" + RoomName + '}';
    }
}
