/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author HONG QUAN
 */
public class Question {
    private int QuestionId;
    private Room room;
    private String QuestionContent;
    private String AnswerContent;
    private String dateQuestion;
    private String dateAnswer;
    private Business business;
    private User user;

    public Question() {
    }

    public Question(int QuestionId, Room room, String QuestionContent, String AnswerContent, String dateQuestion, String dateAnswer, Business business, User user) {
        this.QuestionId = QuestionId;
        this.room = room;
        this.QuestionContent = QuestionContent;
        this.AnswerContent = AnswerContent;
        this.dateQuestion = dateQuestion;
        this.dateAnswer = dateAnswer;
        this.business = business;
        this.user = user;
    }

    public int getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(int QuestionId) {
        this.QuestionId = QuestionId;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public String getQuestionContent() {
        return QuestionContent;
    }

    public void setQuestionContent(String QuestionContent) {
        this.QuestionContent = QuestionContent;
    }

    public String getAnswerContent() {
        return AnswerContent;
    }

    public void setAnswerContent(String AnswerContent) {
        this.AnswerContent = AnswerContent;
    }

    public String getDateQuestion() {
        return dateQuestion;
    }

    public void setDateQuestion(String dateQuestion) {
        this.dateQuestion = dateQuestion;
    }

    public String getDateAnswer() {
        return dateAnswer;
    }

    public void setDateAnswer(String dateAnswer) {
        this.dateAnswer = dateAnswer;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Question{" + "QuestionId=" + QuestionId + ", room=" + room + ", QuestionContent=" + QuestionContent + ", AnswerContent=" + AnswerContent + ", dateQuestion=" + dateQuestion + ", dateAnswer=" + dateAnswer + ", business=" + business + ", user=" + user + '}';
    }

    
}
