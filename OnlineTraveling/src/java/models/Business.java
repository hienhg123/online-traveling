/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author ACER
 */
public class Business {

    private int busiID;
    private String username;
    private String password;
    private String busiName;
    private String email;
    private String phone;
    private boolean status;
    private String firstName;
    private String lastName;

    public Business() {
    }

    public Business(int busiID, String username, String password, String busiName, String email, String phone, boolean status, String firstName, String lastName) {
        this.busiID = busiID;
        this.username = username;
        this.password = password;
        this.busiName = busiName;
        this.email = email;
        this.phone = phone;
        this.status = status;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Business(int busiID, String username, String password, String busiName, String email, String phone, boolean status) {
        this.busiID = busiID;
        this.username = username;
        this.password = password;
        this.busiName = busiName;
        this.email = email;
        this.phone = phone;
        this.status = status;
    }

    public int getBusiID() {
        return busiID;
    }

    public void setBusiID(int busiID) {
        this.busiID = busiID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBusiName() {
        return busiName;
    }

    public void setBusiName(String busiName) {
        this.busiName = busiName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
