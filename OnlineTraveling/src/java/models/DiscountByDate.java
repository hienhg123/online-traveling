/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import java.sql.Date;

/**
 *
 * @author ACER
 */
public class DiscountByDate {

    private int DiscountId;
    private Date DiscountFrom;
    private Date DiscountTo;
    private int Percentage;
    private boolean Status;

    public DiscountByDate() {
    }

    public DiscountByDate(int DiscountId, Date DiscountFrom, Date DiscountTo, int Percentage, boolean Status) {
        this.DiscountId = DiscountId;
        this.DiscountFrom = DiscountFrom;
        this.DiscountTo = DiscountTo;
        this.Percentage = Percentage;
        this.Status = Status;
    }

    
    
//    public DiscountByDate(int DiscountId, Date DiscountFrom, Date DiscountTo, int Percentage) {
//        this.DiscountId = DiscountId;
//        this.DiscountFrom = DiscountFrom;
//        this.DiscountTo = DiscountTo;
//        this.Percentage = Percentage;
//    }

    public int getDiscountId() {
        return DiscountId;
    }

    public void setDiscountId(int DiscountId) {
        this.DiscountId = DiscountId;
    }

    public Date getDiscountFrom() {
        return DiscountFrom;
    }

    public void setDiscountFrom(Date DiscountFrom) {
        this.DiscountFrom = DiscountFrom;
    }

    public Date getDiscountTo() {
        return DiscountTo;
    }

    public void setDiscountTo(Date DiscountTo) {
        this.DiscountTo = DiscountTo;
    }

    public int getPercentage() {
        return Percentage;
    }

    public void setPercentage(int Percentage) {
        this.Percentage = Percentage;
    }

    @Override
    public String toString() {
        return "DiscountByDate{" + "DiscountId=" + DiscountId + ", DiscountFrom=" + DiscountFrom + ", DiscountTo=" + DiscountTo + ", Percentage=" + Percentage + '}';
    }

}
