/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author HONG QUAN
 */
public class Report {
    private int ReportId;
    private String reportContent;
    private String dateReport;
    private User user;
    private Question question;

    public Report() {
    }

    public int getReportId() {
        return ReportId;
    }

    public void setReportId(int ReportId) {
        this.ReportId = ReportId;
    }

    public String getReportContent() {
        return reportContent;
    }

    public void setReportContent(String reportContent) {
        this.reportContent = reportContent;
    }

    public String getDateReport() {
        return dateReport;
    }

    public void setDateReport(String dateReport) {
        this.dateReport = dateReport;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Report(int ReportId, String reportContent, String dateReport, User user, Question question) {
        this.ReportId = ReportId;
        this.reportContent = reportContent;
        this.dateReport = dateReport;
        this.user = user;
        this.question = question;
    }

    @Override
    public String toString() {
        return "Report{" + "ReportId=" + ReportId + ", reportContent=" + reportContent + ", dateReport=" + dateReport + ", user=" + user + ", question=" + question + '}';
    }

        
}
