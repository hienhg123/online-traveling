/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author ACER
 */
public class Attraction {

    private int AttractionId;
    private String AttractionName;
    private Destination DestinationId;
    private String AttractionImage;
    private String AttractionDescription;
    private String AttractionAddress;
    private String AttractionHotline;

    public Attraction() {
    }

    public Attraction(int AttractionId, String AttractionName, Destination DestinationId, String AttractionImage, String AttractionDescription, String AttractionAddress, String AttractionHotline) {
        this.AttractionId = AttractionId;
        this.AttractionName = AttractionName;
        this.DestinationId = DestinationId;
        this.AttractionImage = AttractionImage;
        this.AttractionDescription = AttractionDescription;
        this.AttractionAddress = AttractionAddress;
        this.AttractionHotline = AttractionHotline;
    }

    public int getAttractionId() {
        return AttractionId;
    }

    public void setAttractionId(int AttractionId) {
        this.AttractionId = AttractionId;
    }

    public String getAttractionName() {
        return AttractionName;
    }

    public void setAttractionName(String AttractionName) {
        this.AttractionName = AttractionName;
    }

    public Destination getDestinationId() {
        return DestinationId;
    }

    public void setDestinationId(Destination DestinationId) {
        this.DestinationId = DestinationId;
    }

    public String getAttractionImage() {
        return AttractionImage;
    }

    public void setAttractionImage(String AttractionImage) {
        this.AttractionImage = AttractionImage;
    }

    public String getAttractionDescription() {
        return AttractionDescription;
    }

    public void setAttractionDescription(String AttractionDescription) {
        this.AttractionDescription = AttractionDescription;
    }

    public String getAttractionAddress() {
        return AttractionAddress;
    }

    public void setAttractionAddress(String AttractionAddress) {
        this.AttractionAddress = AttractionAddress;
    }

    public String getAttractionHotline() {
        return AttractionHotline;
    }

    public void setAttractionHotline(String AttractionHotline) {
        this.AttractionHotline = AttractionHotline;
    }

    @Override
    public String toString() {
        return "Attraction{" + "AttractionId=" + AttractionId + ", AttractionName=" + AttractionName + ", DestinationId=" + DestinationId + ", AttractionImage=" + AttractionImage + ", AttractionDescription=" + AttractionDescription + ", AttractionAddress=" + AttractionAddress + ", AttractionHotline=" + AttractionHotline + '}';
    }

}
