/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class Hotel {

    private int HotelId;
    private String HotelName;
    private String HotelAddress;
    private String Description;
    private String HotelImage;
    private int busiID;
    private int DestinationID;
    ArrayList<RoomType> rooms;
    private float avgRating;

    public Hotel() {
    }

    public Hotel(int HotelId, String HotelName, String HotelAddress, String Description, String HotelImage, int busiID, int DestinationID) {
        this.HotelId = HotelId;
        this.HotelName = HotelName;
        this.HotelAddress = HotelAddress;
        this.Description = Description;
        this.HotelImage = HotelImage;
        this.busiID = busiID;
        this.DestinationID = DestinationID;
    }

    public Hotel(int HotelId, String HotelName, String HotelAddress, String Description, String HotelImage, int busiID, int DestinationID, ArrayList<RoomType> rooms) {
        this.HotelId = HotelId;
        this.HotelName = HotelName;
        this.HotelAddress = HotelAddress;
        this.Description = Description;
        this.HotelImage = HotelImage;
        this.busiID = busiID;
        this.DestinationID = DestinationID;
        this.rooms = rooms;
    }

    public Hotel(int HotelId, String HotelName, String HotelAddress, String Description, String HotelImage, int busiID, int DestinationID, ArrayList<RoomType> rooms, float avgRating) {
        this.HotelId = HotelId;
        this.HotelName = HotelName;
        this.HotelAddress = HotelAddress;
        this.Description = Description;
        this.HotelImage = HotelImage;
        this.busiID = busiID;
        this.DestinationID = DestinationID;
        this.rooms = rooms;
        this.avgRating = avgRating;
    }

    public int getHotelId() {
        return HotelId;
    }

    public void setHotelId(int HotelId) {
        this.HotelId = HotelId;
    }

    public String getHotelName() {
        return HotelName;
    }

    public void setHotelName(String HotelName) {
        this.HotelName = HotelName;
    }

    public String getHotelAddress() {
        return HotelAddress;
    }

    public void setHotelAddress(String HotelAddress) {
        this.HotelAddress = HotelAddress;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getHotelImage() {
        return HotelImage;
    }

    public void setHotelImage(String HotelImage) {
        this.HotelImage = HotelImage;
    }

    public int getBusiID() {
        return busiID;
    }

    public void setBusiID(int busiID) {
        this.busiID = busiID;
    }

    public int getDestinationID() {
        return DestinationID;
    }

    public void setDestinationID(int DestinationID) {
        this.DestinationID = DestinationID;
    }

    public ArrayList<RoomType> getRooms() {
        return rooms;
    }

    public void setRooms(ArrayList<RoomType> rooms) {
        this.rooms = rooms;
    }

    public float getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(float avgRating) {
        this.avgRating = avgRating;
    }

    @Override
    public String toString() {
        return "Hotel{" + "HotelId=" + HotelId + ", HotelName=" + HotelName + ", HotelAddress=" + HotelAddress + ", Description=" + Description + ", HotelImage=" + HotelImage + ", busiID=" + busiID + ", DestinationID=" + DestinationID + ", rooms=" + rooms + '}';
    }

}
