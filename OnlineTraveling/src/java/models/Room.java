/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author HONG QUAN
 */
public class Room {

    private int RoomId;
    private String RoomName;
    private String RoomDescription;
    private String RoomImage;
    private int Price;
    private float Size;
    private int Quantity;
    private RoomType roomType;
    private Hotel hotel;
    private int available;
    private float avgRating;
    private int amount;
    private float manyNightPrice;
    private String oneNightPrice_str;//Only for booking confirmation page, for storing the price of 1 room for 1 night
    private String manyNightPrice_str;//Only for booking confirmation page, for storing the price of 1 room for many night
    private String oneRoomPrice_str;//Only for booking confirmation page, for storing the price of many room for many night
    private int availablePlan; //Storing how many available plan for a room in hotel details page
    private int PlanId; // Holding value rate plan value for booking confirmation page.

    public int getRoomId() {
        return RoomId;
    }

    public void setRoomId(int RoomId) {
        this.RoomId = RoomId;
    }

    public String getRoomName() {
        return RoomName;
    }

    public void setRoomName(String RoomName) {
        this.RoomName = RoomName;
    }

    public Room() {
    }

    public String getRoomDescription() {
        return RoomDescription;
    }

    public void setRoomDescription(String RoomDescription) {
        this.RoomDescription = RoomDescription;
    }

    public String getRoomImage() {
        return RoomImage;
    }

    public void setRoomImage(String RoomImage) {
        this.RoomImage = RoomImage;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int Price) {
        this.Price = Price;
    }

    public float getSize() {
        return Size;
    }

    public void setSize(float Size) {
        this.Size = Size;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int Quantity) {
        this.Quantity = Quantity;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public float getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(float avgRating) {
        this.avgRating = avgRating;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getOneNightPrice_str() {
        return oneNightPrice_str;
    }

    public void setOneNightPrice_str(String oneNightPrice_str) {
        this.oneNightPrice_str = oneNightPrice_str;
    }

    public int getAvailablePlan() {
        return availablePlan;
    }

    public void setAvailablePlan(int availablePlan) {
        this.availablePlan = availablePlan;
    }

    public int getPlanId() {
        return PlanId;
    }

    public void setPlanId(int PlanId) {
        this.PlanId = PlanId;
    }

    public String getManyNightPrice_str() {
        return manyNightPrice_str;
    }

    public void setManyNightPrice_str(String manyNightPrice_str) {
        this.manyNightPrice_str = manyNightPrice_str;
    }

    public String getOneRoomPrice_str() {
        return oneRoomPrice_str;
    }

    public void setOneRoomPrice_str(String oneRoomPrice_str) {
        this.oneRoomPrice_str = oneRoomPrice_str;
    }

    public float getManyNightPrice() {
        return manyNightPrice;
    }

    public void setManyNightPrice(float manyNightPrice) {
        this.manyNightPrice = manyNightPrice;
    }

    public Room(int RoomId, String RoomName, String RoomDescription, String RoomImage, int Price, float Size, int Quantity, RoomType roomType, Hotel hotel, int available) {
        this.RoomId = RoomId;
        this.RoomName = RoomName;
        this.RoomDescription = RoomDescription;
        this.RoomImage = RoomImage;
        this.Price = Price;
        this.Size = Size;
        this.Quantity = Quantity;
        this.roomType = roomType;
        this.hotel = hotel;
        this.available = available;
    }

    public Room(int RoomId, String RoomName, String RoomDescription, String RoomImage, int Price, float Size, int Quantity, RoomType roomType, Hotel hotel, int available, float avgRating) {
        this.RoomId = RoomId;
        this.RoomName = RoomName;
        this.RoomDescription = RoomDescription;
        this.RoomImage = RoomImage;
        this.Price = Price;
        this.Size = Size;
        this.Quantity = Quantity;
        this.roomType = roomType;
        this.hotel = hotel;
        this.available = available;
        this.avgRating = avgRating;
    }

    @Override
    public String toString() {
        return "Room{" + "RoomId=" + RoomId + ", RoomName=" + RoomName + ", RoomDescription=" + RoomDescription + ", RoomImage=" + RoomImage + ", Price=" + Price + ", Size=" + Size + ", Quantity=" + Quantity + ", roomType=" + roomType + ", hotel=" + hotel + '}';
    }

}
