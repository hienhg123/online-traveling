/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author ACER
 */
public class RatePlan {

    private int PlanId;
    private String PlanName;
    private int PlanType;
    private float Percentage;
    private int Day;
    private Hotel HotelId;
    private int AvailablePayment;
    private String CancellationPolicy;
    private boolean Meal;
    private boolean reasonable;//Note = false khi can chinh sua RatePlan
    private int RoomId;//Only for getting room with multiple rateplans in hotel detail page.

    public RatePlan() {
    }

    

    public RatePlan(int PlanId, String PlanName, int PlanType, float Percentage, int Day, Hotel HotelId) {
        this.PlanId = PlanId;
        this.PlanName = PlanName;
        this.PlanType = PlanType;
        this.Percentage = Percentage;
        this.Day = Day;
        this.HotelId = HotelId;
    }

    public int getPlanId() {
        return PlanId;
    }

    public void setPlanId(int PlanId) {
        this.PlanId = PlanId;
    }

    public String getPlanName() {
        return PlanName;
    }

    public void setPlanName(String PlanName) {
        this.PlanName = PlanName;
    }

    public int getPlanType() {
        return PlanType;
    }

    public void setPlanType(int PlanType) {
        this.PlanType = PlanType;
    }

    public float getPercentage() {
        return Percentage;
    }

    public void setPercentage(float Percentage) {
        this.Percentage = Percentage;
    }

    public int getDay() {
        return Day;
    }

    public void setDay(int Day) {
        this.Day = Day;
    }

    public Hotel getHotelId() {
        return HotelId;
    }

    public void setHotelId(Hotel HotelId) {
        this.HotelId = HotelId;
    }

    public boolean isReasonable() {
        return reasonable;
    }

    public void setReasonable(boolean reasonable) {
        this.reasonable = reasonable;
    }

    public int getRoomId() {
        return RoomId;
    }

    public void setRoomId(int RoomId) {
        this.RoomId = RoomId;
    }

    public int getAvailablePayment() {
        return AvailablePayment;
    }

    public void setAvailablePayment(int AvailablePayment) {
        this.AvailablePayment = AvailablePayment;
    }

    public String getCancellationPolicy() {
        return CancellationPolicy;
    }

    public void setCancellationPolicy(String CancellationPolicy) {
        this.CancellationPolicy = CancellationPolicy;
    }

    public boolean isMeal() {
        return Meal;
    }

    public void setMeal(boolean Meal) {
        this.Meal = Meal;
    }

    @Override
    public String toString() {
        return "RatePlan{" + "PlanId=" + PlanId + ", PlanName=" + PlanName + ", PlanType=" + PlanType + ", Percentage=" + Percentage + ", Day=" + Day + ", AvailablePayment=" + AvailablePayment + ", CancellationPolicy=" + CancellationPolicy + ", Meal=" + Meal + ", reasonable=" + reasonable + ", RoomId=" + RoomId + '}';
    }
    

}
